#ifndef BBB_IO_H
#define BBB_IO_H

#include "satellite.h"

// GPIO PIN BIT LOCATIONS
  // USRx LEDs are the 21st, 22nd, 23rd, and 24th LSBs of GPIO1
#define USR0 53
#define USR1 54
#define USR2 55
#define USR3 56

bool initIO();
void setGPIODirection(unsigned short gpioNumber, bool setAsInput);
void setGPIOValue(unsigned short gpioNumber, bool value);
bool getGPIOValue(unsigned short gpioNumber);
unsigned int readAIN(unsigned short channel);
void setPWM(unsigned int dutyCyclePercent, unsigned int uSecPeriod, unsigned short channel);
TCB getIOTask();
void ioTask(void * data);

#endif
