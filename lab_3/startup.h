#ifndef STARTUP_H
#define STARTUP_H

#include "satellite.h"
#include "display.h"

TCB getStartup(unsigned int *channel, display_t *satelliteDisplay, display_t *earthDisplay);

#endif // STARTUP_H
