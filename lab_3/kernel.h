#ifndef KERNEL_H
#define KERNEL_H

/**
 * This is the header file for the satellite's kernel
 */

#include <time.h>
#include "satellite.h"

void initKernel();
// This function starts the kernel
void kernelStart();
void addTask(TCB tcb, clock_t wait);
void endTask();

#endif // KERNEL_H
