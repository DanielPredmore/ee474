#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "startup.h"
#include "kernel.h"
#include "consoleDisplay.h"
#include "powerSubsystem.h"
#include "satelliteComms.h"
#include "vehicleComms.h"
#include "thrusterSubsystem.h"
#include "warningAlarm.h"
#include "display.h"
#include "timeWindow.h"
#include "solarPanelControl.h"
#include "bbbIO.h"
#include "consoleKeypad.h"

#define MAJOR_TIME (5 * CLOCKS_PER_SEC)
#define TIME_UPDATE_TIME (CLOCKS_PER_SEC/10)

struct startup_struct
{
	unsigned int * channel;
	display_t * satelliteDisplay;
	display_t * earthDisplay;
};
typedef struct startup_struct startData;

void startup(void *data);

TCB getStartup(unsigned int * channel, display_t *satelliteDisplay, display_t *earthDisplay)
{
	startData *data = (startData *)malloc(sizeof(startData));
	data->channel = channel;
	data->satelliteDisplay = satelliteDisplay;
	data->earthDisplay = earthDisplay;
	TCB tcb;
	tcb.taskDataPtr = data;
	tcb.task = startup;
	return tcb;
}

void startup(void *data)
{
	startData * sData = (startData *)data;
	initializeGlobals();

	display_t *satelliteDisplay = sData->satelliteDisplay;
	display_t *earthDisplay = sData->earthDisplay;

	window_t *conWindow = newWindow(satelliteDisplay, TRUE, 3, 1, 13, 128);
	window_t *sComsWindow = newWindow(earthDisplay, TRUE, 3, 1, 13, 128);
	window_t *vComsWindow = newWindow(earthDisplay, TRUE, 17, 1, 7, 128); 
	window_t *temp1 = newWindow(satelliteDisplay, TRUE, 1 ,1, 1, 128);
	windowPrintColor(temp1, "Satellite Terminal", GREEN, 0, 1);
	window_t *temp2 = newWindow(earthDisplay, TRUE, 1, 1, 1, 128);
	windowPrintColor(temp2, "Earth Terminal", CYAN, 0, 1);

	addTask(getWarningAlarm(), 0);
	addTask(getSatelliteComms(sComsWindow, vComsWindow), MAJOR_TIME);
	addTask(getVehicleComms(vComsWindow, sData->channel), MAJOR_TIME); 
	addTask(getConsoleDisplay(conWindow), 0);
	addTask(getPowerSubsystem(), MAJOR_TIME);
	addTask(getThrusterSubsystem(), MAJOR_TIME);
	//addTask(getSolarPanelControl(), MAJOR_TIME);
	addTask(getIOTask(), TIME_UPDATE_TIME);
	addTask(getTimeWindow(temp1, GREEN, 103), TIME_UPDATE_TIME);
	addTask(getTimeWindow(temp2, CYAN, 103), TIME_UPDATE_TIME);
	endTask();
	free(data);
}
