#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <termios.h>

typedef struct termios termios;
typedef struct timeval timeval;
termios origTermios;
// resets the terminal back to its original state, to be called upon exitting the program
void resetTerminalMode()
{
    tcsetattr(0, TCSANOW, &origTermios);
}
// sets the terminal to read input one character at a time, rather than one line at a time
void setNonblockTerminalMode()
{
    termios newTermios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &origTermios);
    memcpy(&newTermios, &origTermios, sizeof(newTermios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(resetTerminalMode);
    cfmakeraw(&newTermios);
    tcsetattr(0, TCSANOW, &newTermios);
}
// checks if a key has been pressed
int inputPending()
{
    timeval tv = { 0l, 0l };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv);
}
// pulls a character from the input buffer
int getInput()
{
    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}

/*
int main(int argc, char *argv[])
{
    setNonblockTerminalMode();

	while(1){
    while (!kbhit()) {
    }
	int c = getch();
	if(c == 'q')
		return 0;
	printf("\r%c", c);}
}
*/
