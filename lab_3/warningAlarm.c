#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "warningAlarm.h"
#include "satellite.h"
#include "bbbIO.h"

extern unsigned int *batteryLevel;
extern unsigned short fuelLevel;

struct warningAlarmData {
	bool ledStates[3];
};
typedef struct warningAlarmData dataStruct;

// private function prototypes
void warningAlarm(void *data);
void checkSetLed(unsigned short led, bool value, dataStruct *data);
void blinkLed(unsigned short led, int rate, dataStruct *data);

/**
 * This function makes a new TCB task for the kernel
 *
 * @return the TCB for the warningAlarm
 */
TCB getWarningAlarm() 
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));

	setGPIODirection(USR1, FALSE);
	setGPIODirection(USR2, FALSE);
	setGPIODirection(USR3, FALSE);
	setGPIOValue(USR1, FALSE);
	setGPIOValue(USR2, FALSE);
	setGPIOValue(USR3, FALSE);

	// here is space to initialize the data struct's data
	data->ledStates[0] = FALSE;
	data->ledStates[1] = FALSE;
	data->ledStates[2] = FALSE;


	// setting the values in the TCB
	theTCB.task = &warningAlarm;
	theTCB.taskDataPtr = data;
	return theTCB;
}

/**
 * This is the main TCB task function for the warning alarm system.
 * It is responsible for setting the warning lights
 *
 * @param data this is the data struct form the PCB
 */
void warningAlarm(void *data)
{
	// casting back to dataStruct
	dataStruct *theData = (dataStruct*)data;

	// checking the warning conditions
	if (*batteryLevelPtr > 50 && fuelLevel > 50)
		checkSetLed(USR3, TRUE, theData);
	else
		checkSetLed(USR3, FALSE, theData);

	if (*batteryLevelPtr <= 50 && *batteryLevelPtr > BATTERY_LOW)
		blinkLed(USR2, 2, theData);
	else if (*batteryLevelPtr <= BATTERY_LOW) 
		blinkLed(USR2, 1, theData);
	else
		checkSetLed(USR2, FALSE, theData);

	if (fuelLevel <= 50 && fuelLevel > FUEL_LOW)
		blinkLed(USR1, 2, theData);
	else if (fuelLevel <= FUEL_LOW)
		blinkLed(USR1, 1, theData);
	else
		checkSetLed(USR1, FALSE, theData);
}

/**
 * checkSetLed compares the current value of an led to the set value
 * if they are different, it updates the led to the new value.
 * Otherwise it keep the led the same and does nothing
 *
 * @param led the led #
 * @param value the value of the led
 * @param data a pointer to the data struct
 */
void checkSetLed(unsigned short led, bool value, dataStruct *data)
{
	if (value != data->ledStates[led-USR1])
	{
		setGPIOValue(led, value);
		data->ledStates[led-USR1] = value;
	}
}

/**
 * This function blinks an led at a set rate
 *
 * @param led the led #
 * @param rate the speed of the blink in seconds
 * @param data a pointer to the data struct
 */
void blinkLed(unsigned short led, int rate, dataStruct *data)
{
	time_t rawtime;
	time(&rawtime);
	bool set = (rawtime / rate) % (rate * 2) <= rate/2;
	checkSetLed(led, set, data);
}
