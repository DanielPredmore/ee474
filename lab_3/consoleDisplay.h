#ifndef CONSOLE_DISPLAY_H
#define CONSOLE_DISPLAY_H

#include "satellite.h"

TCB getConsoleDisplay();
void display(char * str, int data, int terminal); 

#endif // CONSOLE_DISPLAY_H
