/**
 * This is the kernel for the satellite
 */

#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include "kernel.h"
#include "satellite.h"
#include "consoleDisplay.h"
#include "powerSubsystem.h"
#include "satelliteComms.h"
#include "thrusterSubsystem.h"
#include "warningAlarm.h"
#include "bbbIO.h"

#define MAX_TASKS 50

struct kernelStruct;

/**
 * This is the struct the kernel uses to make the task queue
 * as a linked list
 */
struct kernelStruct {
	TCB tcb; // the task data
	clock_t runlast; // the time it was last run
	clock_t wait; // how long to wait between calls
	struct kernelStruct * next; // the next task
	struct kernelStruct * last; // the last task run 
};
typedef struct kernelStruct task_t;

task_t taskBlocks[MAX_TASKS];

// A pointer to the head of the taskQueue
task_t * taskQueue;
task_t * nullQueue;

// function prototypes
void run();
void doTask();

void initKernel()
{
	initIO();
	int i;
	for (i = 0; i < MAX_TASKS - 1; i++)
	{
		taskBlocks[i].next = &taskBlocks[i+1];
	}
	taskBlocks[MAX_TASKS - 1].next = NULL;
	nullQueue = taskBlocks;
}

/**
 * This function adds a task into the task queue.
 * It may be called by any task while the kernel is running.
 * However external functions may only call this function before the
 * kernel runs.
 *
 * @param tcb the tcb data of the task
 * @param wait the amount of time the kernel will wait before rerunning
 */
void addTask(TCB tcb, clock_t wait)
{
	if (nullQueue == NULL)
			return;
	task_t *task = nullQueue;
	nullQueue = nullQueue->next;
	task->tcb = tcb;
	task->runlast = clock();
	task->wait = wait;

	if (NULL == taskQueue)
	{
		taskQueue = task;
		task->next = task;
		task->last = task;
	}
	else
	{
		task_t *temp = taskQueue->next;
		taskQueue->next = task;
		temp->last = task;
		task->last = taskQueue;
		task->next = temp;
	}
}

/**
 * This function may be called by a task to remove itself form the
 * task queue.
 */
void endTask()
{
	task_t *temp = taskQueue;
	
	if (temp->next == temp)
	{
		taskQueue = NULL;
	}
	else
	{
		temp->last->next = temp->next;
		temp->next->last = temp->last;
		taskQueue = temp->next;
	}

	temp->next = nullQueue;
	nullQueue = temp;
}

/**
 * This function is responsible for starting the kernel
 */
void kernelStart() {
	setGPIODirection(67, FALSE);
	run();
}

/**
 * This the main kernel forever loop
 */
void run() {
	while (taskQueue != NULL)
	{
		taskQueue = taskQueue->next;
		doTask();
		if('q' == currentInput)
		{
			return;	
		}
	}
}

/**
 * This runs a task from the task queue
 */
void doTask()
{
	time_t now = clock();
	if (taskQueue->wait < now - taskQueue->runlast)
	{
		TCB *tcb = &taskQueue->tcb;
		taskQueue->runlast = clock();
		setGPIOValue(67, TRUE);
		tcb->task(tcb->taskDataPtr);
		setGPIOValue(67, FALSE);
	}
}
