/**
 * satellite.c initializes all global variables
 */

#include <stdio.h>
#include <unistd.h> 
#include "satellite.h"
#include "bbbIO.h"

// global variables
extern unsigned int thrusterCommand;
extern unsigned int mode; 
extern unsigned short fuelLevel;
extern unsigned short powerConsumption;
extern unsigned short powerGeneration;
extern char command; 
extern char response; 
extern bool solarPanelState;
extern bool solarPanelDeploy; 
extern bool solarPanelRetract;
extern bool fuelLow;
extern bool batteryLow;
extern unsigned int mode; 
extern unsigned int * batteryLevelPtr;
extern unsigned int * batteryTempPtr;
extern bool driveMotorSpeedInc;
extern bool driveMotorSpeedDec;
extern char currentInput;
extern unsigned short inputChecks;

unsigned int batteryLevel;
unsigned int batteryTemperature;

void initializeGlobals() {
		thrusterCommand = NO_COMMAND;
		fuelLevel = FULL;
		powerConsumption = 0;
		powerGeneration = 0;
		command = '\0';
		response = '\0';
		solarPanelState = FALSE;
		solarPanelDeploy = FALSE; 
		solarPanelRetract = FALSE;
		fuelLow = FALSE;
		batteryLow = FALSE;
		setGPIODirection(MODE_PIN, TRUE); 
		mode = 0;
	   	batteryLevelPtr = &batteryLevel;
		batteryTempPtr = &batteryTemperature;
		batteryLevel = 100;
		batteryTemperature = 33;
		driveMotorSpeedInc = FALSE;
		driveMotorSpeedDec = FALSE;
		currentInput = '\0';
		inputChecks = 0;
}
