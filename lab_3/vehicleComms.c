#include <stdio.h>
#include "satellite.h"
#include <unistd.h>
#include "keypad.h"
#include "display.h"
#include "vehicleComms.h"
#include <stdlib.h>
#include <fcntl.h>
#include "ports.h"

#define CHAR sizeof(char)

// global variables
extern char command;
extern char response; 

// data struct for vehicle communication
struct vehicleCommsData 
{
	char * command; 
	char * response;
	unsigned int *channel;
	window_t * window; 
}; 

typedef struct vehicleCommsData dataStruct;

// function prototypes
void vehicleComms(void *data);

/**
 * This function creates a new TCB for the kernal
 *
 * @returns TCB for vehicleComms
 */

TCB getVehicleComms(window_t * window, unsigned int * channel) 
{
	TCB theTCB; 
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 

	// initilizaing data struct
	(data->command) = &command;
	(data->response) = &response;
	(data->channel) = channel; 	
	(data->window) = window;

	theTCB.task = &vehicleComms;
	theTCB.taskDataPtr = data;

	return theTCB;
}

void vehicleComms(void * data) 
{
	char buffer[2] = {'\0', '\0'}; 
	int rc;
	int wc; 
		
	dataStruct * commsData = (dataStruct *) data;

	wc = write(commsData->channel[1], commsData->command, CHAR);	

	// vehicle sends response
	if ((rc = read(commsData->channel[0], buffer, CHAR)) != -1) 
	{
		commsData->response[0] =  buffer[0];
	} 
	else
	{
		commsData->response[0] = '\0';
	}
}
