#ifndef SATELLITE_H
#define SATELLITE_H

/**
 * This header file contains the globally available variables.
 */

// defining default values
#define NO_COMMAND 0x0000
#define FULL 100
#define EMPTY 0
#define BATTERY_LOW 10
#define FUEL_LOW 10
#define MODE_PIN 27
#define TOTAL_INPUT_CHECKS 1

// defining the TCB struct
struct _satelliteKernelTCB {
		void (*task)(void*);
		void* taskDataPtr;
};
typedef struct _satelliteKernelTCB TCB;

// Defining a bool
enum boolean {
		FALSE = 0,
		TRUE = 1
};
typedef enum boolean bool;


// Thruster Control
unsigned int thrusterCommand;


// Power Management
unsigned int * batteryLevelPtr;
unsigned int * batteryTempPtr;
// unsigned short batteryLevel;
unsigned short fuelLevel;
unsigned short powerConsumption;
unsigned short powerGeneration;

// Solar Panel Control
bool solarPanelState;
bool solarPanelDeploy; 
bool solarPanelRetract;
bool driveMotorSpeedInc; 
bool driveMotorSpeedDec;

// Warning Alarm
bool fuelLow;
bool batteryLow;

// Vehicle Communication
char command; 
char response; 

// Display
unsigned int mode; 

// For key input
char currentInput;
unsigned short inputChecks;

void initializeGlobals();

#endif // SATELLITE_H
