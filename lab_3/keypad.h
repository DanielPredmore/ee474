#ifndef KEYPAD_H
#define KEYPAD_H

// functions for non-blocking input operations
// REMEMBER to provide a way to exit before implementing!
void setNonblockTerminalMode();
int inputPending();
int getInput();

#endif
