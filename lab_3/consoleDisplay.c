#include "satellite.h"
#include <stdio.h>
#include <stdlib.h>
#include "bbbIO.h"
#include "consoleDisplay.h"
#include "display.h"

#define BUFFER_LENGTH 100

// global variables
extern bool solarPanelState;
extern bool solarPanelDeploy;
extern bool solarPanelRetract;
extern bool fuelLow; 
extern bool batteryLow; 
extern bool driveMotorSpeedInc;
extern bool driveMotorSpeedDec;
extern unsigned short batteryLevel; 
extern unsigned short fuelLevel;
extern unsigned short powerConsumption; 
extern unsigned short powerGeneration;
extern unsigned int mode; 


// terminal ports 
display_t * earth; 
display_t * satellite; 

// the corresponding data struct for console display data
struct consoleDisplayData
{
	bool  solarPanelState;
    bool  solarPanelDeploy;
	bool  solarPanelRetract;	
	bool  fuelLow; 
	bool  batteryLow; 
	bool  driveMotorSpeedInc;
	bool  driveMotorSpeedDec;
	unsigned short  batteryLevel;
	unsigned short  fuelLevel;
	unsigned short  powerConsumption; 
	unsigned short  powerGeneration;
    window_t * window; 	
};
typedef struct consoleDisplayData dataStruct; 

// function prototypes 
void consoleDisplay(void * data); 
void display(char * str, int data, int terminal); 

/**
 * This function makes a new TCB for console display
 *
 * @return the TCB for the consoleDisplay 
 */
TCB getConsoleDisplay(window_t * window)
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 

	// -1 to each values so the values are initialized to different
	data->solarPanelState = solarPanelState - 1;
	data->solarPanelDeploy = solarPanelDeploy - 1;
	data->solarPanelRetract = solarPanelRetract - 1;
	data->driveMotorSpeedInc = driveMotorSpeedInc - 1;
	data->driveMotorSpeedDec = driveMotorSpeedDec - 1;
	data->fuelLow = fuelLow - 1; 
	data->batteryLow = batteryLow - 1; 
	data->batteryLevel = (unsigned short)*batteryLevelPtr - 1; 
	data->fuelLevel = fuelLevel - 1;
	data->powerConsumption = powerConsumption - 1;
	data->powerGeneration = powerGeneration - 1;
	data->window = window; 

	theTCB.task = &consoleDisplay; 
	theTCB.taskDataPtr = data; 

	return theTCB; 
}

/**
 * This function prints the status and annunciation modes
 * for the satellite. 
 *
 * @param the given data struct for console display
 */
void consoleDisplay(void * data) 
{
	// data ruct for console display
	dataStruct *prevData = (dataStruct *)data;

	bool prevFuelLow = fuelLow;
	bool prevBatteryLow = batteryLow;
	
	// alerts when fuel is low or battery is low 
	unsigned int alert = (~prevFuelLow && fuelLow) || (~prevBatteryLow && batteryLow);
	
	char buffer[BUFFER_LENGTH];

	mode = getGPIOValue(MODE_PIN);

	// checks if any value has changed
	if (prevData->solarPanelState != solarPanelState ||
			prevData->batteryLevel != (unsigned short) *batteryLevelPtr ||
			prevData->fuelLow != fuelLow ||
			prevData->batteryLow != batteryLow ||
			prevData->fuelLevel != fuelLevel ||
			prevData->powerConsumption != powerConsumption 
			||prevData->powerGeneration != powerGeneration)
	{

		prevData->solarPanelState = solarPanelState;
		prevData->batteryLevel = (unsigned short) *batteryLevelPtr;
		prevData->fuelLow = fuelLow;
		prevData->batteryLow = batteryLow;
		prevData->fuelLevel = fuelLevel;
		prevData->powerConsumption = powerConsumption;
		prevData->powerGeneration = powerGeneration;

		color_t color = RESET;
		windowClear(prevData->window);
		// transfering data to the earth terminal 
		if (mode) // status mode
		{
			if (batteryLow || fuelLow) // battery or fuel is low 
			{
				windowClear(prevData->window);
				windowPrintColor(prevData->window, "ANNUNCIATION MODE:", MAGENTA, 1, 50);
			   	sprintf(buffer, "Fuel Low    = %d \0", fuelLow);
				color = RESET;
				if (fuelLow)
						color = RED;
				windowPrintColor(prevData->window, buffer, color, 3, 52); 
				sprintf(buffer, "Battery Low = %d \0", batteryLow);
			   	color = RESET;
				if (batteryLow)
						color = RED;
				windowPrintColor(prevData->window, buffer, color, 5, 52);  
			}
		
			windowPrintColor(prevData->window, "STATUS MODE:", MAGENTA, 1, 2);
		   	sprintf(buffer, "Solar Panel State = %d\0", solarPanelState);
		   	color = RESET;
			if (!(solarPanelDeploy || solarPanelRetract))
				color = WHITE;
			else if (solarPanelDeploy)
				color = CYAN;
			else if (solarPanelRetract)
				color = BLUE;
			windowPrintColor(prevData->window, buffer, color, 3, 4); 
			sprintf(buffer, "Battery Level     = %d\0", *batteryLevelPtr);
			color = GREEN;
			if (*batteryLevelPtr < 50)
					color = YELLOW;
			if (*batteryLevelPtr < 10)
					color = RED;
			windowPrintColor(prevData->window, buffer, color, 5, 4); 
			sprintf(buffer, "Fuel Level        = %d\0", fuelLevel);
			color = GREEN;
			if (fuelLevel < 50)
					color = YELLOW;
			if (fuelLevel < 10)
					color = RED;
			windowPrintColor(prevData->window, buffer, color, 7, 4); 
			sprintf(buffer, "Power Consumption = %d\0", powerConsumption);
		   	color = RESET;
			if (powerConsumption > 0)
					color = YELLOW;
			if (powerConsumption > 5)
					color = RED;
		   	windowPrintColor(prevData->window, buffer, color, 9, 4); 
			sprintf(buffer, "Power Generation  = %d\0", powerGeneration);
		   	color = YELLOW;
			if (powerGeneration > powerConsumption)
					color = GREEN;
			if (powerGeneration == 0 || powerGeneration == powerConsumption)
					color = RESET;
			windowPrintColor(prevData->window, buffer, color, 11, 4);
		
		}
		else // annunciation mode 
		{
			// print annunciation mode to earth
			windowPrintColor(prevData->window, "ANNUNCIATION MODE:", MAGENTA, 1, 2);
	   		sprintf(buffer, "Fuel Low    = %d \0", fuelLow); 				
			color = RESET;
			if (fuelLow)
					color = RED;
			windowPrintColor(prevData->window, buffer, color, 3, 4); 
			sprintf(buffer, "Battery Low = %d \0", batteryLow); 
			color = RESET;
			if (batteryLow)
					color = RED;
			windowPrintColor(prevData->window, buffer, color, 5, 4);  
		}
	}
}

