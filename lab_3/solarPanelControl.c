#include "solarPanelControl.h"
#include "bbbIO.h"
#include "kernel.h"
#include "consoleKeypad.h"
#include <stdlib.h>

#define DEFAULT_MOTOR_SPEED 85
// max speed
#define MOTOR_SPEED_FULL_ON 100
// min speed
#define MOTOR_SPEED_FULL_OFF 0
#define MOTOR_SPEED_INCREMENT 5
#define MOTOR_DRIVE_FULL_ON 0x00ff
#define MOTOR_DRIVE_FULL_OFF 0
#define MOTOR_PWM_PERIOD_USEC 500

extern bool solarPanelState;
extern bool solarPanelDeploy;
extern bool solarPanelRetract;
extern bool driveMotorSpeedInc;
extern bool driveMotorSpeedDec;

// solar panel control data struct
struct solarPanelControlData
{
	bool * solarPanelState;
	bool * solarPanelDeploy;
	bool * solarPanelRetract;
	bool * driveMotorSpeedInc;
	bool * driveMotorSpeedDec;
};
typedef struct solarPanelControlData dataStruct;

// private function prototypes
void solarPanelControl(void *data);

/**
 * This function makes a new TCB task for the kernel 
 *
 * @return the TCB for the solarPanelControl
 */
TCB getSolarPanelControl()
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));

	// initialization of data struct's data
	data->solarPanelState = &solarPanelState;
	data->solarPanelDeploy = &solarPanelDeploy;
	data->solarPanelRetract = &solarPanelRetract;
	data->driveMotorSpeedInc = &driveMotorSpeedInc;
	data->driveMotorSpeedDec = &driveMotorSpeedDec;

	// setting the values in the TCB
	theTCB.task = &solarPanelControl;
	theTCB.taskDataPtr = data;

	return theTCB;
}
/**
 * This is the task function for the solar panel control task of the satellite system
 *
 * @param data: pointer to the solarPanelControl data struct
 */
void solarPanelControl(void * data)
{
	// how extended the solar panels are
	static unsigned short driveMotor = 0;
	// the speed of the motor
	static unsigned short driveMotorSpeed = DEFAULT_MOTOR_SPEED;
	static bool startedKeypad = FALSE;

	dataStruct * theData = (dataStruct *)data;

	if(TRUE == *(theData->driveMotorSpeedInc))
	{
		if(driveMotorSpeed + MOTOR_SPEED_INCREMENT >= MOTOR_SPEED_FULL_ON)
			driveMotorSpeed = MOTOR_SPEED_FULL_ON;
		else
			driveMotorSpeed += MOTOR_SPEED_INCREMENT;
		*(theData->driveMotorSpeedInc) = FALSE;
		setPWM(driveMotorSpeed, MOTOR_PWM_PERIOD_USEC, 0);
	}
	else if(TRUE == *(theData->driveMotorSpeedDec))
	{
		if(driveMotorSpeed - MOTOR_SPEED_INCREMENT >= MOTOR_SPEED_FULL_ON)
			driveMotorSpeed = MOTOR_SPEED_FULL_OFF;
		else
			driveMotorSpeed -= MOTOR_SPEED_INCREMENT;
		*(theData->driveMotorSpeedDec) = FALSE;
		setPWM(driveMotorSpeed, MOTOR_PWM_PERIOD_USEC, 0);
	}

	if(TRUE == *(theData->solarPanelDeploy))
	{
		if(driveMotor + driveMotorSpeed >= MOTOR_DRIVE_FULL_ON)
		{
			driveMotor = MOTOR_DRIVE_FULL_ON;
			*(theData->solarPanelDeploy) = FALSE;
			*(theData->solarPanelState) = TRUE;
		}
		else
		{
			driveMotor += driveMotorSpeed;
		}
		if(FALSE == startedKeypad)
		{
			startedKeypad = TRUE;
			addTask(getConsoleKeypad(), CLOCKS_PER_SEC/100);
			setPWM(driveMotorSpeed, MOTOR_PWM_PERIOD_USEC, 0);
		}
	}
	else if (TRUE == *(theData->solarPanelRetract))
	{
		if(driveMotor - driveMotorSpeed >= MOTOR_DRIVE_FULL_ON)
		{
			driveMotor = MOTOR_DRIVE_FULL_OFF;
			*(theData->solarPanelRetract) = FALSE;
			*(theData->solarPanelState) = FALSE;
		}
		else
		{
			driveMotor -= driveMotorSpeed;
		}
		if(FALSE == startedKeypad)
		{
			startedKeypad = TRUE;
			addTask(getConsoleKeypad(), CLOCKS_PER_SEC/100);
			setPWM(driveMotorSpeed, MOTOR_PWM_PERIOD_USEC, 0);
		}
	}
	else
	{
		startedKeypad = FALSE;
		setPWM(0, MOTOR_PWM_PERIOD_USEC, 0);
		endTask();
	}
}
