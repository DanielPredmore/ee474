#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include "satellite.h"
#include "kernel.h"
#include "display.h"
#include "keypad.h"
#include "startup.h"
#include "consoleDisplay.h"
#include "powerSubsystem.h"
#include "satelliteComms.h"
#include "timeWindow.h"
#include "vehicleComms.h"
#include "warningAlarm.h"
#include "vehicle.h"


int main(int argc, char** argv)
{
	initKernel();
	FILE *term = fopen("/dev/null", "w");
	display_t *disp = newDisplay(term, 80, 130);
	window_t *wind = newWindow(disp, TRUE, 1, 1, 78, 128);
	int channel[3];

	channel[0] = open("/dev/pts/ptmx", O_RDWR | O_NONBLOCK);
	channel[1] = open("/dev/pts/ptmx", O_RDWR | O_NONBLOCK);
	channel[2] = 0;

	if (pipe(channel) == -1)
	{
			perror("PIPE FAILED");
			exit(1);
	}
	
	pthread_t thread[1];
	int vt = pthread_create(&thread[0], NULL, (void *) vehicle, (void *) channel);

	if (vt)
	{
		perror("THREAD FAILED");
	}

	addTask(getStartup(channel, disp, disp), 0);
	printf("Press q to begin\n");
	kernelStart();

	initKernel();
	addTask(getVehicleComms(wind, channel), 0);
	printf("running vehicleComms press q to contue\n");
	kernelStart();
	channel[2] = 1;

	pthread_join(thread[0], NULL);
	pthread_exit(NULL);


	return 0;

}


