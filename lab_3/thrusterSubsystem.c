#include "thrusterSubsystem.h"
#include "bbbIO.h"
#include <stdlib.h>
#include <time.h>

// maximum number of seconds of fuel for one thruster running at one magnitude
// #define MAX_FUEL 11664000
// test amount to see if system if properly functioning
#define MAX_FUEL 10000
#define DIRECTION_MASK 0x000F
#define DURATION_MASK 0xFF00
#define MAGNITUDE_MASK 0x00F0
#define LEFT_MASK 0x0001
#define RIGHT_MASK 0x0002
#define UP_MASK 0x0004
#define DOWN_MASK 0x0008
#define DURATION_BIT_SHIFT 8
#define MAGNITUDE_BIT_SHIFT 4
#define MAX_MAGNITUDE 0xF
#define THRUSTER_PWM_PERIOD 500

// global variables
extern unsigned int thrusterCommand;
extern unsigned short fuelLevel;
extern bool fuelLow;

// data struct for the global power system variables
struct thrusterSubsystemData
{
	unsigned int * thrusterCommand;
	unsigned short * fuelLevel;
	bool * fuelLow;
};
typedef struct thrusterSubsystemData dataStruct;

// private function prototype
void thrusterSubsystem(void * data);

/**
 * This function makes a new TCB task for the kernel 
 *
 * @return the TCB for the thrusterSubsystem
 */
TCB getThrusterSubsystem()
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));
	// initialization of data struct's data
	data->thrusterCommand = &thrusterCommand;
	data->fuelLevel = &fuelLevel;
	data->fuelLow = &fuelLow;	
	// setting the values in the TCB
	theTCB.task = &thrusterSubsystem;
	theTCB.taskDataPtr = data;

	return theTCB;
}

/**
 * This is the task function for the thruster subsystem task of the satellite system
 *
 * @param data: pointer to the thruster subsystem data struct
 */
void thrusterSubsystem(void * data)
{
	dataStruct * td = (dataStruct *) data;
    static unsigned short left = 0, right = 0, up = 0, down = 0;
	static time_t prevTime;
	time_t nextTime;
	static unsigned long fuelRemaining = MAX_FUEL;
	static unsigned long controlSignal = 0x0000;
    
    unsigned short magnitude = 0;
    static unsigned short duration = 0;
	unsigned short timeElapsed = 0;
    
	if(*(td->thrusterCommand) & DIRECTION_MASK) // any direction
	{
		magnitude = (unsigned short) ((*(td->thrusterCommand) & MAGNITUDE_MASK) >> MAGNITUDE_BIT_SHIFT);
		// setting control signal
		//controlSignal = 0x7FFF;
		//controlSignal = controlSignal >> (MAX_MAGNITUDE - magnitude);
		duration = (unsigned short) ((*(td->thrusterCommand) & DURATION_MASK) >> DURATION_BIT_SHIFT);
		time(&prevTime);
		setPWM(magnitude * 100 / MAX_MAGNITUDE, THRUSTER_PWM_PERIOD, 1);
	}
	if(*(td->thrusterCommand) & LEFT_MASK) // left
    {
        left = magnitude;
    }
    if(*(td->thrusterCommand) & RIGHT_MASK) // right
    {
        right = magnitude;
    }
    if(*(td->thrusterCommand) & UP_MASK) // up
    {
        up = magnitude;
    }
    if(*(td->thrusterCommand) & DOWN_MASK) // down
    {
        down = magnitude;
    }
    
    // *** duration calculation ***
	if(duration != 0)
	{
		time(&nextTime);
		timeElapsed = (unsigned short) (nextTime - prevTime);
		prevTime = nextTime;
		if(timeElapsed > duration)
		{
			duration = 0;
		}
		else
		{
			duration -= timeElapsed;
		}
    	// *** fuelLevel calculation ***
		if(left | right | up | down)
		{
			if((left + right + up + down) * timeElapsed > fuelRemaining)
			{
				fuelRemaining = 0;
			}
			else
			{
				fuelRemaining -= (left + right + up + down) * timeElapsed;
			}
			*(td->fuelLevel) = fuelRemaining / (MAX_FUEL / FULL);
		}
		// *** control signal ***
		//if(controlSignal)
		//{
			// output control signal is lsb
			//if(controlSignal & 0x1) // if lsb is 1
			//	controlSignal = (controlSignal >> 1) | 0x7FFF; // circular shift of 15 lsb
			//else // if lsb is 0
			//	controlSignal = (controlSignal >> 1) | 0x3FFF; // circular shift of 15 lsb
		//	
	}
	else
	{
		left = right = up = down = 0;
		setPWM(0, THRUSTER_PWM_PERIOD, 1);
	}
	if(*(td->fuelLevel) < FUEL_LOW)
		*(td->fuelLow) = TRUE;
	else
		*(td->fuelLow) = FALSE;
}
