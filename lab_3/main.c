/**
 * This is the main c file for the satellite.
 * It is responsible for initializing the global values 
 * and launching the kernel.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include "kernel.h"
#include "startup.h"
#include "vehicle.h"
#include "keypad.h"
#include "display.h"

#define NUM_THREADS 2

void main() 
{
	initKernel();
	setNonblockTerminalMode();
	int channel[3];
	int vt;
	int kt;
	
	channel[0] = open("/dev/pts/ptmx", O_RDWR | O_NONBLOCK);
	channel[1] = open("/dev/pts/ptmx", O_RDWR | O_NONBLOCK); 	
	channel[2] = 0; // quit boolean for vehicle

	if (pipe(channel) == -1) 
	{
		perror("PIPE FAILED"); 
		exit(1); 
	}


	//FILE *sat = fopen("/dev/pts/1", "w");
	//FILE *earth = stdout;

	FILE * sat = stdout;
	FILE * earth = fopen("/dev/pts/3", "w");

	display_t *satelliteDisplay = newDisplay(sat, 80, 130);
	display_t *earthDisplay = newDisplay(earth, 80, 130);

	// create threads 
	pthread_t thread[NUM_THREADS]; 
	vt = pthread_create(&thread[0], NULL, (void *) vehicle, (void *) channel);  	
	
	if(vt) 
	{
		perror("THREAD FAILED"); 
	}
	
	//vehicle(channel);

	addTask(getStartup(channel, satelliteDisplay, earthDisplay), 0);
	kernelStart();
	channel[2] = 1;
	
	freeDisplay(satelliteDisplay);
	freeDisplay(earthDisplay);

	// terminate threads
	pthread_join(thread[0], NULL); 

	pthread_exit(NULL); 
}
