#include "bbbIO.h"
#include "keypad.h"
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

// #define TEST_BBB_IO

// BASE ADDRESSES FOR GPIO HEADERS
  // MAX NUMBER OF PINS PER HEADER
#define MAX_HEADER_PINS 32
#define GPIO_COUNT 128
#define GPIO_HEADER_COUNT 4
#define ADC_STEP_SIZE 12
#define SEL_INP_SHIFT 19
#define SEL_INP_MASK (0xful << SEL_INP_SHIFT)
#define DEF_STEP_CONFIG 0x00040000
#define STEP_CONFIG_MASK (0xffffffff >> 4)
#define END_OF_SEQ_BIT 1
// enables steps for bits 1-12 lsb
#define STEP_ENABLE_MASK 0x1ffe
// address distance between step configs
#define STEP_CONFIG_STEP 0x08
#define GPIO0_START_ADDR 0x44e07000
#define GPIO0_END_ADDR 0x44e08000
#define GPIO1_START_ADDR 0x4804c000
#define GPIO1_END_ADDR   0x4804d000
#define GPIO2_START_ADDR 0x481ac000
#define GPIO2_END_ADDR   0x481ad000
#define GPIO3_START_ADDR 0x481ae000
#define GPIO3_END_ADDR   0x481af000
#define GPIO0_SIZE (GPIO0_END_ADDR - GPIO0_START_ADDR)
#define GPIO1_SIZE (GPIO1_END_ADDR - GPIO1_START_ADDR)
#define GPIO2_SIZE (GPIO2_END_ADDR - GPIO2_START_ADDR)
#define GPIO3_SIZE (GPIO3_END_ADDR - GPIO3_START_ADDR)
// listed as TSC_ADC_SS in AM335x Tech Ref Manual
#define ADC_START_ADDR 0x44e0d000
#define ADC_END_ADDR 0x44e0f000
#define ADC_SIZE (ADC_END_ADDR - ADC_START_ADDR)

// GPIO REGISTERS
#define GPIO_OUTPUTEN 0x134
#define GPIO_DATAIN 0x138
#define GPIO_DATAOUT 0x13C
#define GPIO_CLEARDATAOUT 0x190
#define GPIO_SETDATAOUT 0x194

// ADC REGISTERS
#define IRQSTATUS_RAW 0x24
#define DMAENABLE_SET 0x38
#define STEP_ENABLE 0x54
#define STEP_CONFIG01 0x64
#define STEP_CONFIG02 0x6c
#define STEP_CONFIG03 0x74
#define STEP_CONFIG04 0x7c
#define STEP_CONFIG05 0x84
#define STEP_CONFIG06 0x8c
#define STEP_CONFIG07 0x94
#define STEP_CONFIG08 0x9c
#define STEP_CONFIG09 0xa4
#define STEP_CONFIG10 0xac
#define STEP_CONFIG11 0xb4
#define STEP_CONFIG12 0xbc
#define FIFO0_DATA 0x100
#define FIFO1_DATA 0x200

extern char currentInput;

// PWM stuff


// private function prototypes
void releaseIO();
void enableAINChannel(unsigned short channel);

// structure for holding data for specific gpio pins
struct GPIODATA
{
	unsigned short header;
	unsigned short bitoffset;
}; 
typedef struct GPIODATA gpioData;

// GLOBAL VARS
int fildes; // file descriptor param for mmap function
volatile void * gpioAddr[GPIO_HEADER_COUNT];
volatile void * adcAddr;
gpioData gpios[GPIO_COUNT];

// initializes system for gpio and ain operations
bool initIO()
{ 
  fildes = open("/dev/mem", O_RDWR | O_SYNC); // file descriptor param for mmap function
  int i; // iterator for for-loops
  bool failed = FALSE;
  // initializing gpios
  gpioAddr[0] = mmap(NULL, GPIO0_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, GPIO0_START_ADDR);
  gpioAddr[1] = mmap(NULL, GPIO1_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, GPIO1_START_ADDR);
  gpioAddr[2] = mmap(NULL, GPIO2_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, GPIO2_START_ADDR);
  gpioAddr[3] = mmap(NULL, GPIO3_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, GPIO3_START_ADDR);
  // checks if any of the mmaps failed
  for(i = 0; i < GPIO_HEADER_COUNT; i++)
  {
    if(MAP_FAILED == gpioAddr[i])
	{
      printf("mmap for gpio[%d] failed.\n", i);
	  failed = TRUE;
	}
  }
  // initializing gpios array
  for(i = 0; i < GPIO_COUNT; i++)
  {
    gpios[i].header = i / MAX_HEADER_PINS;
    gpios[i].bitoffset = i % MAX_HEADER_PINS;	
  }
  // initializing adc input
  adcAddr = mmap(NULL, ADC_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, ADC_START_ADDR);
  if(MAP_FAILED == adcAddr)
	failed = TRUE;
  enableAINChannel(0);	
  // initializing pwm inputs
  /*
  FILE * file = fopen("/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip6/export", "w");
  fputs("0", file);
  fclose(file);
  file = fopen("/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip6/export", "w");
  fputs("1", file);
  fclose(file);
  file = fopen("/sys/devices/platform/ocp/ocp:P8_13_pinmux/state", "w");
  fputs("pwm", file);
  fclose(file);
  file = fopen("/sys/devices/platform/ocp/ocp:P8_19_pinmux/state", "w");
  fputs("pwm", file);
  fclose(file);
  file = fopen("/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip6/pwm0/enable", "w");
  fputs("1", file);
  fclose(file);
  file = fopen("/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip6/pwm1/enable", "w");
  fputs("1", file);
  fclose(file);*/

  if(FALSE == failed)
	atexit(releaseIO);
  return failed;  
}

// resets memory mapping of gpio and ain addresses 
// should be called at the end of a program
void releaseIO()
{
  munmap((void*)gpioAddr[0], GPIO0_SIZE);
  munmap((void*)gpioAddr[1], GPIO1_SIZE);
  munmap((void*)gpioAddr[2], GPIO2_SIZE);
  munmap((void*)gpioAddr[3], GPIO3_SIZE);
  munmap((void*)adcAddr, ADC_SIZE);
  close(fildes);
}

// sets the direction of a gpio pin as input or output
// should be set before reading or writing to a gpio pin
void setGPIODirection(unsigned short gpioNumber, bool setAsInput)
{
  unsigned int * outEnPtr = (unsigned int *)(gpioAddr[gpios[gpioNumber].header] + GPIO_OUTPUTEN);
  if(setAsInput)
    *outEnPtr |= (1 << gpios[gpioNumber].bitoffset);
  else
    *outEnPtr &= ~(1 << gpios[gpioNumber].bitoffset);
}

// call setGPIODirection() first and set this pin to output
// sets the gpio pin high or low depending on value
void setGPIOValue(unsigned short gpioNumber, bool value)
{
  unsigned int * dataOutPtr = (unsigned int *)(gpioAddr[gpios[gpioNumber].header] + GPIO_DATAOUT);
  if(value)
    *dataOutPtr |= (1 << gpios[gpioNumber].bitoffset);
  else
    *dataOutPtr &= ~(1 << gpios[gpioNumber].bitoffset);
}
// call setGPIODirection() first and set this pin to input
// returns the current state of the gpio pin
bool getGPIOValue(unsigned short gpioNumber)
{
  volatile unsigned int * dataInPtr = (volatile unsigned int *)(gpioAddr[gpios[gpioNumber].header] + GPIO_DATAIN);
  if(*dataInPtr & (1 << gpios[gpioNumber].bitoffset))
    return TRUE;
  else
    return FALSE;
}
// enables an analog input channel to read analog input
void enableAINChannel(unsigned short channel)
{
  *((unsigned long *)(adcAddr + STEP_ENABLE)) |= STEP_ENABLE_MASK;
  for(int i = 0; i < ADC_STEP_SIZE; i++)
  {
    unsigned long * configPtr = (unsigned long *)(adcAddr + STEP_CONFIG01 + i * STEP_CONFIG_STEP);
    *configPtr &= ~STEP_CONFIG_MASK;
	unsigned long setValue = DEF_STEP_CONFIG + ((unsigned long)channel << SEL_INP_SHIFT);
	*configPtr += setValue;
  }
}
// reads the current value of the analog input channel
unsigned int readAIN(unsigned short channel)
{
  *((unsigned long *)(adcAddr + STEP_ENABLE)) |= STEP_ENABLE_MASK;
  unsigned int * irqStatusRaw = ((unsigned int *)(adcAddr + IRQSTATUS_RAW));
  // waits for an end_of_sequence flag from the adc before reading
  while(!(*(irqStatusRaw) & (1 << END_OF_SEQ_BIT)));
  unsigned int fifo0data = *((unsigned int *)(adcAddr + FIFO0_DATA));
  fifo0data &= ~(UINT_MAX << ADC_STEP_SIZE);
  *((unsigned long *)(adcAddr + STEP_ENABLE)) &= ~STEP_ENABLE_MASK;
  return fifo0data;
}

void setPWM(unsigned int dutyCyclePercent, unsigned int uSecPeriod, unsigned short channel)
{
  char sysCom[256];
  if(0 == channel)
  {
    FILE * file = fopen("/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip6/pwm0/period", "w");
    sprintf(sysCom, "%d", uSecPeriod * 1000);
    fputs(sysCom, file);
    fclose(file);

    file = fopen("/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip6/pwm0/duty_cycle", "w");
    sprintf(sysCom, "%d", dutyCyclePercent * uSecPeriod * 1000 / 100);
    fputs(sysCom, file);
    fclose(file);
  }
  else if (1 == channel)
  {
    FILE * file = fopen("/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip6/pwm1/period", "w");
    sprintf(sysCom, "%d", uSecPeriod * 1000);
    fputs(sysCom, file);
    fclose(file);

    file = fopen("/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip6/pwm1/duty_cycle", "w");
    sprintf(sysCom, "%d", dutyCyclePercent * uSecPeriod * 1000 / 100);
    fputs(sysCom, file);
    fclose(file);
  }
}

#ifdef TEST_BBB_IO
// test main
int main()
{
	initIO();
	setGPIODirection(56, FALSE);
	time_t nextTime, prevTime;
	time(&prevTime);
	bool set = FALSE;
	setGPIOValue(56, FALSE);
	while(1)
	{
		time(&nextTime);
		if(1 < nextTime - prevTime)
		{
			printf("%x\n", readAIN(0));
			time(&prevTime);
		}
		setGPIOValue(56, set);
		set = !set;
	}
}
#endif

TCB getIOTask()
{
  TCB theTCB;
  theTCB.task = &ioTask;
  return theTCB;
}

void ioTask(void * data)
{
  mode = getGPIOValue(27);
  *(batteryLevelPtr) = readAIN(0) * 100 / 0xfff;
  if(TOTAL_INPUT_CHECKS == inputChecks)
  {
    inputChecks = 0;
	currentInput = '\0';
  }
  if(inputPending())
  {
    currentInput = getInput();
  }
}
