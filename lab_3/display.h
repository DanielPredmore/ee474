#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdio.h>
#include "satellite.h"

/**
 * display.h is a lite display driver
 * with window support
 *
 *
 * +--------------------+
 * | This is a test     |
 * | window.            |
 * |                    |
 * +--------------------+
 */

enum color_enum
{
	RESET = 0,
	BLACK = 30,
	RED = 31,
	GREEN = 32,
	YELLOW = 33,
	BLUE = 34,
	MAGENTA = 35,
	CYAN = 36,
	WHITE = 37
};
typedef enum color_enum color_t;


struct window_struct;

struct point_struct
{
	int x;
	int y;
};
typedef struct point_struct point_t;
typedef struct point_struct dimension_t;

struct display_struct
{
	char ** next;
	char ** current;
	color_t ** next_color;
	color_t ** current_color;
	dimension_t dim;
	bool taskQueued;
	FILE *term;
	struct window_struct *top_window;
	struct window_struct *bottom_window;
};
typedef struct display_struct display_t;

struct window_struct
{
	char ** contents;
	color_t ** colors;
	color_t color;
	point_t pos;
	dimension_t dim;
	display_t *display;
	bool boarder;
	struct window_struct *next;
	struct window_struct *last;
};
typedef struct window_struct window_t;

display_t *newDisplay(FILE *term, 
				int rows, 
				int cols);
window_t *newWindow(display_t *display,
			    bool boarder,	
				int pos_x, 
				int pos_y, 
				int dim_x,
				int dim_y);
void windowPrint(window_t *window,
				char *str,
				int x,
				int y);
void windowPrintColor(window_t *window,
				char *str,
				color_t color,
				int x,
				int y);
void windowChar(window_t *window,
				char c,
				int x,
				int y);
void windowCharColor(window_t *window,
				char c,
				color_t color,
				int x,
				int y);
void windowClear(window_t *window);
void SetTopWindow(window_t *window);
void freeWindow(window_t *window);
void freeDisplay(display_t *display);


#endif // DISPLAY_H
