#include "bbbIO.h"
#include <time.h>
#include <stdio.h>

int main()
{
	initIO();
	
	clock_t prevTime = clock();
	clock_t nextTime;

	while(1)
	{
		unsigned int period0, dutyCycle0;
		unsigned int period2, dutyCycle2;

		unsigned int prevPeriod0, prevDuty0;
		unsigned int prevPeriod2, prevDuty2;
		getPWM(&period0, &dutyCycle0, 0);
		getPWM(&period2, &dutyCycle2, 2);
		if(dutyCycle0 > 0)
			prevDuty0 = dutyCycle0;
		if(period0 > 0)
			prevPeriod0 = period0;
		if(dutyCycle2 > 0)
			prevDuty2 = dutyCycle2;
		if(period2 > 0)
			prevPeriod2 = period2;

		nextTime = clock();

		if(nextTime - prevTime > CLOCKS_PER_SEC/4)
		{
			if(period0 == 0)
			{
				printf("last period0: %d ns\n", prevPeriod0 * 10);
				printf("last frequency0: %lf Hz\n", 1000000000.0/((double)(prevPeriod0) * 10.0));
			}
			else
			{
				printf("period0: %d ns\n", period0 * 10);				
				printf("frequency0: %lf Hz\n", 1000000000.0/((double)(period0) * 10.0));
			}
			if(dutyCycle0 == 0)
				printf("last duty0: %d ns\n", prevDuty0 * 10);
			else
				printf("duty cycle0: %d ns\n", dutyCycle0);


			if(period2 == 0)
			{
				printf("last period2: %d ns\n", prevPeriod2 * 10);
				printf("last frequency2: %lf Hz\n", 1000000000.0/((double)(prevPeriod2) * 10.0));
			}
			else
			{
				printf("period2: %d ns\n", period2 * 10);				
				printf("frequency2: %lf Hz\n", 1000000000.0/((double)(period2) * 10.0));
			}

			if(dutyCycle2 == 0)
				printf("last duty2: %d ns\n", prevDuty2);
			else
				printf("duty cycle2: %d ns\n", dutyCycle2);

			prevTime = nextTime;
		}
	}
	
	return 0;
	
}
