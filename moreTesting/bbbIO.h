#ifndef BBB_IO_H
#define BBB_IO_H

#include "satellite.h"

// GPIO PIN BIT LOCATIONS
  // USRx LEDs are the 21st, 22nd, 23rd, and 24th LSBs of GPIO1
#define USR0 53
#define USR1 54
#define USR2 55
#define USR3 56

unsigned int batteryLvl;
unsigned int batteryTemp1;
unsigned int batteryTemp2;
unsigned int ch0M, ch1M, ch2M;

void initIO();
void readAIN();
void setGPIOValue(unsigned int gpioNumber, unsigned int value);
unsigned int getGPIOValue(unsigned int gpioNumber);
void setPWM(unsigned int dutyCyclePercent, unsigned int uSecPeriod, unsigned int channel);
void getPWM(unsigned int * period, unsigned int * dutyCycle, unsigned int channel);
TCB getIOTask();

#endif
