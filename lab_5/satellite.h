#ifndef SATELLITE_H
#define SATELLITE_H

/**
 * This header file contains the globally available variables.
 */

// defining default values
#define NO_COMMAND 0x0000
#define FULL 100
#define EMPTY 0
#define BATTERY_LOW 10
#define FUEL_LOW 10
#define MODE_PIN 27
#define TOTAL_INPUT_CHECKS 1
#define DATA_SIZE 8

// defining the TCB struct
struct _satelliteKernelTCB {
		void (*task)(void*);
		void* taskDataPtr;
		char *pName;
};
typedef struct _satelliteKernelTCB TCB;

// Defining a bool
enum boolean {
		FALSE = 0,
		TRUE = 1
};
typedef enum boolean bool;

// defining circular array
struct circularArray
{
	double array[DATA_SIZE];	
	double * head; 
	double * tail; 
};
typedef struct circularArray circularArray; 

// Pirate Management Task
unsigned int pirateProximity;

// Thruster Control
unsigned int thrusterCommand;

// Power Management
unsigned int * batteryLevelPtr;
unsigned int * batteryTempPtr;
// unsigned short batteryLevel;
unsigned short fuelLevel;
unsigned short powerConsumption;
unsigned short powerGeneration;

// Solar Panel Control
bool solarPanelState;
bool solarPanelDeploy; 
bool solarPanelRetract;
bool overTemperature;
bool driveMotorSpeedInc; 
bool driveMotorSpeedDec;

// Warning Alarm
bool fuelLow;
bool batteryLow;
bool overTemp;

// Vehicle Communication
char command; 
char response; 

// Display
unsigned int mode; 

// For key input
char currentInput;
unsigned short inputChecks;
char gameCommand;

// Transport Distance
circularArray distances;
unsigned int transportPeriod;
unsigned int transportDutyCycle;
TCB tdTCB; 

// Image Capture
double imageFrequency;
bool startImageCapture; 
bool loadImageData;

// Command Task
bool inputError; 
bool displayHide;

// interrupt flags
bool transportSignal;
bool fullyDeployed;
bool fullyRetracted;
bool measureTemp;
bool measureBatt;
bool pirateDetected;
bool commandRunning;
bool pirateDestroyed;

// interrupt acknowledgment flags
bool transportSignalAck;
bool fullyDeployedAck;
bool fullyRetractedAck;
bool measureTempAck;
bool pirateDetectAck;
bool startImageAck;
bool commandRunningAck;

// dynamic process IDs
signed int transportDistPid;
signed int batteryTempReadPid;
signed int driveMotorPid;
signed int solarPanelPid;
signed int keypadPid;
signed int imageCaptPid;
signed int pirateManPid;
signed int commandPid;
signed int gamePid;

// current duty cycle of pwm signal to motor
unsigned int driveMotorSpeed;

// image data frequency buffer
double freqBuffer[16];

// initializes global variables
void initializeGlobals();

#endif // SATELLITE_H
