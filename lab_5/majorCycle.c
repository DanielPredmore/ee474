#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "majorCycle.h"
#include "kernel.h"
#include "display.h"
#include "powerSubsystem.h"
#include "satelliteComms.h"
#include "thrusterSubsystem.h"
#include "vehicleComms.h"


struct majorCycle_struct
{
	int powerSubsystem;
	int satelliteComms;
	int thrusterSubsystem;
	//int vehicleComms;
};
typedef struct majorCycle_struct majorData;

void majorCycle(void *data);

TCB getMajorCycle(window_t *earth, window_t *vehicle, window_t *vehicle2, unsigned int *channel0, unsigned int * channel1)
{
	majorData *data = (majorData *)malloc(sizeof(majorData));
	data->powerSubsystem = addTask(getPowerSubsystem(), 6, 0);
	data->satelliteComms = addTask(getSatelliteComms(earth, vehicle), 4, 0);
	data->thrusterSubsystem = addTask(getThrusterSubsystem(), 6, 0);
	//data->vehicleComms = addTask(getVehicleComms(vehicle, vehicle2, channel0, channel1), 5, 0);
	TCB tcb;
	tcb.taskDataPtr = data;
	tcb.task = majorCycle;
	tcb.pName = "Major Cycle Start";
	return tcb;
}

void majorCycle(void *data)
{
	majorData * mData = (majorData *)data;
	wakeTask(mData->powerSubsystem);
	wakeTask(mData->satelliteComms);
	wakeTask(mData->thrusterSubsystem);
	//wakeTask(mData->vehicleComms);
}
