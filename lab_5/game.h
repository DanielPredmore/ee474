#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include "satellite.h"
#include "display.h"

#define MAX_ATTACKS 5

struct game_struct;
struct player_struct;
struct action_struct;

void (*action_task)(struct action_struct *action, struct player_struct *us, struct player_struct *them);

struct action_struct
{
	char *title;
	int damage;
	int power;
	int defence;
	int heal;
	int drain;
	int chance;
};
typedef struct action_struct action_t;

struct player_struct
{
	char * name;
	char ** ascii;
	int attack_c;
	action_t attacks[MAX_ATTACKS];
	int hp;
	int power;
	int defence;
};
typedef struct player_struct player_t;
	
struct game_struct
{
	player_t *player;
	player_t *npc;
	bool gameover;
};
typedef struct game_struct game_t;

TCB getGame(window_t *statusWindow, window_t *displayWinow, window_t *commandWindow, window_t *eventWindow);

#endif // GAME_H
