#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "timeWindow.h"

struct timeWindow_struct
{
	window_t *window;
	int x;
	color_t color;
	time_t then;
};
typedef struct timeWindow_struct timeData;

void timeWindow(void *data);

TCB getTimeWindow(window_t * window, color_t color, int offset)
{
	timeData *data = (timeData *)malloc(sizeof(timeData));
	TCB tcb;
	tcb.taskDataPtr = data;
	data->window = window;
	data->x = offset;
	data->color = color;
	data->then = 0;
	tcb.task = timeWindow;
	tcb.pName = "Time Window";
	return tcb;
}

void timeWindow(void *data)
{
	timeData *tData = (timeData *)data;
	window_t *window = tData->window;
	time_t now;
  	struct tm * now_tm;

	time(&now);
	if (now != tData->then)
	{
  		now_tm = localtime(&now);
		windowPrintColor(window, asctime(now_tm), tData->color, 0, tData->x);
		tData->then = now;
	}
}
