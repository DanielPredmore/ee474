#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdio.h>
#include "satellite.h"

/**
 * display.h is a lite display driver
 * with window support
 *
 *
 * +--------------------+
 * | This is a test     |
 * | window.            |
 * |                    |
 * +--------------------+
 */

enum color_enum
{
	RESET = 7,
	BLACK = 0,
	RED = 1,
	GREEN = 2,
	YELLOW = 3,
	BLUE = 4,
	MAGENTA = 5,
	CYAN = 6,
	WHITE = 7,
	GREAY = 8,
	BRIGHT_RED = 9,
	BRIGHT_GREEN = 10,
	BRIGHT_YELLOW = 11,
	BRIGHT_BLUE = 12,
	BRIGHT_MAGENTA = 13,
	BRIGHT_CYAN = 14,
	BRIGHT_WHITE = 15
};
typedef enum color_enum color_t;
typedef enum color_enum background_t;

struct window_struct;

struct point_struct
{
	int x;
	int y;
};
typedef struct point_struct point_t;
typedef struct point_struct dimension_t;

struct display_struct
{
	char ** next;
	char ** current;
	color_t ** next_color;
	color_t ** current_color;
	background_t ** next_background;
	background_t ** current_background;
	dimension_t dim;
	FILE *term;
	struct window_struct *top_window;
	struct window_struct *bottom_window;
	bool hidden;
	int pid;
};
typedef struct display_struct display_t;

struct window_struct
{
	char ** contents;
	color_t ** colors;
	background_t ** backgrounds;
	color_t color;
	background_t background;
	point_t pos;
	dimension_t dim;
	display_t *display;
	bool boarder;
	bool setBack;
	bool hidden;
	struct window_struct *next;
	struct window_struct *last;
};
typedef struct window_struct window_t;

display_t *newDisplay(FILE *term, 
				int rows, 
				int cols);
window_t *newWindow(display_t *display,
			    bool boarder,	
				int pos_x, 
				int pos_y, 
				int dim_x,
				int dim_y);
void windowPrint(window_t *window,
				char *str,
				int x,
				int y);
void windowPrintColor(window_t *window,
				char *str,
				color_t color,
				int x,
				int y);
void windowPrintBackground(window_t *window,
				char *str,
				color_t color,
				background_t background,
				int x,
				int y);
void windowChar(window_t *window,
				char c,
				int x,
				int y);
void windowCharColor(window_t *window,
				char c,
				color_t color,
				int x,
				int y);
void windowSetColor(window_t *window, color_t color);
void windowSetBackground(window_t *window, background_t background);
void windowDrawBackground(window_t *window,
				background_t background,
				int start_x,
				int start_y,
				int end_x,
				int end_y);
void windowSetBoarder(window_t * window,
				color_t color,
				background_t background,
				char vertical,
				char horizontal,
				char corner);
void windowColorBoarder(window_t * window,
				color_t color,
				background_t background);
void windowClear(window_t *window);
void windowSetHide(window_t *window, bool hidden);
void displaySetHide(display_t *display, bool hidden);
void SetTopWindow(window_t *window);
void freeWindow(window_t *window);
void freeDisplay(display_t *display);


#endif // DISPLAY_H
