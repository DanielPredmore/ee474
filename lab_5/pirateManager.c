#include "pirateManager.h"
#include <stdlib.h>
#include "kernel.h"

#define DANGER_SAFE 100
#define DANGER_CLOSE_PHASER 30
#define DANGER_CLOSE_PHOTON 5

struct pirateManagerData
{
	bool phaserEn;
	bool photonEn;
};
typedef struct pirateManagerData dataStruct;

void pirateManager(void * data);

TCB getPirateManager()
{
	TCB theTCB;

	dataStruct * data = (dataStruct *) malloc(sizeof(dataStruct));

	data->phaserEn = FALSE;
	data->photonEn = FALSE;

	theTCB.task = &pirateManager;
	theTCB.pName = "Pirate Man.";
	theTCB.taskDataPtr = data;

	return theTCB;
}

void pirateManager(void * data)
{
	dataStruct * pmD = (dataStruct *) data;
	
	
	if(pirateProximity <= DANGER_SAFE)
	{
		if(!pirateDestroyed)
			wakeTask(gamePid);
		if(pirateProximity <= DANGER_CLOSE_PHASER)
		{
			if(pirateProximity <= DANGER_CLOSE_PHOTON)
				pmD->photonEn = TRUE;
			else
				pmD->photonEn = FALSE;
	   		pmD->phaserEn = TRUE;
		}
		else
		{
			pmD->phaserEn = FALSE;
			pmD->photonEn = FALSE;
			if(pirateProximity >= 100 || pirateDestroyed)
			{
				pirateDetectAck = FALSE;
				sleepTask();
			}
		}
	}
}
