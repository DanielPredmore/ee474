#ifndef TRANSPORT_DISTANCE_H
#define TRANSPORT_DISTANCE_H

#include "satellite.h"
#include "transportDistance.h"

TCB getTransportDistance(); 
void transportDistance(void* data); 

#endif // TRANSPORT_DISTANCE_H
