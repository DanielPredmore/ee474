#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include "display.h"
#include "kernel.h"
#include "satellite.h"

#define CLEAR_STRING "\033[2J"
// 17ms delay
#define UPDATE_DELAY (17*CLOCKS_PER_SEC/1000)

void windowRender(window_t *window);
void yankWindow(window_t *window);
char ** buildStringBlock(unsigned int rows, unsigned int cols);
color_t ** buildColorBlock(unsigned int rows, unsigned int cols, color_t color);
void queueUpdate(display_t * display);
void displayUpdate(void *data);

/**
 * This function builds a new display space.
 * The display space can be used to hold windows that contain content.
 * The display will render to a preset size given by the rows and cols values.
 * The output will be displayed on the terminal passed in.
 *
 * @param term the terminal to display on
 * @param rows the number of rows to display
 * @param cols the number of columns to display
 * @return the new display object
 */
display_t *newDisplay(FILE *term,
				int rows,
				int cols)
{
	display_t *display = (display_t *)malloc(sizeof(display_t));
	display->term = term;
	display->next = buildStringBlock(rows, cols);
	display->current = buildStringBlock(rows, cols);
	display->next_color = buildColorBlock(rows, cols, WHITE);
	display->current_color = buildColorBlock(rows, cols, WHITE);
	display->next_background = (background_t **) buildColorBlock(rows, cols, -1);
	display->current_background = (background_t **) buildColorBlock(rows, cols, -1);
	display->dim.x = rows;
	display->dim.y = cols;
	display->top_window = NULL;
	display->hidden = FALSE;
	display->bottom_window = NULL;
	fprintf(display->term, CLEAR_STRING);
	fprintf(display->term, "\033[%d;0H", rows + 1);
	fprintf(display->term, "\033[?25l");
	fflush(display->term);
	display->pid = -1;
	return display;
}

/**
 * This function creates a new window on the given display space.
 * Windows are a child of a display space and will be rendered to their given
 * display space.
 * The window can be bordered or non bordered and is used to display text.
 * 
 * All new windows will be placed on top of the window stack
 *
 * @param display the display the window will be on
 * @param boarder set the boarder on or off
 * @param pos_x the row of the window's content (not boarder) on the display
 * @param pos_y the column of the window's content on the display
 * @param dim_x the number of content rows (not including boarder)
 * @param dim_y the number of content columns (not including boarder)
 * @return the new window
 */
window_t *newWindow(display_t *display,
				bool boarder,	
				int pos_x, 
				int pos_y, 
				int dim_x,
				int dim_y)
{
	if (display == NULL)
		return NULL;
	int px = boarder ? pos_x - 1: pos_x;
	int py = boarder ? pos_y - 1: pos_y;
	int dx = boarder ? dim_x + 2: dim_x;
	int dy = boarder ? dim_y + 2: dim_y;
	window_t *window = (window_t *)malloc(sizeof(window_t));
	window->contents = buildStringBlock(dx, dy);
	window->colors = buildColorBlock(dx, dy, WHITE);
	window->backgrounds = (background_t **) buildColorBlock(dx, dy, BLACK);
	window->pos.x = px;
	window->pos.y = py;
	window->dim.x = dx;
	window->dim.y = dy;
	window->color = RESET;
	window->background = BLACK;
	window->setBack = FALSE;
	window->hidden = FALSE;
	window->display = display;
	window->boarder = boarder;
	window->next = NULL;

	if (window->display->top_window == NULL)
	{
		window->last = NULL;
		display->bottom_window = window;
	}
	else
	{
		window->last = display->top_window;
		display->top_window->next = window;
	}
	display->top_window = window;


	if (boarder)
	{
		window->contents[0][0] = '+';
		window->contents[0][dy-1] = '+';
		window->contents[dx-1][0] = '+';
		window->contents[dx-1][dy-1] = '+';

		int i;
		for (i = 1; i < dx - 1; i++)
		{
			window->contents[i][0] = '|';
			window->contents[i][dy-1] = '|';
		}
		for (i = 1; i < dy - 1; i++)
		{
			window->contents[0][i] = '-';
			window->contents[dx-1][i] = '-';
		}
	}

	queueUpdate(window->display);
	return window;
}

/**
 * This function prints a string onto a window at a given start location.
 * If a string over flows, it will clip onto the next line.
 *
 * @param window the window being printed to
 * @param str the string being printed
 * @param x the start row
 * @param y the start column
 */
void windowPrint(window_t *window,
				char * str,
				int x,
				int y)
{
	int i;
	int d = window->boarder ? 1: 0;
	for (i = 0; str[i] != '\0'; i++)
	{
	 	if (x + d >= window->dim.x - d)
	 		return;
	 	if (str[i] != '\n' && i + y + d < window->dim.y - d)
	 	{
	 		window->contents[x + d][i + y + d] = str[i];
			window->colors[x + d][i + y + d] = window->color;
			if (window->setBack)
					window->backgrounds[x + d][i + y + d] = window->background;
	 	}
	 	else
	 	{
	 		if (str[i] != '\n')
	 			i--;
	 		x++;
	 		y = 0;
	 	}
	}
	queueUpdate(window->display);
}

/**
 * This function prints a string just like the windowPrint function.
 * However this function has the option for color.
 *
 * @param window the window being printed to
 * @param str the string being printed
 * @param color the color being used
 * @param x the start row
 * @param y the start column
 */
void windowPrintColor(window_t *window,
				char *str,
				color_t color,
				int x,
				int y)
{
	color_t orginal = window->color;
	window->color = color;
	windowPrint(window, str, x, y);
	window->color = orginal;
}

void windowPrintBackground(window_t * window,
				char *str,
				color_t color,
				background_t background,
				int x,
				int y)
{
	window->setBack = TRUE;
	background_t original = window->background;
	window->background = background;
	windowPrintColor(window, str, color, x, y);
	window->background = original;
	window->setBack = FALSE;
}

/**
 * This function prints a single char to a given window.
 * If the char is out of bounds, nothing will be changed.
 *
 * @param window the window being used
 * @param c the char being displayed
 * @param x the row
 * @param y the column
 */
void windowChar(window_t *window,
				char c,
				int x,
				int y)
{
	queueUpdate(window->display);
	int d = window->boarder ? 1: 0;
    x = x + d;
    y = y + d;
    if (x > 0 && x < window->display->dim.x - d && y > 0 && y < window->display->dim.y)
	{
    	window->contents[x][y] = c;
		window->colors[x][y] = window->color;
		window->backgrounds[x][y] = window->background;
	}
}

/**
 * The is function prints a single char in a given color
 *
 * @param window the window being used
 * @param c the char being displayed
 * @param color the color being used
 * @param x the row
 * @param y the column
 */
void windowCharColor(window_t *window,
				char c,
				color_t color,
				int x,
				int y)
{
	color_t original = window->color;
	window->color = color;
	windowChar(window, c, x, y);
	window->color = original;
}

void windowSetColor(window_t *window, color_t color)
{
	color_t original = window->color;
	int d = window->boarder ? 1 : 0;
	int i, j;
	for (i = d; i < window->dim.x - d; i++)
	{
		for (j = d; j < window->dim.y - d; j++)
		{
			if (window->colors[i][j] == original)
			{
				window->colors[i][j] = color;
			}
		}
	}
	window->color = color;
	queueUpdate(window->display);
}

void windowSetBackground(window_t *window, background_t background)
{
	background_t original = window->background;
	int d = window->boarder ? 1 : 0;
	int i, j;
	for (i = d; i < window->dim.x - d; i++)
	{
		for (j = d; j < window->dim.y - d; j++)
		{
			if (window->backgrounds[i][j] == original)
			{
				window->backgrounds[i][j] = background;
			}
		}
	}
	window->background = background;
	queueUpdate(window->display);
}

void windowDrawBackground(window_t *window,
				background_t background,
				int start_x,
				int start_y,
				int end_x,
				int end_y)
{
	int d = window->boarder ? 1: 0;
	int i, j;
	for (i = start_x + d; i < end_x + d && i < window->dim.x - d; i++)
	{
		for (j = start_y + d; j < end_y + d && i < window->dim.y - d; j++)
		{
			window->backgrounds[i][j] = background;
		}
	}
	queueUpdate(window->display);
}

void windowSetBoarder(window_t * window,
				color_t color,
				background_t background,
				char vertical,
				char horizontal,
				char corner)
{
	if (!window->boarder)
			return;

	int dx = window->dim.x;
	int dy = window->dim.y;

	window->contents[0][0] = corner;
	window->contents[0][dy-1] = corner;
	window->contents[dx-1][0] = corner;
	window->contents[dx-1][dy-1] = corner;

	int i;
	for (i = 1; i < dx - 1; i++)
	{
		window->contents[i][0] = vertical;
		window->contents[i][dy-1] = vertical;
	}
	for (i = 1; i < dy - 1; i++)
	{
		window->contents[0][i] = horizontal;
		window->contents[dx-1][i] = horizontal;
	}
	windowColorBoarder(window, color, background);
}

void windowColorBoarder(window_t * window,
				color_t color,
				background_t background)
{
	if (!window->boarder)
			return;
	int i;
	for (i = 0; i < window->dim.x; i++)
	{
		window->colors[i][0] = color;
		window->colors[i][window->dim.y - 1] = color;
		window->backgrounds[i][0] = background;
		window->backgrounds[i][window->dim.y - 1] = background;
	}
	for (i = 0; i < window->dim.y; i++)
	{
		window->colors[0][i] = color;
		window->colors[window->dim.x - 1][i] = color;
		window->backgrounds[0][i] = background;
		window->backgrounds[window->dim.x - 1][i] = background;
	}
	queueUpdate(window->display);
}

/**
 * This function clears the content of a window
 *
 * @param window the window to be cleared
 */
void windowClear(window_t *window)
{
	int d = window->boarder ? 1: 0;
	int i, j;
	for (i = d; i < window->dim.x - d; i++)
	{
		for (j = d; j < window->dim.y - d; j++)
		{
			window->contents[i][j] = '\0';
			window->colors[i][j] = window->color;
			window->backgrounds[i][j] = window->background;
		}
	}
	queueUpdate(window->display);
}

void windowSetHide(window_t *window, bool hidden)
{
	window->hidden = hidden;
	queueUpdate(window->display);
}

void displaySetHide(display_t *display, bool hidden)
{
	if (hidden && !display->hidden)
	{
		int i, j;
		for (i = 0; i < display->dim.x; i++)
		{
			for (j = 0; j < display->dim.y; j++)
				{
					display->current[i][j] = '\0';
					display->current_color[i][j] = WHITE;
					display->current_background[i][j] = -1;
				}
		}
		fprintf(display->term, CLEAR_STRING);
		fprintf(display->term, "\033[1;1H");
		fprintf(display->term, "\033[?25h");
		fprintf(display->term, "\1");
		fflush(display->term);
	}
	else if (!hidden && display->hidden)
	{
		fprintf(display->term, CLEAR_STRING);
		//fprintf(display->term, "\033[%d;0H", display->dim.x + 1);
		fprintf(display->term, "\033[?25l");
		//fprintf(display->term, "\1");
		//fflush(display->term);
		queueUpdate(display);
	}
	display->hidden = hidden;
}


/**
 * This function sets the given window to the top of the window stack
 *
 * @param window the window being put on top
 */
void setTopWindow(window_t *window)
{
	if (window->display->top_window == window)
		return;
	yankWindow(window);
	window->next = NULL;
	window->last = window->display->top_window;
	window->display->top_window->next = window;
	window->display->top_window = window;
	queueUpdate(window->display);
}

/**
 * This function deletes a window
 *
 * @param window the window to be deleted
 */
void freeWindow(window_t *window)
{
	yankWindow(window);
	int i = 0;
	for (i = 0; i < window->dim.x; i++)
	{
		free(window->contents[i]);
		free(window->colors[i]);
		free(window->backgrounds[i]);
	}
	free(window->contents);
	free(window->colors);
	free(window->backgrounds);
	queueUpdate(window->display);
	free(window);
}

/**
 * This function deletes a display space and all of its windows
 *
 * @param display the display to be removed
 */
void freeDisplay(display_t *display)
{
	window_t *current, *next;
	for (current = display->bottom_window; current != NULL; current = next)
	{
		next = current->next;
		freeWindow(current);
	}

	int i;
	for (i = 0; i < display->dim.x; i++)
	{
		free(display->current[i]);
		free(display->next[i]);
		free(display->current_color[i]);
		free(display->next_color[i]);
		free(display->current_background[i]);
		free(display->next_background[i]);
	}
	free(display->current);
	free(display->next);
	free(display->current_color);
	free(display->next_color);
	free(display->current_background);
	free(display->next_background);

	fprintf(display->term, CLEAR_STRING);
	fprintf(display->term, "\033[1;1H");
	fprintf(display->term, "\033[?25h");
	fclose(display->term);

	free(display);
}

/**
 * This function renders all windows form the given window to the top of the 
 * window stack.
 * Rendered data is set the window's display's next map
 *
 * @param window the start window of the render process
 */
void windowRender(window_t *window)
{
	window_t *current;
	for (current = window; current != NULL; current = current->next)
	{
		if(!window->hidden)
		{
			int i, j;
			for (i = 0; i < current->dim.x; i++)
			{
				int x = i + current->pos.x;
				if (x >= 0 && x < current->display->dim.x)
				{
					for (j = 0; j < current->dim.y; j++)
					{
						int y = j + current->pos.y;
						if (y >= 0 && y < current->display->dim.y)
						{
							current->display->next[x][y] = current->contents[i][j];
							current->display->next_color[x][y] = current->colors[i][j];
							current->display->next_background[x][y] = current->backgrounds[i][j];
						}
					}
				}
			}
		}
	}
}

/**
 * This function pulls a window out of the display stack
 *
 * @param window the window to be unlinked
 */
void yankWindow(window_t * window)
{
	if (window->display->top_window == window)
	{
		window->display->top_window == window->last;
		if (window->last == NULL)
		{
			window->display->bottom_window = NULL;
			return;
		}
		window->last->next = NULL;
		return;
	}
	window->next->last = window->last;
	if (window->last != NULL)
	{
		window->last->next = window->next;
	}
	else
	{
		window->display->bottom_window = window->next;
	}
}

/**
 * this function builds a 2D char ** array and sets all the values to null
 *
 * @param x the x size
 * @param y the y size
 * @return the 2D char array
 */
char ** buildStringBlock(unsigned int x, unsigned int y)
{
	char ** data = (char **)malloc(sizeof(char *) * x);
	int i, j;
	for (i = 0; i < x; i++)
	{
		data[i] = (char*)malloc(sizeof(char) * (y + 1));
		for (j = 0; j <= y; j++)
		{
			data[i][j] = '\0';
		}
	}
	return data;
}

color_t ** buildColorBlock(unsigned int x, unsigned int y, color_t color)
{
	color_t ** data = (color_t **)malloc(sizeof(color_t *) * x);
	int i, j;
	for (i = 0; i < x; i++)
	{
		data[i] = (color_t *)malloc(sizeof(color_t) * (y + 1));
		for (j = 0; j <= y; j++)
		{
			data[i][j] = color;
		}
	}
	return data;
}

/**
 * This schedules the print task on the queue if one is not already scheduled
 *
 * @param display the display to be printed
 */
void queueUpdate(display_t *display)
{
	if (display->pid == -1)
	{
		TCB tcb;
		tcb.taskDataPtr = display;
		tcb.task = displayUpdate;
		tcb.pName = "Display Update";
		int pid = addTask(tcb, 2, UPDATE_DELAY);
		display->pid = pid;
	}
	else
		wakeTask(display->pid);
}

/**
 * This is the print task for the display.
 * It will only print diffs between the current and next maps.
 * The cursor will keep its position between updates
 *
 * @param data the display data
 */
void displayUpdate(void *data)
{
	display_t *display = (display_t *)data;
	if (display->hidden)
	{
		sleepTask();
		return;
	}
	windowRender(display->bottom_window);
	int current_x = -1;
	int current_y = -1;
	int current_color = -1;
	int current_background = -1;
	int i, j;
	fprintf(display->term, "\033[s");
	//fprintf(display->term, "\033[?25l");
	for (i = 0; i < display->dim.x; i++)
	{
		for (j = 0; j < display->dim.y; j++)
		{
			if (display->current[i][j] != display->next[i][j] || 
				display->current_color[i][j] != display->next_color[i][j] ||
				display->current_background[i][j] != display->next_background[i][j])
			{
				if (current_x != i || current_y != j)
				{
					fprintf(display->term, "\033[%d;%dH", i + 1, j + 1);
					current_x = i;
					current_y = j;
				}
				
				if (current_color != display->next_color[i][j])
				{
					fprintf(display->term, "\033[38;5;%dm", display->next_color[i][j]);
					current_color = display->next_color[i][j];
				}
				if (current_background != display->next_background[i][j])
				{
					fprintf(display->term, "\033[48;5;%dm", display->next_background[i][j]);
					current_background = display->next_background[i][j];
				}

				char c = display->next[i][j];
				if (isspace((int)c) || c == '\0')
				{
					display->next[i][j] = ' ';
				}
				fprintf(display->term, "%c", display->next[i][j]);
				display->current[i][j] = display->next[i][j];
				display->current_color[i][j] = display->next_color[i][j];
				display->current_background[i][j] = display->next_background[i][j];
				current_y++;
			}
		}
	}
	if (current_color != RESET)
			fprintf(display->term, "\033[0m");
	fprintf(display->term, "\033[u");
	//fprintf(display->term, "\033[?25h");
	fflush(display->term);
	sleepTask();
}
