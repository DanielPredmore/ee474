#include "satellite.h"
#include "satelliteComms.h"
#include "keypad.h"
#include "display.h"
#include "bbbIO.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "kernel.h"
#include "styleSheet.h"

#define BUFFER_LENGTH 100

// global variables
extern bool fuelLow;
extern bool batteryLow;
extern bool solarPanelState; 
extern bool solarPanelDeploy;
extern bool solarPanelRetract;
extern bool driveMotorSpeedInc;
extern bool driveMotorSpeedDec;
extern unsigned int * batteryLevelPtr; 
extern unsigned int * batteryTempPtr;
extern unsigned short fuelLevel; 
extern unsigned short powerConsumption; 
extern unsigned short powerGeneration; 
extern unsigned int thrusterCommand;
extern char command; 
extern char response;

// data struct for the satellite communication
struct satelliteCommsData
{
	bool * fuelLow;
	bool * batteryLow;
	bool * solarPanelState; 
	bool * solarPanelDeploy;
	bool * solarPanelRetract;
	bool * driveMotorSpeedInc;
	bool * driveMotorSpeedDec;
	unsigned int * batteryLevelPtr; 
	unsigned int * batteryTempPtr;
	unsigned short * fuelLevel; 
	unsigned short * powerConsumption; 
    unsigned short * powerGeneration;
	unsigned int * thrusterCommand;
	char * command; 
	char * response;
    unsigned int * channel; 
	window_t * window;
	window_t * vWindow;	
};
typedef struct satelliteCommsData dataStruct; 

// function prototypes
void satelliteComms(void *data);

/**
 * This function makes a new TCB task for the kernel
 * 
 * @returns the TCB for satelliteComms
 **/
TCB getSatelliteComms(window_t * window, window_t * vWindow)
{
	TCB theTCB; 
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 
	
	// initilizing data struct	
	(data->fuelLow) = &fuelLow;
	(data->batteryLow) = &batteryLow;
	(data->solarPanelState) = &solarPanelState;
    (data->solarPanelDeploy) = &solarPanelDeploy;
    (data->solarPanelRetract) = &solarPanelRetract;	
	(data->batteryLevelPtr) = batteryLevelPtr;
	(data->batteryTempPtr) = batteryTempPtr;
	(data->driveMotorSpeedInc) = &driveMotorSpeedInc;
	(data->driveMotorSpeedDec) = &driveMotorSpeedDec;
   	(data->fuelLevel) = &fuelLevel; 
	(data->powerConsumption) = &powerConsumption; 
	(data->powerGeneration) = &powerGeneration; 
	(data->thrusterCommand) = &thrusterCommand;
	(data->command) = &command; 
	(data->response) = &response; 
	(data->window) = window;
	(data->vWindow) = vWindow;

	// initialize random seed
	//srand (time(NULL));

	// setting the values in the TCB
	theTCB.task = &satelliteComms; 
	theTCB.taskDataPtr = data; 
	theTCB.pName = "Satellite Comms";

	return theTCB; 
}

/**
 * This function sends random commands to thruster subsystems and send data onto the console
 * 
 * @param data is the data struct for satellite comms
 */
void satelliteComms(void *data)
{
	
	// keeps track of previous values
	bool prevFuelLow = fuelLow;
	bool prevBatteryLow = batteryLow;
	
	// alerts when battery or fuel is low
	int alert = (~prevFuelLow && fuelLow) || (~prevBatteryLow && batteryLow);
	
	// holds print statements
	char buffer[BUFFER_LENGTH];

	// command array
	//char commands[] = {'F','B','L','R','D','H'};
	
	// cast void point
	dataStruct *commsData = (dataStruct*) data; 
	
	// assigns random 16-bit number to thrusterCommand
	//*(commsData->thrusterCommand) = (unsigned int) rand() % 65536; 	
	
	// assigns random command to vehicle communications
	//unsigned int index = (unsigned int) rand() % 6;
	//*(commsData->command) = commands[index];
		
	windowClear(commsData->window); // clears window
	
	// EARTH DATA TRANSFER	

		color_t color = TEXT_DEFAULT; 
		
		windowPrintColor(commsData->window, "Satellite: Buttnik", HEADER, 1, 2);  
		windowPrintColor(commsData->window, "Operator's Name: Dr. Prof. Douglas", HEADER, 2, 2); 
		

	/*	// hide display
		if (*commsData->display == TRUE) 
		{
			displaySetHide(commsData->window->display, TRUE);
		}
		else 
		{
			displaySetHide(commsData->window->display, FALSE); 
		}*/


		// print status mode
		windowPrintColor(commsData->window, "STATUS MODE:", HEADER, 5, 2);
		if(driveMotorSpeedInc)
			sprintf(buffer, "Solar Panel State = %d+\0", solarPanelState);
		else if (driveMotorSpeedDec)
			sprintf(buffer, "Solar Panel State = %d-\0", solarPanelState);
		else
			sprintf(buffer, "Solar Panel State = %d\0", solarPanelState);
		color = TEXT_DEFAULT;
		if (!(solarPanelDeploy || solarPanelRetract))
			color = STABLE_COLOR;
		else if (solarPanelDeploy)
			color = DEPLOY_COLOR;
		else if (solarPanelRetract)
			color = RETRACT_COLOR;
		windowPrintColor(commsData->window, buffer, color, 6, 4);
		sprintf(buffer, "Battery Level     = %d\0", *batteryLevelPtr); 
		color = FULL_COLOR;
		if (*batteryLevelPtr < 50)
				color = MID_COLOR;
		if (*batteryLevelPtr < 10)
				color = LOW_COLOR;
		windowPrintColor(commsData->window, buffer, color, 7, 4); 
		sprintf(buffer, "Battery Temp      = %d\0", batteryTempPtr[0]);
		color = FULL_COLOR;
		if (batteryTempPtr[0] > 50)
				color = MID_COLOR;
		if (batteryTempPtr[0] > 90)
				color = LOW_COLOR;
		windowPrintColor(commsData->window, buffer, color, 8, 4); 
		sprintf(buffer, "Fuel Level        = %d\0", fuelLevel);
		color = FULL_COLOR;
		if (fuelLevel < 50)
				color = MID_COLOR;
		if (fuelLevel < 10)
				color = LOW_COLOR;
		windowPrintColor(commsData->window, buffer, color, 9, 4); 
		sprintf(buffer, "Power Consumption = %d\0", powerConsumption); 
		color = DATA_COLOR;
		if (powerConsumption > 0)
				color = MID_COLOR;
		if (powerConsumption > 5)
				color = LOW_COLOR;
	   	windowPrintColor(commsData->window, buffer, color, 10, 4); 
		sprintf(buffer, "Power Generation  = %d\0", powerGeneration); 
		color = MID_COLOR;
		if (powerGeneration > powerConsumption)
				color = FULL_COLOR;
		if (powerGeneration == 0 || powerGeneration == powerConsumption)
				color = DATA_COLOR;
		windowPrintColor(commsData->window, buffer, color, 11, 4);
		
		// print annunciation mode to earth
		windowPrintColor(commsData->window, "ANNUNCIATION MODE:\0", HEADER, 5, 52);
		color = NORMAL_COLOR;
		if (fuelLow)
			color = WARNING_COLOR;
	   	sprintf(buffer, "Fuel Low    = %d \0", fuelLow); 				
		windowPrintColor(commsData->window, buffer, color, 6, 54);
	   	color = NORMAL_COLOR;
		if (batteryLow)
			color = WARNING_COLOR;
		sprintf(buffer, "Battery Low = %d \0", batteryLow); 
		windowPrintColor(commsData->window, buffer, color, 7, 54);  
		sprintf(buffer, "Over Temp   = %d \0", overTemperature); 
		color = NORMAL_COLOR;
		if (overTemperature)
			color = WARNING_COLOR;
		windowPrintColor(commsData->window, buffer, color, 8, 54);  
		sleepTask();
		
}
