#ifndef KERNEL_H
#define KERNEL_H

/**
 * This is the header file for the satellite's kernel
 */

#include <time.h>
#include "satellite.h"

void initKernel();
// This function starts the kernel
void kernelStart();
int addTask(TCB tcb, unsigned int priority, clock_t wait);
void endTask();
void sleepTask();
void wakeTask(int pid);
void sleepOther(int pid);

#endif // KERNEL_H
