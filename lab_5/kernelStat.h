#ifndef KERNEL_STAT_H
#define KERNEL_STAT_H

#include "display.h"
#include "satellite.h"

TCB getKernelStat(window_t *window);

#endif // KERNEL_STAT_H
