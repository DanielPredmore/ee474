#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include "keypad.h"

#define NUM_THREAD 2
#define CHAR sizeof(char)

// prototypes 
void receiveData(void * ch0);
void sendData(void * ch1); 

char signal;
char inputStatements[128];
unsigned int statementPending;

void recieveData(void * ch0)
{
	FILE * fromSat = (FILE *) ch0;
	char buffer = '\0'; 
	int read = 1;
	read = fread((void*) &buffer, CHAR, 1, fromSat);
	while(read == 1) 
	{
		if(buffer == '$')	
		{
			if(statementPending)
			{
				printf("%s", inputStatements);
				fflush(stdout);
			}
			else
			{
				putc(buffer, stdout);
			}
		}
		else
		{
			putc(buffer, stdout);
		}
		read = fread((void*) &buffer, CHAR, 1, fromSat);
	}
	printf("Press any key to continue\n\r");
	fflush(stdout);
	signal = 0;
}

void sendData(void * ch1)
{
	FILE * toSat = (FILE *) ch1; 
	//FILE * temp = fopen("./tempfifo", "w");
	//fprintf(temp, "hello");
	//fflush(temp);
	
	char c = '\0';
	while(signal)
	{
		//char c = '\0';
		c = getchar();
		/*	if('T' == c || 't' == c)
			{
				char thrusterCommandA = 0;
				char thrusterCommandB = 0;
				statementPending = 0;		
				sprintf(inputStatements, "What thruster would you like to set? (l, r, u, d)\0");
				statementPending = 1;
				while(thrusterCommandB == 0)
				{
					c = getchar();
					if('l' == c)
					{
						thrusterCommandB += (1 << 3);
					}
					else if ('r' == c)
					{
						thrusterCommandB += (1 << 2);
					}
					else if ('u' == c)
					{
						thrusterCommandB += (1 << 1);
					}
				else if ('d' == c)
				{
					thrusterCommandB += (1 << 0);
				}
			}
			
			statementPending = 0;
			sprintf(inputStatements, "How long would you like to set it? (1 - 256 sec)\0");
			statementPending = 1;
			//while(thrusterCommandA == 0)
			//{
			//	scanf("%d", &thrusterCommandA);
			//}
			
			while(c != 'y'){
				if(inputPending())
					c = getInput();
			}
			
			statementPending = 0;
			sprintf(inputStatements, "At what magnitude would you like to set it? (0 - 15)\0");
			statementPending = 1;
			unsigned int magnitude = 16;
			//while(magnitude > 15)
			//{
			//	scanf("%d", &magnitude);
			//}
			//
			while(c != 't'){
				if(inputPending())
					c = getInput();
			}

			//thrusterCommandB += (char)(magnitude << 4);

			char message[4];
			sprintf(message, "T %c%c", thrusterCommandA, thrusterCommandB);
			fwrite(message, CHAR, 4, toSat);
			fflush(toSat);
			statementPending = 0;
		
		}
		else
		{*/
		fwrite(&c, CHAR, 1, toSat);
		fflush(toSat);
		
	}
}
void main() 
{
	FILE * ch0; 
	FILE * ch1;

	//setEchoOff();
	//
	//sprintf(inputStatements, "Hello");
	statementPending = 0;
	setNonblockTerminalMode();

	signal = 1;

	void (*f1)(void *) = recieveData;
	void (*f2)(void *) = sendData;

	ch0 = fopen("./fromSat", "r"); 
	//int fd = open("./toSat", O_WRONLY | O_NONBLOCK);
	//ch1 = fdopen(fd, "w");
	ch1 = fopen("./toSat", "w"); 

	pthread_t thread[NUM_THREAD];
	
	pthread_create(&thread[0], NULL, (void *) f1, (void *) ch0);
    pthread_create(&thread[1], NULL, (void *) f2, (void *) ch1);

	pthread_join(thread[0], NULL);
   	pthread_join(thread[1], NULL); 	
	pthread_exit(NULL);

	fclose(ch0); 
	fclose(ch1);
	printf("\033[2J");
}
