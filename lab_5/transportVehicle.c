#include "transportVehicle.h"
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "bbbIO.h"
#include "block.h"
#include <time.h>
#include <semaphore.h>

#define PWM_PIN 2

#define MAX_TRANS_DIST 2000
#define MIN_TRANS_DIST 100
#define MAX_PERIOD 10000 // us
#define MIN_PERIOD 500 // us
#define SATELLITE 0

#define CONSTANT 0.2 // conversion between distance and period
#define INCR 1
#define DELAY 250000
#define DUTY 50
#define UPDATE_TIME 50

#define TRUE 1
#define FALSE 0

#define BACK 0
#define FORWARD 1

#define CHAR sizeof(char)

void transportVehicle(void * channel) 
{
	unsigned int * transportCh = (unsigned int *)channel;

	clock_t startTime;
	clock_t nextTime; 

	double distance = 0.0;

	unsigned int dock = TRUE; 
	unsigned int travel = FALSE;
	unsigned int direction = FORWARD; 

	unsigned int period = 0; 
	unsigned int duty = DUTY;
	
	char buffer[100]; 

	char request[2] = {'X', '\0'};
   	char confirmation[2] = {'X', '\0'};
	char dummy[2] = {'\0', '\0'};

	while(!transportCh[2])
	{
		// until channel is free
		sem_wait(&tran);  
		sem_post(&tran);
		
		// Transmitting range
		if (distance == SATELLITE)
		{
			travel = FALSE; // Stop at satellite
			if (dock)
			{
				request[0] = 'T'; // request lift off
			} 
			else if (direction == BACK) 
			{
				request[0] = 'D'; // request dock
			}
		}

		if (travel == TRUE) 
		{
			request[0] = '\0';
			confirmation[0] = '\0';
		}	
		
		// communication with satellite
		write(transportCh[1], request, CHAR);
		read(transportCh[0], confirmation, CHAR);

		// confirmation actions
		if (confirmation[0] == 'K')
		{
			dock = FALSE;
			travel = TRUE;
			direction = FORWARD;
		}
		else if (confirmation[0] == 'C')
		{
			dock = TRUE; 
		}
		
		//startTime = clock();
		//nextTime = clock();

		// transport vehicle is moving/traveling
		if (travel == TRUE) 
		{
			/*
				startTime = clock(); 	
			while ((nextTime - startTime) < CLOCKS_PER_SEC)
			{
				nextTime = clock();
				if ((nextTime - startTime) % UPDATE_TIME == 0)
				{
					period += INCR; 
				}
				distance = (double) period * CONSTANT;
				if (distance >= MIN_TRANS_DIST && distance <= MAX_TRANS_DIST) 
				{
					setPWM(duty, period, PWM_PIN); 
				}
			}

			while ((nextTime - startTime) < CLOCKS_PER_SEC) 
			{
				nextTime = clock(); 
				if((nextTime - startTime) < UPDATE_TIME)
				{
					period -= INCR; 
				}
				distance = (double) period * CONSTANT; 
				if (distance >= MIN_TRANS_DIST && distance <= MAX_TRANS_DIST) 
				{
					setPWM(duty, period, PWM_PIN); 
				}
			} */
			
			//usileep(DELAY);
			//nextTime = clock();
			//if(nextTime - startTime < CLOCKS_PER_SEC)
			//{
			//	startTime = nextTime;
			//	printf("Period: %d Dist: %.2f ", period, distance);
			//  printf("\r"); 	
				if (direction == FORWARD) 
				{
					// turn around
					if (distance == MAX_TRANS_DIST) 
					{
						direction = BACK; 
					}
					//else
					//{
						//startTime = clock(); 
						//while (nextTime - startTime < CLOCKS_PER_SEC) 
						//{
						//nextTime = clock();
						//if ((nextTime - startTime) % 500) {
							period += INCR;
						//}
						distance = (double) period * CONSTANT;	
						if (period >= MIN_PERIOD && distance <= MAX_PERIOD)
						{
							setPWM(duty, period, PWM_PIN);
						}
				//}
					//}
				}
				else if (direction == BACK) 
				{
					//startTime = clock(); 
					//while (nextTime - startTime < CLOCKS_PER_SEC) 
					//i{
					//nextTime = clock();
					//if ((nextTime - startTime) % 500)
					//{	
					period -= INCR;
					//}
					distance = (double) period * CONSTANT; 	
					if (period >= MIN_PERIOD && distance <= MAX_PERIOD)
					{
						setPWM(duty, period, PWM_PIN);
					}
					//}
				}
			//} 	
		}
	}
}
