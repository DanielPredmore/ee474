#include "frequencyCapture.h"
#include <unistd.h>
#include <stdio.h>
#include <time.h>

#define DATA_HIGH 68
#define DATA_LOW 44
#define LOAD_ENABLE 26
#define CLK 46
#define RST 65
#define MASK_HIGH (0x1<<0x7)
#define MASK_LOW 0x1

/*
int main()
{
	initIO();
	freqEnable(TRUE);
	freqReset();
	int i, j;
	volatile int k;
	for (j = 0; j < 10; j++)
	{
		clock_t start, stop;
		unsigned long count = 0;
		start = clock();
		freqReset();
		for (i = 0; i < 2; i++)
		{
			for(k = 0; k < 200000000; k++);
			
			count += freqRead() / 2;
			freqReset();
			// usleep(200000);
		}
		stop = clock();
		printf("frequency : %LfHz\n", (((long double)count) / (stop - start)) * CLOCKS_PER_SEC);
		printf("count     : %d\n", count);
		printf("lapseTime : %d\n", stop-start);
	}
}
*/


void freqEnable(bool enable)
{
	freqReset();
	setGPIOValue(LOAD_ENABLE, enable);
}

unsigned short freqRead()
{
	unsigned short data = 0x0;
	unsigned int data_high;
	unsigned int data_low;

	// reading the counter into the shift reg
	setGPIOValue(LOAD_ENABLE, FALSE);
	setGPIOValue(CLK, TRUE);
	setGPIOValue(CLK, FALSE);
	setGPIOValue(LOAD_ENABLE, TRUE);

	// removing the dummy first value
	setGPIOValue(CLK, TRUE);
	setGPIOValue(CLK, FALSE);

	int i;
	for (i = 0; i < 7; i++)
	{
		setGPIOValue(CLK, TRUE);
		data_high = getGPIOValue(DATA_HIGH);
		data_low = getGPIOValue(DATA_LOW);
		data = data | (data_high << (13 - i)) | (data_low << (6 - i)) ;
		setGPIOValue(CLK, FALSE);
	}

	return data;
}

void freqReset()
{
	setGPIOValue(RST, TRUE);
	setGPIOValue(RST, FALSE);
}
