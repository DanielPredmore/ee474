#include <stdio.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/select.h>
#include <unistd.h>
#include <stdlib.h>
#include "bbbIO.h"
#include "keypad.h"
#include "kernel.h"

#define BUFFER_SIZE 16
#define AMP_SIGNAL_MAX 325.0
#define AIN_MAX 1800.0

extern bool measureTemp;
extern bool measureTempAck;
extern bool measureBatt;

int commandFile;
struct timeval tv;
fd_set cf;
//FILE * toSat;

void shiftBuffer(unsigned int * buffer, unsigned int nValue);
void shiftBufferD(double * buffer, double nValue);
void ioTask(void * data);
void batteryTempRead(void * data);
void imageCapture(void * data);

TCB getIOTask()
{
  TCB theTCB;
  theTCB.pName = "I/O Task";
  theTCB.task = &ioTask;
  return theTCB;
}

TCB getBatteryTempRead()
{
	TCB theTCB;
	theTCB.pName = "Batt. Temp. Read";
	theTCB.task = &batteryTempRead;
	return theTCB;
}

TCB getImageCapture()
{
	TCB theTCB;
	theTCB.pName = "Image Capt.";
	theTCB.task = &imageCapture;
	return theTCB;
}

void closeCommandFile()
{
	FD_CLR(commandFile, &cf);
	//fclose(toSat);
	close(commandFile);	
}

void ioTask(void * data)
{
	static unsigned int btLBuffer[BUFFER_SIZE];

	static bool fileSet = FALSE;

	// named for the pwm channel they're coming from ecap1 is not exposed on the board
	unsigned int period2, duty2;

	mode = getGPIOValue(27);

	readAIN();

	if(measureBatt)
	{
		*(batteryLevelPtr) = 100 * batteryLvl / 0xfff;
		shiftBuffer(btLBuffer, *(batteryLevelPtr));
	}

	pirateProximity = ((AIN_MAX - (proximity * AIN_MAX) / 0xfff) * 200) / AIN_MAX;
	if(pirateProximity <= 100)
	{
		if(!pirateDetected && !pirateDestroyed)
		{
			pirateDetected = TRUE;
			raise(SIGUSR1);
		}
	}
	else
	{
		pirateDetected = FALSE;
		pirateDetectAck = FALSE;
		pirateDestroyed = FALSE;
	}

	// measureTemp = getGPIOValue(45);
	
	if(measureTemp && !measureTempAck)
		raise(SIGUSR1);

	// get data from the PWM capture channels
	// getPWM(&period0, &duty0, 0);
	getPWM(&period2, &duty2, 2);
/*
	if(period0 == 0)
	{
		imageFrequency = 0;
	}
	else
	{
		imageFrequency = 1000000000.0 / (period0 * 10.0);
	}
	shiftBufferD(freqBuffer, imageFrequency);
*/	

	if(!fileSet)
	{
		commandFile = open("./toSat", O_RDWR | O_NONBLOCK | O_RSYNC);
		tv.tv_sec = 0l;
		tv.tv_usec = 0l;
		atexit(closeCommandFile);
		fileSet = TRUE;
	}

	char c;

	FD_SET(commandFile, &cf);
	if(select(commandFile + 1, &cf, NULL, NULL, &tv) && !commandRunningAck)
	{
		commandRunning = TRUE;
		raise(SIGUSR1);
	}

	if(period2 == 0)
	{
		transportSignal = FALSE;
		transportPeriod = 0;
		transportDutyCycle = 0;
	}
	else
	{	
		transportSignal = TRUE;
		transportPeriod = period2 * 10;
		transportDutyCycle = duty2;
		if(!transportSignalAck)
			raise(SIGUSR1);
	}	
	
	if(TOTAL_INPUT_CHECKS == inputChecks)
	{
		inputChecks = 0;
		currentInput = '\0';
	}
	if(inputPending())
	{
		currentInput = getInput();
	}
}

void batteryTempRead(void * data)
{
	static unsigned int btTBuffer[BUFFER_SIZE] = {33, 33, 33, 33, 33, 33, 33, 33,
			33, 33, 33, 33, 33, 33, 33, 33};
	
	unsigned int nbatteryTemp1 = (unsigned int) (32 * (batteryTemp1 * (AIN_MAX / 0xfff) / AIN_MAX) + 33);
	unsigned int nbatteryTemp2 = (unsigned int) (32 * (batteryTemp2 * (AIN_MAX / 0xfff) / AIN_MAX) + 33);

	if(((double)(btTBuffer[BUFFER_SIZE - 1] * 12.0)/ 10.0 < (double)nbatteryTemp1)||
		((double)(btTBuffer[BUFFER_SIZE - 1] * 12.0)/ 10.0 < (double)nbatteryTemp2))
	{
		overTemperature = TRUE;
		raise(SIGUSR1);
	}
	
	*(batteryTempPtr) = (nbatteryTemp1 + nbatteryTemp2) / 2;
	shiftBuffer(btTBuffer, *(batteryTempPtr));	

	if(!measureTemp)
	{
		measureTempAck = FALSE;
		sleepTask();
	}
}

void imageCapture(void * data)
{	
	static double freqBufferLocal[16] = {	0.0, 0.0, 0.0, 0.0,
											0.0, 0.0, 0.0, 0.0,
											0.0, 0.0, 0.0, 0.0,
											0.0, 0.0, 0.0, 0.0};	
	unsigned int period0, duty0;
	static unsigned int measures = 0;
	
	getPWM(&period0, &duty0, 0);

	if(period0 == 0)
	{
		imageFrequency = 0;
	}
	else
	{
		imageFrequency = 1000000000.0 / (period0 * 10.0);
		measures++;
	}
	shiftBufferD(freqBufferLocal, imageFrequency);
	if(16 == measures)
	{
		for(int i = 0; i < 16; i++)
			freqBuffer[i] = freqBufferLocal[i];
		measures = 0;
		startImageAck = FALSE;
		sleepTask();
	}
}

void shiftBuffer(unsigned int * buffer, unsigned int nValue)
{
	for(int i = 0; i < BUFFER_SIZE - 1; i++)
	{
		buffer[i] = buffer[i + 1];
	}
	buffer[BUFFER_SIZE - 1] = nValue;
}


void shiftBufferD(double * buffer, double nValue)
{
	for(int i = 0; i < BUFFER_SIZE - 1; i++)
	{
		buffer[i] = buffer[i + 1];
	}
	buffer[BUFFER_SIZE - 1] = nValue;
}
