#!/bin/bash

slots="/sys/devices/platform/bone_capemgr/slots"

echo cape-universaln > $slots
echo BB-ADC > $slots

# enabling pwm header 0 ecap
pwmchip0="/sys/devices/platform/ocp/48300000.epwmss/48300100.ecap/pwm/pwmchip*"
echo 0 > $pwmchip0/export
echo 1 > $pwmchip0/pwm0/enable

# enabling pwm header 1 epwm
pwmchip1="/sys/devices/platform/ocp/48302000.epwmss/48302200.ehrpwm/pwm/pwmchip*"
echo 0 > $pwmchip1/export
echo 1 > $pwmchip1/export
echo 1 > $pwmchip1/pwm0/enable
echo 1 > $pwmchip1/pwm1/enable

# enablign pwm header 2 epwm
pwmchip2="/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip*"
echo 0 > $pwmchip2/export
echo 1 > $pwmchip2/export
echo 1 > $pwmchip2/pwm0/enable
echo 1 > $pwmchip2/pwm1/enable

pwmchip2e="/sys/devices/platform/ocp/48304000.epwmss/48304100.ecap/pwm/pwmchip*"
echo 0 > $pwmchip2e/export
echo 1 > $pwmchip2e/pwm0/enable

ocp="/sys/devices/platform/ocp"
echo pwm > $ocp/ocp:P8_13_pinmux/state
echo pwm > $ocp/ocp:P8_19_pinmux/state
echo pwm > $ocp/ocp:P9_42_pinmux/state
echo pwm > $ocp/ocp:P9_14_pinmux/state
echo pwm > $ocp/ocp:P9_16_pinmux/state
echo pwm2 > $ocp/ocp:P9_28_pinmux/state
echo gpio > $ocp/ocp:P8_08_pinmux/state
echo gpio > $ocp/ocp:P8_10_pinmux/state
echo gpio > $ocp/ocp:P8_11_pinmux/state
echo gpio > $ocp/ocp:P8_12_pinmux/state
echo gpio > $ocp/ocp:P8_14_pinmux/state
echo gpio > $ocp/ocp:P8_15_pinmux/state
echo gpio > $ocp/ocp:P8_16_pinmux/state
echo gpio > $ocp/ocp:P8_17_pinmux/state
echo gpio > $ocp/ocp:P8_18_pinmux/state

#./satellite.out
