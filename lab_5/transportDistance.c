#include "satellite.h"
#include <stdlib.h>
#include <stdio.h>
#include "bbbIO.h"
#include "kernel.h"
#include "display.h"

#define DATA_SIZE 8
#define TOLERANCE 0.10
#define NANOSEC 1000000000.0 // conversion from us to s
#define CONSTANT 200000.0 // conversion between frequency and distance

#define MAX_FREQ 2000 // hz
#define MIN_FREQ 100 // hz

#define BUFFER_SIZE 100

// determine global variables
extern circularArray distances;
extern unsigned int transportPeriod;
extern unsigned int transportDutyCycle;
extern bool transportSignal;
extern signed int transportDistPid;

// data struct for transport distance
struct transportDistanceData
{
	double frequency; 
	double distance; 
	double prevDistance;
	double tolerance;
	window_t * window;
	circularArray * distances; 
};
typedef struct transportDistanceData dataStruct;

// prototypes
void transportDistance(void *data); 
void writeData(circularArray * data, double point);
double readData(circularArray * data); 

/**
 * This function makes a new TCB task for the kernel
 *
 * @returns the TCB for transportDistance 
 **/
TCB getTransportDistance(window_t * window) 
{
	TCB theTCB; 
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 
	
	(data->distances) = &distances; 
	(data->frequency) = 0.0; 
	(data->distance) = 0.0;
	(data->prevDistance) = 0.0;
    (data->tolerance) = 0.0;
	(data->window) = window; 	

	theTCB.task = &transportDistance; 
	theTCB.taskDataPtr = data; 
	theTCB.pName = "Transport Distance";

	return theTCB; 
}


void transportDistance(void * data)
{	
	double read; 
	char buffer[BUFFER_SIZE]; 

	dataStruct * transData = (dataStruct *) data; 
	
	// print distances to console
	while (transData->distances->tail != transData->distances->head)  
	{
		if ((read = readData(transData->distances)) >= 0) 
		{
			windowPrint(transData->window, "                                         ", 3, 54); 
			sprintf(buffer, "Transport vehicle reported to be at %.1fm ", read);
		    windowPrint(transData->window, buffer, 3, 54); 
		}
	}

	if (transportPeriod > 0) 
	{ 	
		(transData->frequency) = NANOSEC / ((double) transportPeriod);
		if (transData->frequency <= MAX_FREQ && transData->frequency >= MIN_FREQ) 
		{
			transData->distance = (CONSTANT / transData->frequency);
			transData->tolerance = transData->prevDistance * TOLERANCE;
			//printf("Dist: %0.1f | Tolerance: %0.1f %0.1f", transData->distance,  transData->prevDistance + transData->tolerance, transData->prevDistance - transData->tolerance);
			//printf("\r");
			if (transData->distance > transData->prevDistance + transData->tolerance || transData->distance < transData->prevDistance - transData->tolerance) 
			{ 
				writeData(transData->distances, transData->distance);
			   	transData->prevDistance = transData->distance; 	
			}
		}
	}
/*	// if (transData->distances->tail != transData->distances->head)  
//	{
//		if ((read = readData(transData->distances)) >= 0) 
		{	
			sprintf(buffer, "Transport vehicle is %.1f m away\0", read);
		    windowPrint(transData->window, buffer, 3, 54); 
		} 
		//else 
		//{			
		//	windowPrint(transData->window, "Currently no updates to transport distance", 4, 54); 
		//					}
	}
*/
	if (transportPeriod == 0)  
	{
		transportSignalAck = FALSE;
		sleepOther(transportDistPid);
	}
}

// reads through a circular array 
double readData(circularArray * data)
{
	double value; 
	if (data->tail != data->head) 
	{
		if (data->tail != data->array + DATA_SIZE - 1)
		{
			value = *(data->tail);
			data->tail++; 
		} 
		else 
		{
			value = *(data->tail); 
			data->tail = data->array; 
		}
		return value; 
	}
	else
	{
		return -1.0;
	}
}	
 

// writes to circular array
void writeData(circularArray * data, double point) 
{
	if (data->head != data->array + DATA_SIZE - 1) // not at end of buffer
	{
		*(data->head) = point;
		data->head++; 
	} 
	else 
	{
		*(data->head) = point;
		data->head = data->array; 
	}
}
