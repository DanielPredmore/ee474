#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "game.h"
#include "satellite.h"
#include "kernel.h"


#define DIM_X 41
#define DIM_Y 64
#define NUM_PLAYERS 8
#define DELTA 100
#define BASE 50
#define NUM_ACTIONS 6
#define MAX_STAT 150
#define BACK_Y 130

#define PATH "/home/debian/ee474/asciiArt/satellite"
#define NAME "The Satellite"
#define BACK_COLOR 245
#define BACKGROUND_PATH "/home/debian/ee474/asciiArt/background"

#define PATH_1 "/home/debian/ee474/asciiArt/pirateShip0"
#define PATH_2 "/home/debian/ee474/asciiArt/pirateShip1"
#define PATH_3 "/home/debian/ee474/asciiArt/pirateShip2"
#define PATH_4 "/home/debian/ee474/asciiArt/pirateShip3"
#define PATH_5 "/home/debian/ee474/asciiArt/pirateShip4"
#define PATH_6 "/home/debian/ee474/asciiArt/pirateShip5"
#define PATH_7 "/home/debian/ee474/asciiArt/pirateShip6"
#define PATH_8 "/home/debian/ee474/asciiArt/pirateShip7"
#define NAME_1 "The Evil TA"
#define NAME_2 "The Angry Giant"
#define NAME_3 "USS Enterprise"
#define NAME_4 "TARDIS"
#define NAME_5 "The Black Pearl"
#define NAME_6 "The Death Star"
#define NAME_7 "The Millennium Falcon"
#define NAME_8 "The Martian"

struct game_data_struct
{
	game_t * game;
	window_t * statusWindow;
	window_t * displayWindow;
	window_t * commandWindow;
	window_t * eventWindow;
	player_t * players;
	action_t * actions;
	int nPlayers;
	int pirateProximity;
	char ** background;
};
typedef struct game_data_struct game_data;

void game(void *data);
void newGame(game_data *data);
void newPlayer(player_t *player, char * name, char * asciii, bool shift);
void battle(void *data);
void updateStatus(window_t *statusWindow, player_t *player, player_t *npc);
void updateDisplay(window_t *displayWindow, char **background, player_t *player, color_t pColor, player_t *npc, color_t nColor);
void updateCommand(window_t *commandWindow, player_t *player, action_t *actions);
bool doAction(action_t *action, player_t *caster, player_t *target);
bool canDo(action_t *action, player_t *player, int act);
action_t * buildActions();
action_t * ai(action_t *actions, player_t *npc);
void loadBackground(game_data *data);

TCB getGame(window_t * statusWindow, window_t * displayWindow, window_t * commandWindow, window_t * eventWindow)
{
	game_data * data = (game_data *)malloc(sizeof(game_data));
	data->game = NULL;
	data->statusWindow = statusWindow;
	data->displayWindow = displayWindow;
	data->commandWindow = commandWindow;
	data->eventWindow = eventWindow;
	data->players = NULL;
	data->actions = NULL;
	data->background = NULL;
	game_t * theGame = (game_t *)malloc(sizeof(game_t));
	player_t * thePlayer = (player_t *)malloc(sizeof(player_t));
	newPlayer(thePlayer, NAME, PATH, FALSE);
	theGame->player = thePlayer;
	theGame->npc = NULL;
	theGame->gameover = TRUE;
	data->game = theGame;
	data->nPlayers = NUM_PLAYERS;
	data->players = (player_t *)malloc(sizeof(player_t) * NUM_PLAYERS);
	newPlayer(&data->players[0], NAME_1, PATH_1, TRUE);
	newPlayer(&data->players[1], NAME_2, PATH_2, TRUE);
	newPlayer(&data->players[2], NAME_3, PATH_3, TRUE);
	newPlayer(&data->players[3], NAME_4, PATH_4, TRUE);
	newPlayer(&data->players[4], NAME_5, PATH_5, TRUE);
	newPlayer(&data->players[5], NAME_6, PATH_6, TRUE);
	newPlayer(&data->players[6], NAME_7, PATH_7, TRUE);
	newPlayer(&data->players[7], NAME_8, PATH_8, TRUE);
	data->actions = buildActions();
	loadBackground(data);
	updateDisplay(displayWindow, data->background, data->game->player, WHITE, NULL, BLACK);
	TCB tcb;
	tcb.taskDataPtr = data;
	tcb.task = game;
	tcb.pName = "Game";
	srand((unsigned int) NULL);
	return tcb;
}

void game(void *data)
{
	game_data *gData = (game_data *)data;

	if (gData->game->gameover && !pirateDestroyed)	
		newGame(gData);
	sleepTask();
}


void newGame(game_data * data)
{
	windowClear(data->statusWindow);
	windowClear(data->displayWindow);
	windowClear(data->commandWindow);
	windowClear(data->eventWindow);
	data->game->npc = &data->players[rand() % NUM_PLAYERS];

	player_t *player = data->game->player;
	player_t *npc = data->game->npc;

	npc->hp = BASE + (rand() % DELTA);
	npc->power = BASE + (rand() % DELTA);
	npc->defence = (rand() % DELTA) / 2;

	player->hp = 100;
	player->power = batteryLevelPtr[0];
	player->defence = player->power / 4;

	updateDisplay(data->displayWindow, data->background, player, WHITE, npc, WHITE);
	updateStatus(data->statusWindow, player, npc);
	updateCommand(data->commandWindow, player, data->actions);

	windowPrint(data->eventWindow, npc->name, 1, 20 - strlen(npc->name)/2);
	windowPrint(data->eventWindow, "Approaches", 2, 15); 

	data->game->gameover = FALSE;
	data->pirateProximity = pirateProximity;

	TCB tcb;
	tcb.taskDataPtr = data;
	tcb.task = battle;
	tcb.pName = "Battle Task";
	addTask(tcb, 3, CLOCKS_PER_SEC / 30);
}

void newPlayer(player_t *player, char * name, char * path, bool shift)
{
	player->name = name;
	player->attack_c = -1;
	player->hp = -1;
	player->power = -1;
	player->defence = -1;

	player->ascii = (char **)malloc(sizeof(char *) * DIM_X);

	int i;
	for (i = 0; i < DIM_X; i++)
	{
		player->ascii[i] = (char *)malloc(sizeof(char) * (DIM_Y + 1));
		player->ascii[i][DIM_Y] = '\0';
		player->ascii[i][0] = '\0';
	}
	i = 0;
	FILE * fp = fopen(path, "r");
	
	if (fp == NULL)
			return;

	while(fgets(player->ascii[i], DIM_Y, fp) != NULL && i < DIM_X)
	{
		i++;
	}
	fclose(fp);

	if (!shift)
			return;

	int max = 0;
	for (i = 0; i < DIM_X; i++)
	{
		int len = strlen(player->ascii[i]);
		if (len > max)
		{
			max = len;
		}
	}
	int disp = DIM_Y - max - 1;
	int j;
	for (i = 0; i < DIM_X; i++)
	{
		for (j = 0; j < max; j++)
		{
			int source = max - j - 1;
			int dest = DIM_Y - j - 2;
			player->ascii[i][dest] = player->ascii[i][source];
		}
		for (j = 0; j < disp; j++)
		{
			player->ascii[i][j] = ' ';
		}
	}
}

void battle(void *data)
{
	game_data *gData = (game_data *)data;
	action_t * playerAction;
	player_t * player = gData->game->player;
	player_t * npc = gData->game->npc;
	char buff[50];
	
	if (pirateProximity > 100)
	{
		windowClear(gData->eventWindow);
		sprintf(buff, "%s ran away", npc->name);
		windowPrintColor(gData->eventWindow, buff, BRIGHT_CYAN, 1, 1);
		updateDisplay(gData->displayWindow, gData->background, player, WHITE, NULL, BLACK);
		gData->game->gameover = TRUE;
		endTask();
		return;
	}

	int current_clicks = gData->pirateProximity / 5;
	int next_clicks = pirateProximity / 5;

	if (current_clicks != next_clicks)
	{
		updateDisplay(gData->displayWindow, gData->background, player, WHITE, npc, WHITE);
	}

	if (pirateProximity != gData->pirateProximity)
	{
		if (gData->pirateProximity > 30 && pirateProximity <= 30)
		{
			windowClear(gData->eventWindow);
			sprintf(buff, "%s is in phaser range", npc->name);
			windowPrintColor(gData->eventWindow, buff, BRIGHT_GREEN, 1, 1);
			updateCommand(gData->commandWindow, player, gData->actions);
		}
		else if (gData->pirateProximity <= 30 && pirateProximity > 30)
		{
			windowClear(gData->eventWindow);
			sprintf(buff, "%s is out of range", npc->name);
			windowPrintColor(gData->eventWindow, buff, BRIGHT_RED, 1, 1);
			updateCommand(gData->commandWindow, player, gData->actions);
		}
		else if (gData->pirateProximity > 5 && pirateProximity <= 5)
		{
			windowClear(gData->eventWindow);
			sprintf(buff, "%s is in photon range", npc->name);
			windowPrintColor(gData->eventWindow, buff, BRIGHT_GREEN, 1, 1);
			updateCommand(gData->commandWindow, player, gData->actions);
		}
		else if (gData->pirateProximity <= 5 && pirateProximity > 5)
		{
			windowClear(gData->eventWindow);
			sprintf(buff, "%s is out of photon range", npc->name);
			windowPrintColor(gData->eventWindow, buff, BRIGHT_RED, 1, 1);
			updateCommand(gData->commandWindow, player, gData->actions);
		}
		gData->pirateProximity = pirateProximity;
	}

	if (pirateProximity > 30)
	{
		return;
	}

	if (gameCommand == '\0' || (gameCommand < '1' && gameCommand > '9'))
		return;

	int act = (int) gameCommand - '1';
	gameCommand = '\0';

	if (act < 0 || act >= NUM_ACTIONS)
	{
		windowClear(gData->eventWindow);
		windowPrintColor(gData->eventWindow, "That is not a valid action", BRIGHT_RED, 1, 1);
		return;
	}

	playerAction = &gData->actions[act];

	if (!canDo(playerAction, player, act))
	{
		windowClear(gData->eventWindow);
		sprintf(buff, "Unable to do %s", playerAction->title);
		windowPrintColor(gData->eventWindow, buff, BRIGHT_YELLOW, 1, 1);
		return;
	}


	action_t * npcAction = ai(gData->actions, gData->game->npc);
	windowClear(gData->eventWindow);

	
	sprintf(buff, "%s attempted:", player->name);
	windowPrint(gData->eventWindow, buff, 0, 1);
	bool pSuc = doAction(playerAction, player, npc);
	if (pSuc)
	{
		sprintf(buff, "%s and succeeded", playerAction->title);
	}
	else
	{
		sprintf(buff, "%s and failed", playerAction->title);
	}
	windowPrint(gData->eventWindow, buff, 1, 1);

	if (npc->hp > 0)
	{
		sprintf(buff, "%s attempted:", npc->name);
		windowPrint(gData->eventWindow, buff, 3, 1);
		bool nSuc = doAction(npcAction, npc, player);
		if (nSuc)
		{
			sprintf(buff, "%s and succeeded", npcAction->title);
		}
		else
		{
			sprintf(buff, "%s and failed", npcAction->title);
		}
		windowPrint(gData->eventWindow, buff, 4, 1);
	}

	if (player->hp == 0)
	{
		windowClear(gData->eventWindow);
		windowPrintColor(gData->eventWindow, "You have been defeated!", RED, 1, 1);
		windowPrintColor(gData->eventWindow, "The pirate loots you and leaves", WHITE, 3, 1);
		updateDisplay(gData->displayWindow, gData->background, player, RED, npc, WHITE);
	}
	else if(npc->hp == 0)
	{
		windowClear(gData->eventWindow);
		windowPrintColor(gData->eventWindow, "You have destroyed the pirate", BRIGHT_GREEN, 1, 1);
		updateDisplay(gData->displayWindow, gData->background, player, WHITE, NULL, RED);
	}

	updateStatus(gData->statusWindow, player, npc);
	updateCommand(gData->commandWindow, player, gData->actions);

	if (player->hp <= 0 || npc->hp <= 0)
	{
		windowClear(gData->commandWindow);
		gData->game->gameover = TRUE;
		pirateDestroyed = TRUE;
		sleepOther(pirateManPid);
		sleepOther(gamePid);
		endTask();
	}
}


	

void updateStatus(window_t *statusWindow, player_t *player, player_t *npc)
{
	char buff[25];
	windowClear(statusWindow);
	
	windowPrintColor(statusWindow, player->name, BRIGHT_GREEN, 0, 0);
	windowPrintColor(statusWindow, npc->name, BRIGHT_YELLOW, 0, 65);

	sprintf(buff, "HP: %d", player->hp);
	windowPrintColor(statusWindow, buff, BRIGHT_GREEN, 1, 0);
	sprintf(buff, "POWER: %d", player->power);
	windowPrintColor(statusWindow, buff, BRIGHT_BLUE, 1, 20);
	sprintf(buff, "SHIELD: %d", player->defence);
	windowPrintColor(statusWindow, buff, BRIGHT_CYAN, 1, 40);

	sprintf(buff, "HP: %d", npc->hp);
	windowPrintColor(statusWindow, buff, BRIGHT_GREEN, 1, 65);
	sprintf(buff, "POWER: %d", npc->power);
	windowPrintColor(statusWindow, buff, BRIGHT_BLUE, 1, 85);
	sprintf(buff, "SHIELD: %d", npc->defence);
	windowPrintColor(statusWindow, buff, BRIGHT_CYAN, 1, 105);
}

void updateDisplay(window_t *displayWindow, char **background, player_t *player, color_t pColor, player_t *npc, color_t nColor)
{
	int i, j;
	int click = 20 - pirateProximity / 5;
	char *str;
	windowClear(displayWindow);
	for (i = 0; i < DIM_X; i++)
	{
		windowPrintColor(displayWindow, background[i], BACK_COLOR, i, 0);
		if (player != NULL)
		{
			for (j = 0; j < DIM_Y && player->ascii[i][j] == ' '; j++);
			str = &player->ascii[i][j];
			windowPrintColor(displayWindow, str, pColor, i, 0 + j);
		}
		if (npc != NULL)
		{
			for(j = 0; j < DIM_Y && npc->ascii[i][j] == ' '; j++);
			str = &npc->ascii[i][j];
			windowPrintColor(displayWindow, str, nColor, i, 65 + j - click);
		}
	}
}

void updateCommand(window_t *commandWindow, player_t *player, action_t *actions)
{
	windowClear(commandWindow);
	if (pirateProximity > 30)
		return;
	color_t color = WHITE;
	char buff[25];
	int i;
	action_t *action;
	for (i = 0; i < NUM_ACTIONS; i++)
	{
		int x = (i % 2) * 2 + 1;
		int y = (i / 2) * 20 + 1;
		action = &actions[i];
		sprintf(buff, "%d: %s", i + 1, action->title);
	   	if (canDo(action, player, i))
		{
			color = WHITE;
		}
		else
		{
			color = GREAY;
		}
		windowPrintColor(commandWindow, buff, color, x, y);
	}

}

bool doAction(action_t *action, player_t *caster, player_t *target)
{
	int chance = rand() % 100;
	bool hit = FALSE;

	if (action->chance >= chance)
	{
		hit = TRUE;
		if (action->damage != 0)
		{
			int damage = (action->damage * 100) / (50 + rand() % 101);

			if (target->defence > 0)
			{
				target->defence -= damage;
				if (target->defence < 0)
				{
					damage = -target->defence;
					target->defence = 0;
				}
				else
				{
					damage = 0;
				}
			}
			target->hp -= damage;
			if (target->hp < 0)
			{
				target->hp = 0;
			}
		}
		if (action->power > 0)
		{
			int power = (action->power * 100) / (50 + rand() % 101);
			caster->power += power;
			if (caster->power > MAX_STAT)
			{
				caster->power = MAX_STAT;
			}
		}
		int defence = (action->defence * 100) / (50 + rand() % 101);
		caster->defence += defence;
		if (caster->defence > MAX_STAT)
		{
			caster->defence = MAX_STAT;
		}
		int heal = (action->heal * 100) / (50 + rand() % 101);
		caster->hp += heal;
		if (caster->hp > MAX_STAT)
		{
			caster->hp = MAX_STAT;
		}
		int drain = (action->drain * 100) / (50 + rand() % 101);
		target->power -= drain;
		if (target->power < 0)
		{
			target->power = 0;
		}
	}
	if (action->power < 0)
	{
		caster->power += action->power;
		if (caster->power < 0)
		{
			caster->power = 0;
		}
	}
	return hit;
}

bool canDo(action_t *action, player_t *player, int act)
{
	if (player->power < -action->power)
			return FALSE;
	if (act == 1 && pirateProximity > 5)
			return FALSE;
	return TRUE;
}
	
action_t *buildActions()
{
	action_t *actions = (action_t *)malloc(sizeof(action_t) * NUM_ACTIONS);
	
	actions[0].title = "Phaser";
	actions[0].damage = 15 + rand() % 21;
	actions[0].power = -15;
	actions[0].defence = 0;
	actions[0].heal = 0;
	actions[0].drain = 0;
	actions[0].chance = 75;

	actions[1].title = "Photon Torpedo";
	actions[1].damage = 35 + rand() % 16;
	actions[1].power = 0;
	actions[1].defence = 0;
	actions[1].heal = 0;
	actions[1].drain = 0;
	actions[1].chance = 50;

	actions[2].title = "Power Up";
	actions[2].damage = 0;
	actions[2].power = 30 + rand() % 16;
	actions[2].defence = 0;
	actions[2].heal = 0;
	actions[2].drain = 0;
	actions[2].chance = 85;

	actions[3].title = "Repair";
	actions[3].damage = 0;
	actions[3].power = -15;
	actions[3].defence = 0;
	actions[3].heal = 10 + rand() % 16;
	actions[3].drain = 0;
	actions[3].chance = 85;

	actions[4].title = "Drain Power";
	actions[4].damage = 0;
	actions[4].power = -15 + rand() % 30;
	actions[4].defence = 0;
	actions[4].heal = 0;
	actions[4].drain = 15 + rand() % 10;
	actions[4].chance = 45;

	actions[5].title = "Charge Shield";
	actions[5].damage = 0;
	actions[5].power = -30;
	actions[5].defence = 20 + rand() % 16; 
	actions[5].heal = 0;
	actions[5].drain = 0;
	actions[5].chance = 90;

	return actions;
}

action_t * ai(action_t *actions, player_t * npc)
{
	action_t * action;
	bool bad = FALSE;
	do
	{
		action = &actions[ rand() % NUM_ACTIONS];
		bad = FALSE;

		if (npc->hp > 100 && action->heal > 0)
				bad = TRUE;
		if (npc->power < -action->power)
				bad = TRUE;
		if (npc->defence > 100 && action->defence > 0)
				bad = TRUE;
		if (npc->power > 100 && action->power > 0)
				bad = TRUE;
	} while(bad);
	return action;
}

void loadBackground(game_data *data)
{
	data->background = (char **)malloc(sizeof(char *) * DIM_X);
	
	int i;
	for (i = 0; i < DIM_X; i++)
	{
		data->background[i] = (char *)malloc(sizeof(char) * (BACK_Y + 1));
		data->background[i][0] ='\0';
		data->background[i][BACK_Y] = '\0';
	}

	i = 0;
	FILE *fp = fopen(BACKGROUND_PATH, "r");

	if (fp == NULL)
			return;

	while(fgets(data->background[i], BACK_Y, fp) != NULL && i < DIM_X)
	{
		i++;
	}
	fclose(fp);
}
