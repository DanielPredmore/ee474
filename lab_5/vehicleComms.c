#include <stdio.h>
#include <signal.h>
#include "satellite.h"
#include <unistd.h>
#include "keypad.h"
#include "display.h"
#include "vehicleComms.h"
#include <stdlib.h>
#include <fcntl.h>
#include <semaphore.h>
#include "ports.h"
#include "kernel.h"
#include "block.h"
#include "styleSheet.h"

#define CHAR sizeof(char)

// global variables
extern char command;
extern char response; 
extern circularArray distances;
extern bool startImageCapture; 

// data struct for vehicle communication
struct vehicleCommsData 
{
	bool * startImageCapture; 
	char * command;
	char prevCommand; 
	char * response;
	circularArray * distances;
	unsigned int * miningCh;
	unsigned int * transportCh;
	window_t * satWindow; 
	window_t * window; 
}; 

typedef struct vehicleCommsData dataStruct;

// function prototypes
void vehicleComms(void *data);

/**
 * This function creates a new TCB for the kernal
 *
 * @returns TCB for vehicleComms
 */

TCB getVehicleComms(window_t * window, window_t * window2, unsigned int * miningCh, unsigned int * transportCh) 
{
	TCB theTCB; 
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 

	// initilizaing data struct
	(data->command) = &command;
	(data->prevCommand) = '\0';
	(data->response) = &response;
	(data->miningCh) = miningCh;
	(data->distances) = &distances; 
	(data->transportCh) = transportCh;
	(data->startImageCapture) = &startImageCapture; 	
	(data->window) = window;
	(data->satWindow) = window2; 

	theTCB.task = &vehicleComms;
	theTCB.taskDataPtr = data;
	theTCB.pName = "Vehicle Comms";

	return theTCB;
}

void vehicleComms(void * data) 
{
	char response[2] = {'\0', '\0'};
	char buffer[100];
	color_t color = RESET;
		
	dataStruct * commsData = (dataStruct *) data;
	
	sem_post(&mine);
	write(commsData->miningCh[1], commsData->command, CHAR);

	// MINING VEHICLE COMMUNICATIONS
	
	// read response from mining vehicle
	if (read(commsData->miningCh[0], response, CHAR) > 0) 
	{
		commsData->response[0] =  response[0];
	} 
	else
	{
		commsData->response[0] = '\0';
	}
	sem_wait(&mine);
	
	// clears window
	//windowClear(commsData->window);

	windowPrintColor(commsData->window, "Commands: ", HEADER, 1, 2); 

	// Prints the commands sent
	windowPrint(commsData->window, "                                   ", 1, 52);
	windowPrintColor(commsData->window, "Mining Vehicle Communication:\0", HEADER, 1, 52);
	if (commsData->command != '\0') 
	{
		windowPrint(commsData->window, "                       ", 3, 54); 
		windowPrintColor(commsData->window, "Earth sent command: \0", SENT_COLOR, 3, 54);
		windowCharColor(commsData->window, *commsData->command, SENT_COLOR, 3, 74);
	} 
	else 
	{
		windowPrint(commsData->window, "                           ", 3, 54); 
		windowPrintColor(commsData->window, "No command sent from Earth", RED, 3, 54);  	
	}

	// if receive response from vehicle 
	if(commsData->response[0] == 'S')
	{
		windowPrint(commsData->window, "                         ", 5, 54); 
		sprintf(buffer, "Earth received the response: W          \0");
		windowPrintColor(commsData->window, buffer, RECIVED_COLOR, 5, 54); 
	}
	else if (commsData->response[0] == 'I')
	{
		windowPrint(commsData->window, "                         ", 5, 54); 
		sprintf(buffer, "Earth received the response: P          \0");
		windowPrintColor(commsData->window, buffer, RECIVED_COLOR, 5, 54);
	   	
		windowPrint(commsData->window, "Image Frequencies: ", 1, 90);
		sprintf(buffer, "[%-08.3lf %-08.3lf %-08.3lf %-08.3lf\0", freqBuffer[0], freqBuffer[1], freqBuffer[2], freqBuffer[3]);
		windowPrint(commsData->window, buffer, 2, 92);
		sprintf(buffer, " %-08.3lf %-08.3lf %-08.3lf %-08.3lf\0", freqBuffer[4], freqBuffer[5], freqBuffer[6], freqBuffer[7]);
		windowPrint(commsData->window, buffer, 3, 92);
		sprintf(buffer, " %-08.3lf %-08.3lf %-08.3lf %-08.3lf\0", freqBuffer[8], freqBuffer[9], freqBuffer[10], freqBuffer[11]);
		windowPrint(commsData->window, buffer, 4, 92);
		sprintf(buffer, " %-08.3lf %-08.3lf %-08.3lf %-08.3lf]\0", freqBuffer[12], freqBuffer[13], freqBuffer[14], freqBuffer[15]);
		windowPrint(commsData->window, buffer, 5, 92);
	}	
	else if (commsData->response[0] != '\0') 
	{
		windowPrint(commsData->window, "                         ", 5, 54); 
		sprintf(buffer, "Earth received the response: A %c\0", commsData->response[0]);
		windowPrintColor(commsData->window, buffer, RECIVED_COLOR, 5, 54); 
	}	
	else 
	{
		response[0] = '\0';
		windowPrint(commsData->window, "                                 ", 5, 54); 
		windowPrintColor(commsData->window, "No response from mining vehicle", NO_COMMS_COLOR, 5, 54); 
	}
	
	// prints last command
	commsData->prevCommand = *commsData->command;		
	
	// TRANSPORT VEHICLE COMMUNICATIONS	
	char request[2] = {'x', '\0'};
	char confirmation[2] = {'\0', '\0'};
	double distance; 

	sem_post(&tran);
	windowPrint(commsData->satWindow, "                                      ", 3, 4); // clear lines
	windowPrint(commsData->satWindow, "                                      ", 5, 4); // clear lines
	windowPrintColor(commsData->satWindow, "Transport Vehicle Communications: ", HEADER, 1, 2); 

	// checks for transport vehicle request 
	if (read(commsData->transportCh[0], request, CHAR) > 0)
	{
		sprintf(buffer, "Received the request: %c\0", request[0]);
		if (request[0] == 'T') // lift off request
		{ 
			windowPrintColor(commsData->satWindow, buffer, RECIVED_COLOR, 3, 4);
			confirmation[0] = 'K'; 
			sprintf(buffer, "Sent confirmation: %c\0", confirmation[0]);
			windowPrintColor(commsData->satWindow, buffer, SENT_COLOR, 5, 4);
		}
		else if (request[0] == 'D') // docking request
		{
			windowPrintColor(commsData->satWindow, buffer, RECIVED_COLOR, 3, 4); 
			confirmation[0] = 'C';
			sprintf(buffer, "Sent confirmation: %c\0", confirmation[0]);
			windowPrintColor(commsData->satWindow, buffer, SENT_COLOR, 5, 4); 
		}
		else 
		{
			windowPrintColor(commsData->satWindow, "No requests from transport vehicle\0", NO_COMMS_COLOR, 3, 4); 
		}	
	}

	// sends confirmation to transport vehicle 
	write(commsData->transportCh[1], confirmation, CHAR);

	sem_wait(&tran);
}
