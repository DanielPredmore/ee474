#include "solarPanelControl.h"
#include "bbbIO.h"
#include "kernel.h"
#include "consoleKeypad.h"
#include <stdlib.h>
#include <signal.h>

#define DEFAULT_MOTOR_SPEED 50
// max speed
#define MOTOR_SPEED_FULL_ON 100
// min speed
#define MOTOR_SPEED_FULL_OFF 0
#define MOTOR_SPEED_INCREMENT 5
#define MOTOR_DRIVE_FULL_ON 50000
#define MOTOR_DRIVE_FULL_OFF 0
#define MOTOR_PWM_PERIOD_USEC 500

extern bool solarPanelState;
extern bool solarPanelDeploy;
extern bool solarPanelRetract;
extern bool driveMotorSpeedInc;
extern bool driveMotorSpeedDec;

// solar panel control data struct
struct solarPanelControlData
{
	bool * solarPanelState;
	bool * solarPanelDeploy;
	bool * solarPanelRetract;
	bool * driveMotorSpeedInc;
	bool * driveMotorSpeedDec;
};
typedef struct solarPanelControlData dataStruct;

// private function prototypes
void solarPanelControl(void *data);
void driveMotorTask(void * data);

/**
 * This function makes a new TCB task for the kernel 
 *
 * @return the TCB for the solarPanelControl
 */
TCB getSolarPanelControl()
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));

	// initialization of data struct's data
	data->solarPanelState = &solarPanelState;
	data->solarPanelDeploy = &solarPanelDeploy;
	data->solarPanelRetract = &solarPanelRetract;
	data->driveMotorSpeedInc = &driveMotorSpeedInc;
	data->driveMotorSpeedDec = &driveMotorSpeedDec;

	// setting the values in the TCB
	theTCB.task = &solarPanelControl;
	theTCB.taskDataPtr = data;
	theTCB.pName = "Solar Panel Ctrl";

	return theTCB;
}
TCB getDriveMotorTask()
{
	TCB theTCB;

	theTCB.task = &driveMotorTask;
	theTCB.pName = "Dr. M.";

	return theTCB;
}
/**
 * This is the task function for the solar panel control task of the satellite system
 *
 * @param data: pointer to the solarPanelControl data struct
 */
void solarPanelControl(void * data)
{
	// the speed of the motor
	static bool startedKeypad = FALSE;

	dataStruct * theData = (dataStruct *)data;

	if(fullyDeployed)
	{
		*(theData->solarPanelDeploy) = FALSE;
		*(theData->solarPanelState) = TRUE;
		fullyDeployed = FALSE;
		fullyDeployedAck = FALSE;
		measureTemp = TRUE;
		raise(SIGUSR1);
	}

	if(fullyRetracted)
	{
		*(theData->solarPanelRetract) = FALSE;
		*(theData->solarPanelState) = FALSE;
		fullyRetracted = FALSE;
		fullyRetractedAck = FALSE;
		measureTemp = FALSE;
	}

	if(TRUE == *(theData->driveMotorSpeedInc))
	{
		if(driveMotorSpeed + MOTOR_SPEED_INCREMENT >= MOTOR_SPEED_FULL_ON)
			driveMotorSpeed = MOTOR_SPEED_FULL_ON;
		else
			driveMotorSpeed += MOTOR_SPEED_INCREMENT;
		*(theData->driveMotorSpeedInc) = FALSE;
		setPWM(driveMotorSpeed, MOTOR_PWM_PERIOD_USEC, 0);
	}
	else if(TRUE == *(theData->driveMotorSpeedDec))
	{
		if(driveMotorSpeed - MOTOR_SPEED_INCREMENT >= MOTOR_SPEED_FULL_ON)
			driveMotorSpeed = MOTOR_SPEED_FULL_OFF;
		else
			driveMotorSpeed -= MOTOR_SPEED_INCREMENT;
		*(theData->driveMotorSpeedDec) = FALSE;
		setPWM(driveMotorSpeed, MOTOR_PWM_PERIOD_USEC, 0);
	}

	if(TRUE == *(theData->solarPanelDeploy) && !overTemperature)
	{
		if(FALSE == startedKeypad)
		{
			startedKeypad = TRUE;
			wakeTask(keypadPid);
			setPWM(driveMotorSpeed, MOTOR_PWM_PERIOD_USEC, 0);
		}
	}
	else if (TRUE == *(theData->solarPanelRetract))
	{
		if(FALSE == startedKeypad)
		{
			startedKeypad = TRUE;
			wakeTask(keypadPid);
			setPWM(driveMotorSpeed, MOTOR_PWM_PERIOD_USEC, 0);
		}
	}
	else
	{
		startedKeypad = FALSE;
		setPWM(0, MOTOR_PWM_PERIOD_USEC, 0);
		sleepTask();
	}
}

void driveMotorTask(void * data)
{
	// how extended the solar panels are
	static unsigned short driveMotor = 0;
	
	if(TRUE == solarPanelDeploy)
	{
		if(driveMotor + driveMotorSpeed >= MOTOR_DRIVE_FULL_ON && !fullyDeployedAck)
		{
			driveMotor = MOTOR_DRIVE_FULL_ON;
			fullyDeployed = TRUE;
			raise(SIGUSR1);
		}
		else
		{
			driveMotor += driveMotorSpeed;
		}
	}
	else if (TRUE == solarPanelRetract)
	{
		if(driveMotor - driveMotorSpeed >= MOTOR_DRIVE_FULL_ON && !fullyRetractedAck)
		{
			driveMotor = MOTOR_DRIVE_FULL_OFF;
			fullyRetracted = TRUE;
			raise(SIGUSR1);
		}
		else
		{
			driveMotor -= driveMotorSpeed;
		}
	}
}
