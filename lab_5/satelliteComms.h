#ifndef SATELLITE_COMMS_H
#define SATELLITE_COMMS_H

#include "satellite.h"
#include "display.h"
#include "keypad.h" 
#include "satelliteComms.h"

TCB getSatelliteComms(window_t * window, window_t * vWindow);

#endif // SATELLITE_COMMS_H
