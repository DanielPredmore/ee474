#include <stdio.h>
#include "kernel.h"
#include "satellite.h"
#include "display.h"
#include "timeWindow.h"

void main()
{
	display_t *display = newDisplay(stdout, 100, 100);
	if (display == NULL)
			printf("Broken\n");
	window_t *window = newWindow(display, TRUE, 1,1,5,25);
	
	int i, j;
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 5; j++)
		{
			windowPrint( newWindow(display, TRUE, 27 + 6 * i, 5 + 6 * j, 5, 5), "Hi", 2, 2);
		}
	}

	window_t *timeWindow = newWindow(display, TRUE, 1, 27, 1, 26);
	addTask(getTimeWindow(timeWindow, BLUE, 1), CLOCKS_PER_SEC / 10);

	windowPrintColor(window, "Hello, World", RED, 2, 5);
	kernelStart();
	freeDisplay(display);
}
