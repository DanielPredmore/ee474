#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include "keypad.h"

#define NUM_THREAD 2
#define CHAR sizeof(char)

// prototypes 
void receiveData(void * ch0);
void sendData(void * ch1); 

char signal;
char inputStatements[60];
unsigned int inputStatementLength;
unsigned int statementPending;

void recieveData(void * ch0)
{
	FILE * fromSat = (FILE *) ch0;
	char buffer = '\0'; 
	int read = 1;
	int l = 0;
	read = fread((void*) &buffer, CHAR, 1, fromSat);
	while(read == 1) 
	{
		if(buffer == '\1')
		{
			fflush(stdout);
		}
		else if(buffer == '$' || buffer == '\5')	
		{
			if(statementPending)
			{
				if(l < inputStatementLength)
				{
					putc(inputStatements[l], stdout);
					l++;
				}
				else
				{
					putc('.', stdout);
					l++;
					if(l == 60)
						l = 0;
				}
			}
			else
			{
				putc('.', stdout);
				l++;
				if(l == 60)
					l = 0;
			}
		}
		else
		{
			putc(buffer, stdout);
		}
		read = fread((void*) &buffer, CHAR, 1, fromSat);
	}
	fflush(stdout);
	signal = 0;
}

void sendData(void * ch1)
{
	FILE * toSat = (FILE *) ch1; 
	static unsigned char strBuild[3] = "\0\0\0";
	static unsigned char lastCommand ='\0';

	char c = '\0';
	while(signal)
	{
		c = getchar();
		if('T' == c && lastCommand != 'M')
		{
			char thrusterCommandA = 5;
			char thrusterCommandB = 0;
			unsigned int magnitude = 16;
			unsigned int duration = 0;
			
			fwrite("T ", CHAR, 2, toSat);
			fflush(toSat);
			
			statementPending = 0;
			sprintf(inputStatements, "Duration? (5 - 126 sec)\0");
			inputStatementLength = strlen(inputStatements);
			statementPending = 1;

			fwrite("\1", CHAR, 1, toSat);
			fflush(toSat);

			int i = 0;
			char inputPrompt[60];
			sprintf(inputPrompt, "%s", inputStatements);
			while(duration == 0 && signal)
			{
				c = getchar();
				if(c == '\n' || c == '\r')
				{
					if(strBuild[0] != '\0')
					{
						for(int j = 0; j < i; j++)
						{
							int multiplier = 1;
							for(int k = 0; k < j; k++)
							{
								multiplier *= 10;
							}
							duration += (strBuild[i - j - 1] - 48) * multiplier;
						}
						if(duration > 126)
						{
							duration = 126;
						}
						else if (duration == 0)
						{
							i = 0;
							strBuild[0] = strBuild[1] = strBuild[2] = '\0';
							statementPending = 0;
							sprintf(inputStatements, "%s %c%c%c\0", inputPrompt, strBuild[0], strBuild[1], strBuild[2]);
							inputStatementLength = strlen(inputStatements);
							statementPending = 1;
							fwrite("\1", CHAR, 1, toSat);
							fflush(toSat);
						}
					}
				}
				else if(c > 47 && c < 58)
				{
					strBuild[i] = c;
					i++;
					statementPending = 0;
					sprintf(inputStatements, "%s %c%c%c\0", inputPrompt, strBuild[0], strBuild[1], strBuild[2]);
					inputStatementLength = strlen(inputStatements);
					statementPending = 1;
					if(i == 3)
					{
						for(int j = 0; j < i; j++)
						{
							int multiplier = 1;
							for(int k = 0; k < j; k++)
							{
								multiplier *= 10;
							}
							duration += (strBuild[i - j - 1] - 48) * multiplier;
						}
						if(duration > 126)
						{
							duration = 126;
						}
						else if (duration == 0)
						{
							i = 0;
							strBuild[0] = strBuild[1] = strBuild[2] = '\0';
							statementPending = 0;
							sprintf(inputStatements, "%s %c%c%c\0", inputPrompt, strBuild[0], strBuild[1], strBuild[2]);
							inputStatementLength = strlen(inputStatements);
							statementPending = 1;
							fwrite("\1", CHAR, 1, toSat);
							fflush(toSat);
						}
					}
					else
					{
						fwrite("\1", CHAR, 1, toSat);
						fflush(toSat);
					}
				}
			}

			thrusterCommandA = (char)duration;
			
			strBuild[0] = strBuild[1] = strBuild[2] = '\0';

			statementPending = 0;			
			sprintf(inputStatements, "Which thruster? (l, r, u, d)\0");
			inputStatementLength = strlen(inputStatements);
			statementPending = 1;
		
			fwrite("\1", CHAR, 1, toSat);
			fflush(toSat);

			while(thrusterCommandB == 0 && signal)
			{
				c = getchar();
				if('l' == c)
				{
					thrusterCommandB += (1 << 3);
				}
				else if ('r' == c)
				{
					thrusterCommandB += (1 << 2);
				}
				else if ('u' == c)
				{
					thrusterCommandB += (1 << 1);
				}
				else if ('d' == c)
				{
					thrusterCommandB += (1 << 0);
				}
			}
			
			statementPending = 0;
			sprintf(inputStatements, "Magnitude? (0 - 15)\0");
			inputStatementLength = strlen(inputStatements);	
			statementPending = 1;
			
			fwrite("\1", CHAR, 1, toSat);
			fflush(toSat);

			i = 0;
			sprintf(inputPrompt, "%s", inputStatements);
			while(magnitude > 15 && signal)
			{
				c = getchar();
				if(c == '\n' || c == '\r')
				{
					if(strBuild[0] != '\0')
					{
						magnitude = 0;
						for(int j = 0; j < i; j++)
						{
							int multiplier = 1;
							for(int k = 0; k < j; k++)
							{
								multiplier *= 10;
							}
							magnitude += (strBuild[i - j - 1] - 48) * multiplier;
						}
						if(magnitude > 15)
						{
							magnitude = 15;
						}
					}
				}
				else if(c > 47 && c < 58)
				{
					strBuild[i] = c;
					i++;
					statementPending = 0;
					sprintf(inputStatements, "%s %c%c\0", inputPrompt, strBuild[0], strBuild[1]);
					inputStatementLength = strlen(inputStatements);
					statementPending = 1;
					if(i == 2)
					{
						magnitude = 0;
						for(int j = 0; j < i; j++)
						{
							int multiplier = 1;
							for(int k = 0; k < j; k++)
							{
								multiplier *= 10;
							}
							magnitude += (strBuild[i - j - 1] - 48) * multiplier;
						}
						if(magnitude > 15)
						{
							magnitude = 15;
						}
					}
					else
					{
						fwrite("\1", CHAR, 1, toSat);
						fflush(toSat);
					}
				}
			}
			
			strBuild[0] = strBuild[1] = strBuild[2] = '\0';
			
			thrusterCommandB += (char)(magnitude << 4);

			statementPending = 0;
			sprintf(inputStatements, "    \0");
			inputStatementLength = strlen(inputStatements);
			statementPending = 1;
	
			fwrite("\1", CHAR, 1, toSat);
			fflush(toSat);

			char message[2];
			sprintf(message, "%c%c", thrusterCommandA, thrusterCommandB);
			fwrite(message, CHAR, 2, toSat);
			fflush(toSat);
		}
		else
		{
			fwrite(&c, CHAR, 1, toSat);
			if(c != ' ')
				lastCommand = c;
			fflush(toSat);
		}
	}
}
void main() 
{
	FILE * ch0; 
	FILE * ch1;
	
	statementPending = 1;
	sprintf(inputStatements, ".....");
	inputStatements[59] = '\0';
	inputStatementLength = 5;
	
	setNonblockTerminalMode();

	signal = 1;

	void (*f1)(void *) = recieveData;
	void (*f2)(void *) = sendData;

	ch0 = fopen("./fromSat", "r"); 
	ch1 = fopen("./toSat", "w"); 

	pthread_t thread[NUM_THREAD];
	
	pthread_create(&thread[0], NULL, (void *) f1, (void *) ch0);
    pthread_create(&thread[1], NULL, (void *) f2, (void *) ch1);

	pthread_join(thread[0], NULL);
   	pthread_join(thread[1], NULL); 	
	pthread_exit(NULL);

	fclose(ch0); 
	fclose(ch1);
	printf("\033[2J");
}
