#include "satellite.h"
#include <stdio.h>
#include <stdlib.h>
#include "bbbIO.h"
#include "consoleDisplay.h"
#include "display.h"
#include "styleSheet.h"

#define BUFFER_LENGTH 100

// global variables
extern bool solarPanelState;
extern bool solarPanelDeploy;
extern bool solarPanelRetract;
extern bool fuelLow; 
extern bool batteryLow; 
extern bool driveMotorSpeedInc;
extern bool driveMotorSpeedDec;
extern unsigned int * batteryLevelPtr;
extern unsigned int * batteryTempPtr;
extern unsigned short fuelLevel;
extern unsigned short powerConsumption; 
extern unsigned short powerGeneration;
extern unsigned int mode;
extern double imageFrequency;
extern unsigned int transportPeriod;


// the corresponding data struct for console display data
struct consoleDisplayData
{
	bool  solarPanelState;
    bool  solarPanelDeploy;
	bool  solarPanelRetract;	
	bool  fuelLow; 
	bool  batteryLow; 
	bool  driveMotorSpeedInc;
	bool  driveMotorSpeedDec;
	bool  mode;
	bool  overTemp;
	unsigned short pirateProximity;
	unsigned short  batteryLevel;
	unsigned short batteryTemp;
	unsigned short  fuelLevel;
	unsigned short  powerConsumption; 
	unsigned short  powerGeneration;
	double imageFrequency;
	unsigned int transportPeriod;
	circularArray distances;
    window_t * window; 	
};
typedef struct consoleDisplayData dataStruct; 

// function prototypes 
void consoleDisplay(void * data); 

/**
 * This function makes a new TCB for console display
 *
 * @return the TCB for the consoleDisplay 
 */
TCB getConsoleDisplay(window_t * window)
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 

	// -1 to each values so the values are initialized to different
	data->solarPanelState = !solarPanelState;
	data->solarPanelDeploy = !solarPanelDeploy;
	data->solarPanelRetract = !solarPanelRetract;
	data->driveMotorSpeedInc = !driveMotorSpeedInc;
	data->driveMotorSpeedDec = !driveMotorSpeedDec;
	data->fuelLow = !fuelLow; 
	data->batteryLow = !batteryLow; 
	data->batteryLevel = ((unsigned short)*batteryLevelPtr) - 1;
    data->batteryTemp = ((unsigned short)*batteryTempPtr) - 1;	
	data->fuelLevel = fuelLevel - 1;
	data->powerConsumption = powerConsumption - 1;
	data->powerGeneration = powerGeneration - 1; 
	data->window = window;
	data->mode = !mode;
	data->imageFrequency = imageFrequency - 1;
	data->transportPeriod = transportPeriod - 1;
	data->pirateProximity = pirateProximity - 1;

	theTCB.task = consoleDisplay; 
	theTCB.taskDataPtr = data; 
	theTCB.pName = "Console Display";

	return theTCB; 
}

/**
 * This function prints the status and annunciation modes
 * for the satellite. 
 *
 * @param the given data struct for console display
 */
void consoleDisplay(void * data) 
{
	
	// data ruct for console display
	dataStruct *prevData = (dataStruct *)data;

	// checks if any value has changed
	if (prevData->solarPanelState != solarPanelState ||
			prevData->batteryLevel != *batteryLevelPtr ||
			prevData->fuelLow != fuelLow ||
			prevData->batteryLow != batteryLow ||
			prevData->fuelLevel != fuelLevel ||
			prevData->powerConsumption != powerConsumption ||
			prevData->powerGeneration != powerGeneration ||
			prevData->batteryTemp != *batteryTempPtr ||
			prevData->driveMotorSpeedInc != driveMotorSpeedInc ||
			prevData->driveMotorSpeedDec != driveMotorSpeedDec ||
			prevData->imageFrequency != imageFrequency ||
			prevData->transportPeriod != transportPeriod ||
			prevData->mode != mode ||
			prevData->pirateProximity != pirateProximity)
	{

	// alerts when fuel is low or battery is low 
	unsigned int alert = (fuelLow) || (batteryLow) || overTemperature;
	
		char buffer[BUFFER_LENGTH];

		// mode = getGPIOValue(MODE_PIN);

		color_t color = TEXT_DEFAULT;

		// transfering data to the earth terminal 
		if (!mode) // status mode
		{
			windowClear(prevData->window);
			if (alert) // battery or fuel is low 
			{
				windowClear(prevData->window);
				windowPrintColor(prevData->window, "ANNUNCIATION MODE:", HEADER, 1, 50);
			   	sprintf(buffer, "Fuel Low    = %d \0", fuelLow);
				color = NORMAL_COLOR;
				if (fuelLow)
						color = WARNING_COLOR;
				windowPrintColor(prevData->window, buffer, color, 3, 52); 
				sprintf(buffer, "Battery Low = %d \0", batteryLow);
			   	color = NORMAL_COLOR;
				if (batteryLow)
						color = WARNING_COLOR;
				windowPrintColor(prevData->window, buffer, color, 5, 52);  
				sprintf(buffer, "Over Temp   = %d \0", overTemperature); 
				color = NORMAL_COLOR;
				if (overTemperature)
						color = WARNING_COLOR;
				windowPrintColor(prevData->window, buffer, color, 7, 52);  
			}
		
			windowPrintColor(prevData->window, "STATUS MODE:", HEADER, 1, 2);
			if(driveMotorSpeedInc)
		   		sprintf(buffer, "Solar Panel State = %d+\0", solarPanelState);
			else if (driveMotorSpeedDec)
		   		sprintf(buffer, "Solar Panel State = %d-\0", solarPanelState);
			else
		   		sprintf(buffer, "Solar Panel State = %d\0", solarPanelState);

		   	color = TEXT_DEFAULT;
			if (!(solarPanelDeploy || solarPanelRetract))
				color = STABLE_COLOR;
			else if (solarPanelDeploy)
				color = DEPLOY_COLOR;
			else if (solarPanelRetract)
				color = RETRACT_COLOR;
			windowPrintColor(prevData->window, buffer, color, 3, 4); 
			sprintf(buffer, "Battery Level     = %d\0", batteryLevelPtr[0]);
			color = FULL_COLOR;
			if (*batteryLevelPtr < 50)
					color = MID_COLOR;
			if (*batteryLevelPtr < 10)
					color = LOW_COLOR;
			windowPrintColor(prevData->window, buffer, color, 5, 4); 
			color = FULL_COLOR;
			if (batteryTempPtr[0] > 50)
					color = MID_COLOR;
			if (batteryTempPtr[0] > 90)
					color = LOW_COLOR;
			sprintf(buffer, "Battery Temp      = %d\0", batteryTempPtr[0]);
			windowPrintColor(prevData->window, buffer, color, 7, 4); 
			sprintf(buffer, "Fuel Level        = %d\0", fuelLevel);
			color = FULL_COLOR;
			if (fuelLevel < 50)
					color = MID_COLOR;
			if (fuelLevel < 10)
					color = LOW_COLOR;
			windowPrintColor(prevData->window, buffer, color, 9, 4); 
			sprintf(buffer, "Power Consumption = %d\0", powerConsumption);
		   	color = DATA_COLOR;
			if (powerConsumption > 0)
					color = MID_COLOR;
			if (powerConsumption > 5)
					color = LOW_COLOR;
		   	windowPrintColor(prevData->window, buffer, color, 11, 4); 
			sprintf(buffer, "Power Generation  = %d\0", powerGeneration);
		   	color = MID_COLOR;
			if (powerGeneration > powerConsumption)
					color = FULL_COLOR;
			if (powerGeneration == 0 || powerGeneration == powerConsumption)
					color = DATA_COLOR;
			windowPrintColor(prevData->window, buffer, color, 13, 4);
			color = HEADER;
			windowPrintColor(prevData->window, "Image Frequencies: ", color, 9, 50);
			sprintf(buffer, "[%-08.3lf %-08.3lf %-08.3lf %-08.3lf %-08.3lf %-08.3lf %-08.3lf %-08.3lf\0", freqBuffer[0],
				freqBuffer[1], freqBuffer[2], freqBuffer[3], freqBuffer[4], freqBuffer[5], 
				freqBuffer[6], freqBuffer[7]);
			color = DATA_COLOR;
			windowPrintColor(prevData->window, buffer, color, 11, 52);
			sprintf(buffer, " %-08.3lf %-08.3lf %-08.3lf %-08.3lf %-08.3lf %-08.3lf %-08.3lf %-08.3lf]\0", freqBuffer[8],
				freqBuffer[9], freqBuffer[10], freqBuffer[11], freqBuffer[12], freqBuffer[13], 
				freqBuffer[14], freqBuffer[15]);
			color = DATA_COLOR;
			windowPrintColor(prevData->window, buffer, color, 12, 52);
			sprintf(buffer, "Drive Speed      = %d %%\0", driveMotorSpeed);
			color = DATA_COLOR;
			windowPrintColor(prevData->window, buffer, color, 1, 90);
			sprintf(buffer, "Transport Period = %d ns\0", transportPeriod);
			color = DATA_COLOR;
			windowPrintColor(prevData->window, buffer, color, 3, 90);
			if(pirateProximity > 100 || pirateDestroyed)
				sprintf(buffer, "Pirate Proximity = never too far\0");
			else
				sprintf(buffer, "Pirate Proximity = %d m\0", pirateProximity);
			color = DATA_COLOR;
			windowPrintColor(prevData->window, buffer, color, 5, 90);
			
		}
		else if ( prevData->batteryLow != batteryLow ||
					   	prevData->fuelLow != fuelLow ||
						prevData->overTemp != overTemperature ||
					   	prevData->mode != mode )// annunciation mode 
		{
			windowClear(prevData->window);
			// print annunciation mode to earth
			windowPrintColor(prevData->window, "ANNUNCIATION MODE:", HEADER, 1, 2);
	   		sprintf(buffer, "Fuel Low    = %d \0", fuelLow); 				
			color = NORMAL_COLOR;
			if (fuelLow)
					color = WARNING_COLOR;
			windowPrintColor(prevData->window, buffer, color, 3, 4); 
			sprintf(buffer, "Battery Low = %d \0", batteryLow); 
			color = NORMAL_COLOR;
			if (batteryLow)
					color = WARNING_COLOR;
			windowPrintColor(prevData->window, buffer, color, 5, 4);  
			sprintf(buffer, "Over Temp   = %d \0", overTemperature); 
			color = NORMAL_COLOR;
			if (overTemperature)
					color = WARNING_COLOR;
			windowPrintColor(prevData->window, buffer, color, 7, 4);  
		} 

		prevData->solarPanelState = solarPanelState;
		prevData->batteryLevel = batteryLevelPtr[0];
		prevData->batteryTemp = batteryTempPtr[0];
		prevData->fuelLow = fuelLow;
		prevData->batteryLow = batteryLow;
		prevData->fuelLevel = fuelLevel;
		prevData->powerConsumption = powerConsumption;
		prevData->powerGeneration = powerGeneration;
		prevData->driveMotorSpeedInc = driveMotorSpeedInc;
		prevData->driveMotorSpeedDec = driveMotorSpeedDec;
		prevData->mode = mode;
		prevData->overTemp = overTemperature;
		prevData->imageFrequency = imageFrequency;
		prevData->transportPeriod = transportPeriod;
	} 
}

