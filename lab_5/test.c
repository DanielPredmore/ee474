/**
 * This is the test module for the satellite code
 * It will run tests on each of the c files and 
 * print their status to the terminal.
 */

#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include "satellite.h"
#include "consoleDisplay.h"
#include "powerSubsystem.h"
#include "satelliteComms.h"
#include "thrusterSubsystem.h"
#include "warningAlarm.h"

void runTask(TCB *task, unsigned int n, unsigned int t);
void testPowerSubsystem();
double timeTask(TCB *task);
double timerTask(TCB *task);

int main(int argc, char **argv)
{
	printf("Begin testing satellite software systems\n");

	printf("Testing satellite.h initialization of global vars\n");
	initializeGlobals();

	printf("thrusterCommand: ");
	assert(thrusterCommand == NO_COMMAND);
	printf("OK\n");

	printf("batteryLevel: ");
	assert(batteryLevel == FULL);
	printf("OK\n");


	printf("fuelLevel: ");
	assert(fuelLevel == FULL);
	printf("OK\n");

	printf("powerConsumption: ");
	assert(powerConsumption == 0);
	printf("OK\n");

	printf("powerGeneration: ");
	assert(powerGeneration == 0);
	printf("OK\n");

	/*
	printf("solarPanelDeployed: ");
	assert(solarPanelDeployed == FALSE);
	printf("OK\n");
	*/

	printf("fuelLow: ");
	assert(fuelLow == FALSE);
	printf("OK\n");
	
	printf("batteryLow: ");
	assert(batteryLow == FALSE);
	printf("OK\n");

	printf("Global initialization is good\n");

	printf("\n");

	/*
	printf("Testing LED driver\n");
	
	printf("Turning off LEDs 1-3\n");
	setLed(1, FALSE);
	setLed(2, FALSE);
	setLed(3, FALSE);

	assert(checkLed(1) == FALSE);
	assert(checkLed(2) == FALSE);
	assert(checkLed(3) == FALSE);

	printf("Works!!!\n");

	printf("Turning on LEDs 1-3\n");
	setLed(1, TRUE);
	setLed(2, TRUE);
	setLed(3, TRUE);

	assert(checkLed(1) == TRUE);
	assert(checkLed(2) == TRUE);
	assert(checkLed(3) == TRUE);

	printf("Works!!!\n");

	setLed(1, FALSE);
	setLed(2, FALSE);
	setLed(3, FALSE);

	printf("LED test is done\n");
	printf("If LEDs did not work, make sure the path in ledDriver.h is right\n");
	printf("Also make sure to run as root or sudo to use LEDs\n");
	*/

	printf("\n");

	printf("Testing the warning alarm system\n");
	TCB tcb = getWarningAlarm();
	
	printf("LED 3 should be on\n");
	runTask(&tcb, -1, 5);

	batteryLevel = 25;
	fuelLevel = 25;
	printf("LEDs 2 and 3 should blink slowly\n");
	runTask(&tcb, -1, 8);

	batteryLevel = 0;
	fuelLevel = 0;
	printf("LEDs 2 and 3 should blink faster\n");
	runTask(&tcb, -1, 5);

	/*
	setLed(1, FALSE);
	setLed(2, FALSE);
	setLed(3, FALSE);
	*/

	initializeGlobals();
	
	printf("\n");

	/*
	printf("Testing the power subsystem\n");
	testPowerSubsystem();
	printf("Power subsystem works\n");
	*/

	initializeGlobals();
	printf("\n");

	printf("Testing the thruster subsystem\n");
	tcb = getThrusterSubsystem();

	printf("Success the code is working\n");

	printf("Timing fuctions\n");
	
	initializeGlobals();
	tcb = getSatelliteComms();
	double microSec = timerTask(&tcb);
	printf("Satellite Communications takes %f micro seconds\n", microSec);

	initializeGlobals();
	tcb = getWarningAlarm();
	microSec = timeTask(&tcb);
	printf("Warning Alarm takes %f micro seconds\n", microSec);

	/*
	setLed(1, FALSE);
	setLed(2, FALSE);
	setLed(3, FALSE);
	*/

	initializeGlobals();
	tcb = getPowerSubsystem();
	microSec = timeTask(&tcb);
	printf("Power Subsystem takes %f micro seconds\n", microSec);

	initializeGlobals();
	tcb = getThrusterSubsystem();
	microSec = timeTask(&tcb);
	printf("Thruster Subsystem takes %f micro seconds\n", microSec);

	initializeGlobals();
	tcb = getConsoleDisplay();
	microSec = timeTask(&tcb);
	printf("Console Display takes %f micro seconds\n", microSec);

	return 0;
}


void runTask(TCB *task, unsigned int n, unsigned int t)
{
	time_t now;
	time_t stop;
	int i = 0;

	time(&now);
	stop = now + t;
	while ( i < n && now < stop )
	{
		task->task(task->taskDataPtr);
		time(&now);
		i++;
	}
}

/*
void testPowerSubsystem()
{
unsigned short bll = batteryLevel;
	unsigned short pgl = powerGeneration;
	unsigned short pcl = powerConsumption;
	bool spdl = solarPanelDeployed;
	unsigned short minBat = batteryLevel;

	TCB tcb = getPowerSubsystem();

	int i;
	for (i = 0; i < 1000000; i++)
	{
		tcb.task(tcb.taskDataPtr);

		if (batteryLevel < 0 || batteryLevel > 100)
		{
			printf("Error: Battery is out of range\n");
			exit(1);
		}

		if (batteryLevel == 0)
		{
			printf("Error: Batter is dead\n");
			printf("Battery level last: %d\n", bll);
			printf("Power consumption: %d\n", powerConsumption);
			printf("Power generation: %d\n", powerGeneration);
			exit(1);
		}

		if (solarPanelDeployed)
		{
			if (batteryLevel != bll - powerConsumption + powerGeneration && batteryLevel != 0 && batteryLevel != 100)
			{
				printf("Error: Battery is not charging correctly\n");
				printf("Battery level: %d\n", batteryLevel);
				printf("Battery level Last: %d\n", bll);
				printf("Power consumption: %d\n", powerConsumption);
				printf("Power consumption Last: %d\n", pcl);
				printf("Power generation: %d\n", powerGeneration);
				printf("Poser generation Last: %d\n", pgl);
				exit(1);
			}
			if ((powerGeneration < pgl ||
				(bll < 50 && powerGeneration < pgl + 1)|| 
				powerGeneration > pgl + 2) && solarPanelDeployed)
			{
				printf("Error: Power generation is not changing correctly\n");
				printf("Battery level: %d\n", batteryLevel);
				printf("Battery level last: %d\n", bll);
				printf("Power Generation: %d\n", powerGeneration);
				printf("Power generation last: %d\n", pgl);
				exit(1);
			}
		}
		else if (batteryLevel != bll - 3 * powerConsumption && batteryLevel != 0)
		{
			printf("Error: Battery is not consuming properly\n");
			exit(1);
		}
		
		if (powerConsumption < pcl - 2 || powerConsumption > pcl + 2)
		{
			printf("Error: Power consumption is not changing correctly\n");
			printf("Power consumption: %d\n", powerConsumption);
			printf("Power consumption last: %d\n", powerConsumption);
			exit(1);
		}

		if (minBat > batteryLevel)
				minBat = batteryLevel;

		bll = batteryLevel;
		pgl = powerGeneration;
		pcl = powerConsumption;
		spdl = solarPanelDeployed;
	}

	printf("The battery reached a low %d\%\n", minBat);
}
*/

double timeTask(TCB *task)
{
	int i;
	clock_t start, stop;
	start = clock();
	for (i = 0; i < 1000000; i++)
	{
		task->task(task->taskDataPtr);
	}
	stop = clock();
	double total = ((double)(stop - start)) / CLOCKS_PER_SEC;
	return total;
}

double timerTask(TCB *task)
{
	int i;
	clock_t start, stop;
	start = clock();
	for (i = 0; CLOCKS_PER_SEC > clock() - start; i++)
	{
		task->task(task->taskDataPtr);
	}
	stop = clock();
	return ((double)(stop - start)) * 1000000 / CLOCKS_PER_SEC / i;
}
