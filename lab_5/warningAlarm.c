#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "warningAlarm.h"
#include "satellite.h"
#include "bbbIO.h"

#define ALARM_GPIO 47
#define ONE_SEC (CLOCKS_PER_SEC)
#define TWO_SEC (2*CLOCKS_PER_SEC)
#define TEN_HZ (CLOCKS_PER_SEC/20)
#define FIVE_SEC (10*CLOCKS_PER_SEC)


extern unsigned int *batteryLevel;
extern unsigned short fuelLevel;

struct warningAlarmData {
	bool ledStates[3];
	clock_t ledActive[3];
	bool overTemp;
	bool alarm;
	bool alt;
	bool act;
	clock_t change;
	clock_t wait;
	clock_t update[3];
};
typedef struct warningAlarmData dataStruct;

// private function prototypes
void warningAlarm(void *data);
void checkSetLed(unsigned int led, bool value, dataStruct *data);
void blinkLed(unsigned int led, clock_t rate, dataStruct *data);

/**
 * This function makes a new TCB task for the kernel
 *
 * @return the TCB for the warningAlarm
 */
TCB getWarningAlarm() 
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));

	setGPIOValue(USR1, FALSE);
	setGPIOValue(USR2, FALSE);
	setGPIOValue(USR3, FALSE);
	setGPIOValue(ALARM_GPIO, FALSE);

	// here is space to initialize the data struct's data
	data->ledStates[0] = FALSE;
	data->ledStates[1] = FALSE;
	data->ledStates[2] = FALSE;
	data->ledActive[0] = 0;
	data->ledActive[1] = 0;
	data->ledActive[2] = 0;
	data->overTemp = FALSE;
	data->alarm = FALSE;
	data->act = FALSE;

	// setting the values in the TCB
	theTCB.task = warningAlarm;
	theTCB.taskDataPtr = data;
	theTCB.pName = "Warning Alarm";
	return theTCB;
}

/**
 * This is the main TCB task function for the warning alarm system.
 * It is responsible for setting the warning lights
 *
 * @param data this is the data struct form the PCB
 */
void warningAlarm(void *data)
{
	// casting back to dataStruct
	dataStruct *theData = (dataStruct*)data;
	
	if(overTemperature && !theData->act)
	{
		if (!theData->overTemp)
		{
			theData->wait = clock();
			setGPIOValue(ALARM_GPIO, TRUE);
			theData->alarm = TRUE;
			theData->overTemp = TRUE;
		}
		else if (currentInput == 'a')
		{
			theData->act = TRUE;
			theData->alarm = FALSE;
			setGPIOValue(ALARM_GPIO, FALSE);
			currentInput = '\0';
			overTemperature = FALSE;
		}
		else if (clock() - theData->wait > 15*CLOCKS_PER_SEC)
		{
			if (theData->alarm)
			{
				setGPIOValue(ALARM_GPIO, FALSE);
				theData->alarm = FALSE;
				theData->change = clock();
				theData->alt = FALSE;
			}
			else if (clock() - theData->change > 5*CLOCKS_PER_SEC)
			{
				theData->alt = !theData->alt;
				theData->change = clock();
			}

			if (theData->alt)
			{
				checkSetLed(USR1, TRUE, theData);
				checkSetLed(USR2, TRUE, theData);
			}
			else
			{
				blinkLed(USR1, TEN_HZ, theData);
				blinkLed(USR2, TEN_HZ, theData);
			}
		}
		else
		{
			if (*batteryLevelPtr <= 50 && *batteryLevelPtr > BATTERY_LOW)
				blinkLed(USR2, TWO_SEC, theData); 
			else if (*batteryLevelPtr <= BATTERY_LOW) 
				blinkLed(USR2, ONE_SEC, theData);
			else
			{
				checkSetLed(USR2, FALSE, theData);
			}

			if (fuelLevel <= 50 && fuelLevel > FUEL_LOW)
				blinkLed(USR1, TWO_SEC, theData);
			else if (fuelLevel <= FUEL_LOW)
				blinkLed(USR1, ONE_SEC, theData);
			else
			{
				checkSetLed(USR1, FALSE, theData);
			}
		}
	}
	else
	{
		if (!overTemperature)
				theData->act = FALSE;
		
		theData->overTemp = FALSE;
		if (theData->alarm)
		{
			setGPIOValue(ALARM_GPIO, FALSE);
			theData->alarm = FALSE;
		}


		if (*batteryLevelPtr <= 50 && *batteryLevelPtr > BATTERY_LOW)
		{
			blinkLed(USR2, TWO_SEC, theData); 
		}
		else if (*batteryLevelPtr <= BATTERY_LOW) 
			blinkLed(USR2, ONE_SEC, theData);
		else
		{
			checkSetLed(USR2, FALSE, theData);
		}

		if (fuelLevel <= 50 && fuelLevel > FUEL_LOW)
			blinkLed(USR1, TWO_SEC, theData);
		else if (fuelLevel <= FUEL_LOW)
			blinkLed(USR1, ONE_SEC, theData);
		else
		{
			checkSetLed(USR1, FALSE, theData);
		}
	}	


	// checking the warning conditions
	if (*batteryLevelPtr > 50 && fuelLevel > 50)
		checkSetLed(USR3, TRUE, theData);
	else
	{
		checkSetLed(USR3, FALSE, theData);
	}	
}

/**
 * checkSetLed compares the current value of an led to the set value
 * if they are different, it updates the led to the new value.
 * Otherwise it keep the led the same and does nothing
 *
 * @param led the led #
 * @param value the value of the led
 * @param data a pointer to the data struct
 */
void checkSetLed(unsigned int led, bool value, dataStruct *data)
{
	setGPIOValue(led, value);
	data->ledStates[led-USR1] = value;
}

/**
 * This function blinks an led at a set rate
 *
 * @param led the led #
 * @param rate the speed of the blink in seconds
 * @param data a pointer to the data struct
 */
void blinkLed(unsigned int led, clock_t rate, dataStruct *data)
{
	clock_t now = clock();
	if (data->ledActive[led-USR1] != rate)
	{
		data->ledActive[led-USR1] = rate;
		data->update[led-USR1] = now + rate;
		checkSetLed(led, !data->ledStates[led-USR1], data);
	}
	else if (now > data->update[led-USR1])
	{
		data->update[led-USR1] = now + rate;
		checkSetLed(led, !data->ledStates[led-USR1], data);
	}
}
