/**
 * satellite.c initializes all global variables
 */

#include <stdio.h>
#include <unistd.h> 
#include "satellite.h"
#include "bbbIO.h"

// global variables
extern unsigned int thrusterCommand;
extern unsigned int pirateProximity;
extern unsigned int mode; 
extern unsigned short fuelLevel;
extern unsigned short powerConsumption;
extern unsigned short powerGeneration;
extern char command; 
extern char response; 
extern bool solarPanelState;
extern bool solarPanelDeploy; 
extern bool solarPanelRetract;
extern bool fuelLow;
extern bool batteryLow;
extern unsigned int mode; 
extern unsigned int * batteryLevelPtr;
extern unsigned int * batteryTempPtr;
extern bool driveMotorSpeedInc;
extern bool driveMotorSpeedDec;
extern char currentInput;
extern unsigned short inputChecks;
extern char gameCommand;
extern circularArray distances;
extern bool overTemp; 
extern unsigned int transportPeriod;
extern unsigned int transportDutyCycle;
extern double imageFrequency;

extern bool transportSignal;
extern bool fullyDeployed;
extern bool fullyRetracted;
extern bool measureTemp;
extern bool pirateDetected;
extern bool startImageCapture;
extern bool loadImageData;
extern bool display; 
extern bool inputError;

extern bool transportSignalAck;
extern bool fullyDeployedAck;
extern bool fullyRetractedAck;
extern bool measureTempAck;
extern bool pirateDetectAck;
extern bool startImageAck;
extern bool commandRunningAck;
extern unsigned int driveMotorSpeed;

extern double freqBuffer[16];

extern signed int transportDistPid;
extern signed int batteryTempReadPid;

unsigned int batteryLevel;
unsigned int batteryTemperature;


void initializeGlobals() {
	thrusterCommand = NO_COMMAND;
	displayHide = FALSE; 
	inputError = FALSE; 
	pirateProximity = 200;
	pirateDetected = FALSE;
	fuelLevel = FULL;
	powerConsumption = 0;
	powerGeneration = 0;
	command = '\0';
	response = '\0';
	solarPanelState = FALSE;
	solarPanelDeploy = FALSE; 
	solarPanelRetract = FALSE;
	fuelLow = FALSE;
	batteryLow = FALSE;
	mode = 0;
   	batteryLevelPtr = &batteryLevel;
	batteryTempPtr = &batteryTemperature;
	driveMotorSpeedInc = FALSE;
	driveMotorSpeedDec = FALSE;
	currentInput = '\0';
	inputChecks = 0;
	gameCommand = '\0';
	overTemp = FALSE;
	for (int i = 0; i < DATA_SIZE; i++)
	{
		distances.array[i] = 0.0;
	}
	distances.head = (distances.array);
	distances.tail = (distances.array); 
	transportPeriod = 0;
	transportDutyCycle = 0;
	imageFrequency = 0.0;
	batteryLevel = 100;
	batteryTemperature = 33;
	driveMotorSpeed = 50;
	transportSignal = FALSE;
	fullyDeployed = FALSE;
	fullyRetracted = TRUE;
	measureTemp = FALSE;
	measureBatt = FALSE;
	transportSignalAck = FALSE;
	fullyDeployedAck = FALSE;  
	fullyRetractedAck = FALSE;
	pirateDetectAck = FALSE;
	startImageCapture = FALSE;
	loadImageData = FALSE;	
	startImageAck = FALSE;
	commandRunning = FALSE;
	commandRunningAck = FALSE;
	pirateDestroyed = FALSE;
	for(int i = 0; i < 16; i++)
		freqBuffer[i] = 0.0;
}
