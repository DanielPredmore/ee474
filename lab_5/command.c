#include <signal.h>
#include "satellite.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "display.h"
#include "keypad.h"

#define CHAR sizeof(char)
#define BUFFER_SIZE 100
#define COMMAND_SIZE 10 
#define PROMPT_BUFFER "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\0"
#define PROMPT_BUFFER2 "\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\5\0"

// global variables
extern bool inputError; 
extern bool displayHide;
extern bool transportSignal; 
extern bool measureTemp; 
extern bool startImageCapture;
extern char command;
extern unsigned int thrusterCommand;
extern circularArray distances; 
extern double imageFrequency; 

int fildes;

// prototypes
void commandTask(void * data); 

// data struct
typedef struct commandData
{
	FILE * earth;
	bool * inputError;
	bool * displayHide;
	char * command; 
	unsigned int * thrusterCommand; 
	bool * transportSignal;
	bool * measureTemp; 
	bool * measureBatt;
	bool * startImageCapture;
	double * imageFrequency; 
	circularArray * distances; 
	window_t * window; 
} dataStruct; 

/**
 *	Returns the TCB block for the kernel 
 *	@return TCB block for command task
 */
TCB getCommandTask(window_t * window) 
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));

	fildes = open("toSat", O_RDONLY | O_NONBLOCK);
	//(data->earth) = fopen("./toSat", "r");
	(data->earth) = fdopen(fildes, "r");
	//fildes = fileno((data->earth));
	(data->window) = window;
	(data->imageFrequency) = &imageFrequency;
	(data->distances) = &distances;
	(data->inputError) = &inputError;
	(data->displayHide) = &displayHide;
	(data->command) = &command; 
	(data->thrusterCommand) = &thrusterCommand; 
   	(data->transportSignal) = &transportSignal; 
	(data->measureTemp) = &measureTemp;
	(data->measureBatt) = &measureBatt;
	(data->startImageCapture) = &startImageCapture;	
	
	theTCB.task = &commandTask;
	theTCB.taskDataPtr = data; 
	theTCB.pName = "Command Task";

	return theTCB;
}

void commandTask(void * data) 
{
	char input = '\0'; 
	char buffer[BUFFER_SIZE];
	static char command[COMMAND_SIZE] = "\0\0\0\0";
	static char lastCommand[BUFFER_SIZE] = "NO COMMAND\0";
	static bool interruptsSet = FALSE;
	static bool lastCommandInvalid = FALSE;
	static bool update = TRUE;
	static bool promptSet = FALSE;	

	// initialize data struct
	dataStruct * cData = (dataStruct *) data; 
	
	//windowPrint(cData->window, "COMMANDS:\0", 1, 2);  
	
	// read from earth input
	if( command[0] == '\0')
	{
		// restart error flag
		*(cData->inputError) = FALSE;
		if(fread((void*)&input, CHAR, 1, cData->earth) == 1)
		{
			command[0] = input; 
			update = TRUE;
		}
		else
		{
			update = FALSE;
		}
	}	
	// start interrupts
	else if (command[0] == 'S')
	{
		if(interruptsSet == FALSE)
		{
			*(cData->transportSignal) = TRUE;
			*(cData->measureTemp) = TRUE;
		    *(cData->measureBatt) = TRUE;	
			*(cData->startImageCapture) = TRUE;
			raise(SIGUSR1);
			interruptsSet = TRUE;
		}
		command[0] = '\0';
		sprintf(lastCommand, "A S                   \0");
		lastCommandInvalid = FALSE;
	}
	// stop interrupts
	else if (command[0] == 'P')
	{
		if(interruptsSet == TRUE)
		{
			*(cData->transportSignal) = FALSE;
			*(cData->measureTemp) = FALSE; 
		    *(cData->measureBatt) = FALSE;	
			*(cData->startImageCapture) = FALSE;
			interruptsSet = FALSE;
		}
		command[0] = '\0';
		sprintf(lastCommand, "A P                   \0");
		lastCommandInvalid = FALSE;
	}
	// display command
	else if (command[0] == 'D')
	{
		*cData->displayHide = !*cData->displayHide;
	   	if(*cData->displayHide == TRUE)
			displaySetHide(cData->window->display, TRUE);
		else
			displaySetHide(cData->window->display, FALSE);	
		command[0] = '\0';
		sprintf(lastCommand, "A D                   \0");
		lastCommandInvalid = FALSE;
	} 
	// thruster commands
	else if (command[0] == 'T')
	{
		sprintf(lastCommand, "A T                  \0");
		lastCommandInvalid = FALSE;
		*(cData->inputError) = FALSE;
		if(command[1] == '\0')
		{
			if(fread((void*) &input, CHAR, 1, cData->earth) == 1)
			{
				if(input != ' ')
				{
					*(cData->inputError) = TRUE;
					command[0] = '\0';
					command[1] = '\0';
					lastCommandInvalid = TRUE;
				}
				else
				{
					command[1] = input;
				}
			}
		}
		else if (command[1] == ' ')
		{

			if(command[2] == '\0')
			{
				if(fread((void*) &input, CHAR, 1, cData->earth) == 1)
				{
					if(input == '\1')
					{
						promptSet = !promptSet;
					}
					else if(input != '\0')
					{
						command[2] = input;
					}
				}
			}
			else
			{
				if(fread((void*) &input, CHAR, 1, cData->earth) == 1)
				{
					command[3] = input;
					*(cData->thrusterCommand) = command[2];
					*(cData->thrusterCommand) = *(cData->thrusterCommand) << 8;
					*(cData->thrusterCommand) += command[3];
					command[0] = command[1] = command[2] = command[3] = '\0';
					sprintf(lastCommand, "A T %x                   \0", *(cData->thrusterCommand));
					lastCommandInvalid = FALSE;
				}
			}
		}
	}
	// mining vehicle commands
	else if (command[0] == 'V')
	{
		sprintf(lastCommand, "A V                  \0");
		lastCommandInvalid = FALSE;
		*(cData->inputError) = FALSE;
		if(command[1] == '\0')
		{
			if(fread((void*) &input, CHAR, 1, cData->earth) == 1)
			{
				if(input == ' ')
				{
					command[1] = input;
				}
				else
				{
					command[0] = command[1] = '\0';
					*(cData->inputError) = TRUE;
					lastCommandInvalid = TRUE;
				}
			}
		}
		else if (command[1] == ' ')
		{
			if(fread((void*) &input, CHAR, 1, cData->earth) == 1)
			{
				if (input == 'F' || input == 'B' || input == 'L' || input == 'R' || input == 'D'||	
					input == 'H' || input == 'S' || input == 'I')
				{
					*(cData->command) = input;
					command[0] = command[1] = '\0';
					sprintf(lastCommand, "A V %c                   \0", input);
					startImageCapture = TRUE;
					raise(SIGUSR1);
					lastCommandInvalid = FALSE;
				}
				else
				{
					*(cData->inputError) = TRUE;
					lastCommandInvalid = TRUE;
					command[0] = command[1] = '\0';
				}
			}	
		}
		else 
		{	
			*(cData->inputError) = TRUE; 
			lastCommandInvalid = TRUE;
		}
	}
	// measurements
	else if (command[0] == 'M')
	{
		sprintf(lastCommand, "A M                  \0");
		lastCommandInvalid = FALSE;
		*(cData->inputError) = FALSE;
		if(command[1] == '\0')
		{
			if(fread((void*) &input, CHAR, 1, cData->earth) == 1)
			{
				if(input == ' ')
				{
					command[1] = input;
				}
				else
				{
					command[0] = command[1] = '\0';
					*(cData->inputError) = TRUE;
					lastCommandInvalid = TRUE;
				}
			}
		}
		else if (command[1] == ' ')
		{
			if(fread((void*) &input, CHAR, 1, cData->earth) == 1)
			{
				if(input == 'I')
				{
					sprintf(lastCommand, "A M I: %08.3lf                   \0", imageFrequency);	
					command[0] = command[1] = '\0';
					lastCommandInvalid = FALSE;
				}
				else if(input == 'T')
				{
					sprintf(lastCommand, "A M T: %.1f                   \0", *(distances.tail));
					command[0] = command[1] = '\0';
					lastCommandInvalid = FALSE;
				}
				else
				{
					*(cData->inputError) = TRUE;
					command[0] = command[1] = '\0';
					lastCommandInvalid = TRUE;
				}
			}	
		}
		else 
		{	
			*(cData->inputError) = TRUE; 
			command[0] = command[1] = '\0';
			lastCommandInvalid = TRUE;
		}
	}
	else if (command[0] == 'q')
	{
		currentInput = 'q';
	}
	else if(command[0] > '0' && command[0] <= '9')
	{
		gameCommand = command[0];
		command[0] = '\0';
	}
	else
	{
		command[0] = '\0';
		*(cData->inputError) = TRUE; 
		lastCommandInvalid = TRUE;
		update = TRUE;
	}
	
	// error with command

	if(update)
	{	
		if (*(cData->inputError) == TRUE || lastCommandInvalid)
		{
			sprintf(buffer, "E: Invalid command                   \0"); 
			windowPrint(cData->window, buffer, 3, 4);  
		} 
		else // no errors
		{
			sprintf(buffer, "                                  \0"); 
			windowPrint(cData->window, buffer, 3, 4);  
			windowPrint(cData->window, lastCommand, 5, 4); 
		}
		if(promptSet)
		{
			windowPrint(cData->window, PROMPT_BUFFER, 9, 2);  
		}
		else
		{
			windowPrint(cData->window, PROMPT_BUFFER2, 9, 2);  
		}
	}
}

/*
void commandTask(void * data) 
{
	char input = '\0'; 
	char output = '\0';
	char buffer[BUFFER_SIZE];
	char command[COMMAND_SIZE];	

	// initialize data struct
	dataStruct * cData = (dataStruct *) data; 
	
	windowPrint(cData->window, "COMMANDS:", 1, 2);  
	
	// restart error flag
	*(cData->inputError) = FALSE; 

	// read from earth input
	fread((void*)&input, CHAR, 1, cData->earth);
 	
	command[0] = input; 

	// start interrupts
	if (input == 'S')
	{
		fread((void*) &input, CHAR, 1, cData->earth);
		//input = getInputFifo(fildes);
		command[1] = input; 
		if (input == '\n')
		{
			*(cData->transportSignal) = TRUE;
		    *(cData->measureTemp) = TRUE; 
			*(cData->startImageCapture) = TRUE;
			
			output = 'S';
			fwrite((void*)&output, CHAR, 1, cData->earth); 
		} 
		else
		{
			*(cData->inputError) = TRUE;
		}		
	}
	// stop interrupts
	else if (input == 'P')
	{
		fread((void*) &input, CHAR, 1, cData->earth);
		//input = getInputFifo(fildes);
		command[1] = input; 
		if (input == '\n') 
		{	
			*(cData->transportSignal) = FALSE;
		    *(cData->measureTemp) = FALSE; 
			*(cData->startImageCapture) = FALSE;

			output = 'P';
			fwrite((void*)&output, CHAR, 1, cData->earth); 
		}
		else 
		{
			*(cData->inputError) = TRUE; 
		}
	}
	// display command
	else if (input = 'D')
	{
		fread((void*) &input, CHAR, 1, cData->earth);
		// input = getInputFifo(fildes);
		command[1] = input; 
		if(input = '\n')
		{
			display = !display; 
		}
		else 
		{
			*(cData->inputError) = TRUE; 
		}
	} 
	// thruster commands
	else if (input == 'T')
	{
		fread((void*) &input, CHAR, 1, cData->earth);
		// input = getInputFifo(fildes);
		command[1] = input; 
		if (input == ' ') 
		{
			//fread((void*) &input, CHAR, 1, cData->earth);
			input = getInputFifo(fildes);
			*(cData->thrusterCommand) = input;
			*(cData->thrusterCommand) << 8; 
			//fread((void*) &input, CHAR, 1, cData->earth); 
			input = getInputFifo(fildes);
			*(cData->thrusterCommand) += input; 
		}
		else 
		{
			*(cData->inputError) = TRUE; 
		}
	}
	// mining vehicle commands
	else if (input == 'V')
	{
		fread((void*) &input, CHAR, 1, cData->earth); 
		// input = getInputFifo(fildes);
		command[1] = input; 
		if (input == ' ')
		{
			//fread((void*) &input, CHAR, 1, cData->earth);
			input = getInputFifo(fildes);
			command [2] = input; 
			if (input == 'F' || input == 'B' || input == 'L' || input == 'R' || input == 'D'||	
				input == 'H' || input == 'S' || input == 'I')
			{
				fread((void*) &input, CHAR, 1, cData->earth);
				// input = getInputFifo(fildes);
				command[3] = input; 
				if (input == '\n') 
				{
					*(cData->command) == input;
					fwrite((void*) &input, CHAR, 1, cData->earth);
				}
				else 
				{
				*(cData->inputError) = TRUE; 
				}
			}
			else
			{
				*(cData->inputError) = TRUE; 
			}
		}
		else 
		{	
			*(cData->inputError) = TRUE; 
		}
	}
	// measurements
	else if (input == 'M')
	{
		fread((void*) &input, CHAR, 1, cData->earth);
		//input = getInputFifo(fildes);
		command[1] = input; 
		if (input ==  ' ')
		{
			fread((void*) &input, CHAR, 1, cData->earth);
			//input = getInputFifo(fildes);
			command[2] = input; 
			//returns image capture frequency 
			if (input == 'I')
			{
				fread((void*) &input, CHAR, 1, cData->earth); 
				//input = getInputFifo(fildes);
				command[3] = input; 
				if (input == '\n') 
				{	
					sprintf(buffer, "M %d", cData->imageFrequency); 
					windowPrint(cData->window, buffer, 3, 54); 
				}
			}
			// returns transport distance
			else if (input == 'T') 
			{
				fread((void*) &input, CHAR, 1, cData->earth);
				//input = getInputFifo(fildes);
				command[3] = input; 
				if (input == '\n') 
				{
					sprintf(buffer, "M %d", *(cData->distances->tail));
					windowPrint(cData->window, buffer, 3, 54); 
				}
			}
		}	
	}
	else
	{
		*(cData->inputError) = TRUE; 
	}
	
	// error with command
	if (*(cData->inputError) == TRUE)
	{
		sprintf(buffer, "Invalid command: %s", command); 
		windowPrint(cData->window, buffer, 3, 4);  
		output = 'E';
		fwrite((void*) &output, CHAR, 1, cData->earth); 
	} 
	else // no errors
	{
		// thruster command print
		if (command[0] == 'T') 
		{
			sprintf(buffer, "A %s %d", command, *(cData->thrusterCommand));
		}
		else // other commands
		{
			sprintf(buffer, "A %s", command); 
		}
		windowPrint(cData->window, buffer, 5, 4); 
	}
	windowPrint(cData->window, "$", 9, 2);  
}*/
