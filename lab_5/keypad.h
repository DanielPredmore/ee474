#ifndef KEYPAD_H
#define KEYPAD_H

// functions for non-blocking input operations
// REMEMBER to provide a way to exit before implementing!
void setNonblockTerminalMode();
int inputPending();
int inputPendingFifo(int fildes);
int getInput();
int getInputFifo(int fildes);
void setEchoOff();

#endif
