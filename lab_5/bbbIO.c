#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <time.h>
#include "bbbIO.h"

// GPIO HEADER ADDRESSES
#define GPIO0_ADDR 0x44e07000
#define GPIO0_END 0x44e08000
#define GPIO1_ADDR 0x4804c000
#define GPIO1_END   0x4804d000
#define GPIO2_ADDR 0x481ac000
#define GPIO2_END   0x481ad000
#define GPIO3_ADDR 0x481ae000
#define GPIO3_END   0x481af000
#define GPIO0_SIZE (GPIO0_END - GPIO0_ADDR)
#define GPIO1_SIZE (GPIO1_END - GPIO1_ADDR)

#define GPIO2_SIZE (GPIO2_END - GPIO2_ADDR)
#define GPIO3_SIZE (GPIO3_END - GPIO3_ADDR)

// GPIO OFFSETS
#define GPIO_REVISION 0x0
#define GPIO_SYSCONFIG 0x10
// end of interrupt register
#define GPIO_EOI 0x20
#define GPIO_IRQSTAT_RAW0 0x24
#define GPIO_IRQSTAT_RAW1 0x28
#define GPIO_IRQSTAT0 0x2c
#define GPIO_IRQSTAT1 0x30
#define GPIO_IRQSTAT_SET0 0x34
#define GPIO_IRQSTAT_SET1 0x38
#define GPIO_IRQSTAT_CLR0 0x3c
#define GPIO_IRQSTAT_CLR1 0x40
#define GPIO_SYSSTAT 0x114
#define GPIO_CTRL 0x130
// output enable register

#define GPIO_OE 0x134
#define GPIO_DATAIN 0x138 
#define GPIO_DATAOUT 0x13c
#define GPIO_LVLDETECT0 0x140
#define GPIO_LVLDETECT1 0x144
#define GPIO_RISEDETECT 0x148
#define GPIO_FALLDETECT 0x14c
#define GPIO_DEBOUNCE_EN 0x150
#define GPIO_DEBOUNCE_TIME 0x154
#define GPIO_CLRDATAOUT 0x190
#define GPIO_SETDATAOUT 0x194

// ADC ADDRESS
#define ADC_ADDR 0x44e0d000
#define ADC_END 0x44e0f000
#define ADC_SIZE (ADC_END - ADC_ADDR)

// ADC OFFSETS
#define ADC_REVISION 0x0
#define ADC_SYSCONFIG 0x10
// end of interrupt
#define ADC_EOI 0x20
#define ADC_IRQSTAT_RAW 0x24
#define ADC_IRQSTAT 0x28
#define ADC_IRQSTAT_SET 0x2c
#define ADC_IRQSTAT_CLR 0x30
#define ADC_IRQWAKEUP 0x34
// DMA enable
#define ADC_DMAEN_SET 0x38
#define ADC_DMAEN_CLR 0x4c
#define ADC_CTRL 0x40
#define ADC_STAT 0x44
#define ADC_RANGE 0x48
#define ADC_CLKDIV 0x4c
#define ADC_MISC 0x50
// step enable, every 0.44mV is a step
#define ADC_STEPEN 0x54
#define ADC_IDLECONF 0x58
// for touch screen controller, not used
#define TSC_CHARGE_STEPCONF 0x5c
#define TSC_CHARGE_DELAY 0x60
// 16 step configs each one 0x8 apart
// same for step delays
#define ADC_STEPCONF1 0x64
#define ADC_STEPDELAY1 0x68
#define ADC_STEPOFFSET 0x08
#define ADC_FIFO0COUNT 0xe4
#define ADC_FIFO0THRESHOLD 0xe8
#define ADC_DMA0REQ 0xec
#define ADC_FIFO1COUNT 0xf0
#define ADC_FIFO1THRESHOLD 0xf4
#define ADC_DMA1REQ 0xf8
#define ADC_FIFO0DATA 0x100
#define ADC_FIFO1DATA 0x200

// PWM ADDRESSES
#define PWM_SS0_ADDR 0x48300000
#define PWM_SS0_END 0x48300260
#define PWM_SS1_ADDR 0x48302000
#define PWM_SS1_END 0x48302260
#define PWM_SS2_ADDR 0x48304000
#define PWM_SS2_END 0x48304260
#define PWM_SS0_SIZE (PWM_SS0_END - PWM_SS0_ADDR)
#define PWM_SS1_SIZE (PWM_SS1_END - PWM_SS1_ADDR)
#define PWM_SS2_SIZE (PWM_SS2_END - PWM_SS2_ADDR)

// PWM SUBMODULE OFFSETS
#define PWM_ECAP 0x100
#define PWM_EQEP 0x180
#define PWM_EPWM 0x200

// PWM SUBMODULE ADDRESSES
#define PWM_ECAP0 (PWM_SS0_ADDR + PWM_ECAP)
#define PWM_ECAP1 (PWM_SS1_ADDR + PWM_ECAP)
#define PWM_ECAP2 (PWM_SS2_ADDR + PWM_ECAP)
#define PWM_EQEP0 (PWM_SS0_ADDR + PWM_EQEP)
#define PWM_EQEP1 (PWM_SS1_ADDR + PWM_EQEP)
#define PWM_EQEP2 (PWM_SS2_ADDR + PWM_EQEP)
#define PWM_EPWM0 (PWM_SS0_ADDR + PWM_EPWM)
#define PWM_EPWM1 (PWM_SS1_ADDR + PWM_EPWM)
#define PWM_EPWM2 (PWM_SS2_ADDR + PWM_EPWM)

// PWM_SS OFFSETS
// PWM_REV is PWM_IDVER is AM335x Tech. Ref. Manual
#define PWM_REV 0x0
#define PWM_SYSCONFIG 0x4
#define PWM_CLKCONFIG 0x8
#define PWM_CLKSTATUS 0xc

// PWM EPWM OFFSETS
// A and B are the two pwm outputs
// TIME-BASE MODULE
// Time-base ctrl register
#define PWM_EPWM_TBCTL 0x0
// Time-base status register
#define PWM_EPWM_TBSTS 0x2
// Time-base EHRPWM extension phase register
#define PWM_EPWM_TBPHSHR 0x4
// time-base phase register
#define PWM_EPWM_TBPHS 0x6
// time-base counter register
#define PWM_EPWM_TBCNT 0x8
// time-base period register
#define PWM_EPWM_TBPRD 0xa
// Counter-Compare(CC) module
// CC ctrl register
#define PWM_EPWM_CMPCTL 0xe
// CC EHRPWM extension 'A' register
#define PWM_EPWM_CMPAHR 0x10
// CC 'A' regsiter
#define PWM_EPWM_CMPA 0x12
// CC 'B' register
#define PWM_EPWM_CMPB 0x14
// Action-Qualifier(AQ) module
// AQ control register for A
#define PWM_EPWM_AQCTLA 0x16
// AQ control register for B
#define PWM_EPWM_AQCTLB 0x18
// AQ software force register
#define PWM_EPWM_AQSFRC 0x1a
// AQ continuous software force
#define PWM_EPWM_AQCSFRC 0x1c
// Dead-Band(DB) generator module
// DB ctrl register
#define PWM_EPWM_DBCTL 0x1e
// DB rising edge delay count
#define PWM_EPWM_DBRED 0x20
// DB falling edge delay count
#define PWM_EPWM_DBFED 0x22
// PWM-Chopper(PC) module
// PC ctrl register
#define PWM_EPWM_PCCTL 0x3c
// Trip-zone(TZ) module, following order in documentation
// TZ select register
#define PWM_EPWM_TZSEL 0x24
// TZ ctrl register
#define PWM_EPWM_TZCTL 0x28
// TZ enable interrupt register
#define PWM_EPWM_TZEINT 0x2a
// TZ flag register
#define PWM_EPWM_TZFLG 0x2c
// TZ clear register
#define PWM_EPWM_TZCLR 0x2e
// TZ force register
#define PWM_EPWM_TZFRC 0x30
// Event Trigger(ET) module
// ET select register
#define PWM_EPWM_ETSEL 0x32
// ET pre-scale register
#define PWM_EPWM_ETPS 0x34
// ET flag register
#define PWM_EPWM_ETFLG 0x36
// ET clear register
#define PWM_EPWM_ETCLR 0x38
// ET force register
#define PWM_EPWM_ETFRC 0x3a
// enhanced high-res PWM(EHRPWM) module
// EHRPWM Configuration Register
#define PWM_EPWM_HRCNFG 0x1040

// PWM ECAP OFFSETS
// Time Stamp Counter for recording event captures
#define PWM_ECAP_TSCTR 0x0
// phase control for apwm output mode
#define PWM_ECAP_CTRPHS 0x4
// capture registers
#define PWM_ECAP_CAP1 0x8
#define PWM_ECAP_CAP2 0xc
#define PWM_ECAP_CAP3 0x10
#define PWM_ECAP_CAP4 0x14
// eCap ctrl registers
#define PWM_ECAP_ECCTL1 0x28
#define PWM_ECAP_ECCTL2 0x2a
#define PWM_ECAP_ECEINT 0x2c
// eCap Flag register
#define PWM_ECAP_ECFLG 0x2e
#define PWM_ECAP_ECCLR 0x30
#define PWM_ECAP_ECFRC 0x32
#define PWM_ECAP_REVID 0x5c

// CONTROL MODULE ADDRESS
#define CTRL_MOD_ADDR 0x44e10000
#define CTRL_MOD_END 0x44e12000
#define CTRL_MOD_SIZE (CTRL_MOD_END - CTRL_MOD_ADDR)

// CTRL MODULE OFFSETS
#define CTRL_REV 0x0
#define CTRL_HWINFO 0x4
#define CTRL_SYSCONFIG 0x10
#define CTRL_STATUS 0x40
// ONLY IMPORTANT REGISTER OFFSETS DEFINED
// 8 AIN from AIN 7 to 0, separated by offset
#define CTRL_CONF_AIN7 0xb10
#define CTRL_CONF_AIN_OFFSET 0x4
// to be set to mode4
#define CTRL_CONF_P8_13 0x824
#define CONF_P8_13_PWM_MODE 0x4
// to be set to mode4
#define CTRL_CONF_P8_19 0x820
#define CONF_P8_19_PWM_MODE 0x4
// to be set to mode6
#define CTRL_CONF_P9_14 0x848
#define CONF_P9_14_PWM_MODE 0x6
// to be set to mode6
#define CTRL_CONF_P9_16 0x84c
#define CONF_P9_16_PWM_MODE 0x6
// to be set to mode0
#define CTRL_CONF_P9_42 0x964
#define CONF_P9_42_ECAP_MODE 0x0

// pointers to addresses
volatile void * gpioAddr[4];
volatile void * adcAddr;
volatile void * pwmAddr[3];
volatile void * ctrlAddr;

// function prototypes
void releaseIO();
void setGPIODirection(unsigned int gpioNumber, unsigned int setAsInput);

#ifdef TEST_BBB_IO
int main(int argc, char * argv[])
{
	initIO();

	enablePWM();

	if(argc == 3)
	{
		char * end;
		setPWM(strtol(argv[1], &end, 10), strtol(argv[2], &end, 10), 0); 
	}

 	printf("UINT_MAX = %x; UCHAR_MAX = %x; USHORT_MAX = %x\n", UINT_MAX, UCHAR_MAX, USHRT_MAX);

	unsigned int * clkStatus = (unsigned int *)(pwmAddr[2] + PWM_CLKSTATUS);
	unsigned short * tbCtl = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_TBCTL);
	unsigned short * tbSts = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_TBSTS);
	unsigned short * tbPhsHr = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_TBPHSHR);
	unsigned short * tbPhs = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_TBPHS);
	unsigned short * tbCnt = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_TBCNT);
	unsigned short * tbPrd = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_TBPRD);

	printf("clkStatus: %#010x\n", *clkStatus);
	printf("tbCtl: %#06x\n", *tbCtl);	
	printf("tbSts: %#06x\n", *tbSts);	
	printf("tbPhsHr: %#06x\n", *tbPhsHr);	
	printf("tbPhs: %#06x\n", *tbPhs);	
	printf("tbCnt: %#06x\n", *tbCnt);	
	printf("tbPrd: %#06x\n", *tbPrd);	
	
	unsigned short * cmpCtl = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_CMPCTL);	
	unsigned short * cmpAHr = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_CMPAHR);	
	unsigned short * cmpA = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_CMPA);	
	unsigned short * cmpB = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_CMPB);

	printf("cmpCtl: %#06x\n", *cmpCtl);	
	printf("cmpAHr: %#06x\n", *cmpAHr);	
	printf("cmpA: %#06x\n", *cmpA);	
	printf("cmpB: %#06x\n", *cmpB);

	unsigned short * aqCtlA = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_AQCTLA);
	unsigned short * aqCtlB = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_AQCTLB);
	unsigned short * aqSFrc = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_AQSFRC);
	unsigned short * aqCSFrc = (unsigned short *)(pwmAddr[2] + PWM_EPWM + PWM_EPWM_AQCSFRC);

	printf("aqCtlA: 0x%04x\n", *aqCtlA);
	printf("aqCtlB: 0x%04x\n", *aqCtlB);
	printf("aqSFrc: 0x%04x\n", *aqSFrc);
	printf("aqCSFrc: 0x%04x\n", *aqCSFrc);

	clock_t prevTime = clock();
	clock_t nextTime;

	printf("%d\n", CLOCKS_PER_SEC/4);

	while(1)
	{
		unsigned int period, dutyCycle;
		unsigned int prevPeriod, prevDuty;	
		getPWM(&period, &dutyCycle);
		if(dutyCycle > 0)
			prevDuty = dutyCycle;
		if(period > 0)
			prevPeriod = period;

		nextTime = clock();

		if(nextTime - prevTime > CLOCKS_PER_SEC/4)
		{
			if(period == 0)
				printf("last period: %d\n", prevPeriod);
			else
				printf("period: %d\n", period);

			if(dutyCycle == 0)
				printf("last duty: %d\n", prevDuty);
			else
				printf("duty cycle: %d\n", dutyCycle);
			prevTime = nextTime;
		}
	}
	
	return 0;
}

#endif

void enablePWM()
{
	unsigned int * clkConf[3];
	unsigned int * clkStat[3];

	for(int i = 0; i < 3; i++)
	{
		clkConf[i] = (unsigned int *)(pwmAddr[i] + PWM_CLKCONFIG);
		clkStat[i] = (unsigned int *)(pwmAddr[i] + PWM_CLKSTATUS);
		// checks if all clocks are enabled
		if(!(*clkStat[i] & 0x111))
		{
			*clkConf[i] |= 0x111; // sets clkEn bits high
			*clkConf[i] &= ~(0x222); // sets clkStop bits low
		}
	}

	// enable settings for eCap
	unsigned short * eccCtl1 = (unsigned short *)(pwmAddr[0] + PWM_ECAP + PWM_ECAP_ECCTL1);	
	unsigned short * eccCtl2 = (unsigned short *)(pwmAddr[0] + PWM_ECAP + PWM_ECAP_ECCTL2);

	// configures eCap to capture both rising and falling edges 
	// to measure both duty cycle and period, 4 measures at a time
	// as well as enabling event capture mode
	*eccCtl2 &= (USHRT_MAX << 11);
	*eccCtl2 += 0xd7;
	*eccCtl1 = 0xc144;
	// enable settings for eCap
	eccCtl1 = (unsigned short *)(pwmAddr[2] + PWM_ECAP + PWM_ECAP_ECCTL1);	
	eccCtl2 = (unsigned short *)(pwmAddr[2] + PWM_ECAP + PWM_ECAP_ECCTL2);

	// configures eCap to capture both rising and falling edges 
	// to measure both duty cycle and period, 4 measures at a time
	// as well as enabling event capture mode
	*eccCtl2 &= (USHRT_MAX << 11);
	*eccCtl2 += 0xd7;
	*eccCtl1 = 0xc144;
}

/**
 * sets the values int the pointers passed as the currently measured period and dutyCycle
 * sets both values to 0 if no capture
 */
void getPWM(unsigned int *period, unsigned int *dutyCyclePercent, unsigned int channel)
{
	static unsigned int prevPeriod[3] = {0, 0, 0};
	static unsigned int prevDutyCycle[3] = {0, 0, 0};
	static clock_t prevTime[3] = {0, 0, 0};
	
	// register for flags raised by ecap module
	unsigned short * ecFlg = (unsigned short *)(pwmAddr[channel] + PWM_ECAP + PWM_ECAP_ECFLG);
	unsigned short * ecClr = (unsigned short *)(pwmAddr[channel] + PWM_ECAP + PWM_ECAP_ECCLR);
	clock_t nextTime = clock();

	// checks if 4 events captures have occurred by checking the CEVNT4 bit
	// slowest frequency capture is 20 Hz
	if((*ecFlg & (1 << 4)) == 0)
	{
		if(nextTime - prevTime[channel] < CLOCKS_PER_SEC / 10)
		{
			*period = prevPeriod[channel];
			*dutyCyclePercent = prevDutyCycle[channel];
		}
		else
		{
			prevPeriod[channel] = 0;
			prevDutyCycle[channel] = 0;
			*period = 0;
			*dutyCyclePercent = 0;
		}
	}
	else
	{
		unsigned short * eccCtl2 = (unsigned short *)(pwmAddr[channel] + PWM_ECAP + PWM_ECAP_ECCTL2);
		// addresses to the registers containing time stamps of capture events
		unsigned int * cap1 = (unsigned int *)(pwmAddr[channel] + PWM_ECAP + PWM_ECAP_CAP1);
		unsigned int * cap2 = (unsigned int *)(pwmAddr[channel] + PWM_ECAP + PWM_ECAP_CAP2);
		unsigned int * cap3 = (unsigned int *)(pwmAddr[channel] + PWM_ECAP + PWM_ECAP_CAP3);
		unsigned int * cap4 = (unsigned int *)(pwmAddr[channel] + PWM_ECAP + PWM_ECAP_CAP4);
		
		unsigned int period1, period2, duty1, duty2, T1, T2, T3;

		T1 = *cap2 - *cap1;
		T2 = *cap3 - *cap2;
		T3 = *cap4 - *cap3;

		period1 = T1 + T2;
		period2 = T2 + T3;

		*period = (period1 + period2) / 2;
		//*period = (T1 + T2 + T3) / 3;
		prevPeriod[channel] = *period;

		duty1 = T1;
		duty2 = T3;

		if(*period == 0)
		{
			*dutyCyclePercent = 0;
		}
		else
		{
			*dutyCyclePercent = (((duty1 + duty2) / 2) * 100) / *period;
		}
		prevDutyCycle[channel] = *dutyCyclePercent;

		// clears captevt4 flag
		*ecClr |= (1 << 4);
		// re-arms ecap i.e. sets re-arm bit high
		*eccCtl2 |= (1 << 3);

		prevTime[channel] = clock();
	}
}

void setPWM(unsigned int dutyCyclePercent, unsigned int periodInUsec, unsigned int channel)
{
	if(periodInUsec == 0 || dutyCyclePercent == 0)
	{
		periodInUsec = 0;
		dutyCyclePercent = 0;
	}
		
	unsigned int header = (5 - channel) / 2;
	unsigned int pin = (5 - channel) % 2;
	
	unsigned short * timeBaseCtrl = (unsigned short *)(pwmAddr[header] + PWM_EPWM + PWM_EPWM_TBCTL);
	unsigned short * timeBasePeriod = (unsigned short *)(pwmAddr[header] + PWM_EPWM + PWM_EPWM_TBPRD);

	// time base
	// sets PWM to free run and clk div of the high speed clk to /2
	// keeps clk div of pwm clk to /1
	// all other settings have been ignored
	*timeBaseCtrl = 0xc080; 
	// sets period, tbClk iterates at 100MHz
	unsigned long periodInClks = periodInUsec * 50;
	unsigned int xTraClkDiv = 0;
	while (periodInClks > USHRT_MAX)
	{
		periodInClks /= 2;
		xTraClkDiv++;
	}
	*timeBasePeriod = (unsigned short)(periodInClks);
	if(xTraClkDiv < 7 && xTraClkDiv > 0)
	{
		*timeBaseCtrl += (xTraClkDiv << 10);
	}
	
	unsigned short * cmpCtrl = (unsigned short *)(pwmAddr[header] + PWM_EPWM + PWM_EPWM_CMPCTL);
	unsigned short * cmpCnt;
	if(pin)
		cmpCnt = (unsigned short *)(pwmAddr[header] + PWM_EPWM + PWM_EPWM_CMPB);
	else
		cmpCnt = (unsigned short *)(pwmAddr[header] + PWM_EPWM + PWM_EPWM_CMPA);

	unsigned short dutyCycle =(unsigned short)((periodInClks * dutyCyclePercent)/ 100); 

	*cmpCnt = dutyCycle;

	// action qualifier
	unsigned short * aqCtrl;
	if(pin)
		aqCtrl = (unsigned short *)(pwmAddr[header] + PWM_EPWM + PWM_EPWM_AQCTLB);
	else
		aqCtrl = (unsigned short *)(pwmAddr[header] + PWM_EPWM + PWM_EPWM_AQCTLA);
	
	*aqCtrl &= (USHRT_MAX << 12);
	if(pin)
		*aqCtrl += 0x10a;
	else
		*aqCtrl += 0x1a;
}

void enableGPIO()
{
	unsigned int * gpioCtrl[3];
	gpioCtrl[0] = (unsigned int * )(gpioAddr[0] + GPIO_CTRL);
	gpioCtrl[1] = (unsigned int * )(gpioAddr[1] + GPIO_CTRL);
	gpioCtrl[2] = (unsigned int * )(gpioAddr[2] + GPIO_CTRL);
	unsigned int * gpioOE[3];
	gpioOE[0] = (unsigned int * )(gpioAddr[0] + GPIO_OE);
	gpioOE[1] = (unsigned int * )(gpioAddr[1] + GPIO_OE);
	gpioOE[2] = (unsigned int * )(gpioAddr[2] + GPIO_OE);
	
	for(int i = 0; i < 3; i++)
	{
		if((*gpioCtrl[i] & 0x3) != 0x1)
		{
			*gpioCtrl[i] &= ~0x3;
			*gpioCtrl[i] += 0x1;
		}
	}

	// all the gpio pin #'s we are using
	setGPIODirection(27, 1);
	setGPIODirection(47, 0);
	setGPIODirection(67, 0);
	setGPIODirection(45, 1);
	
	setGPIODirection(68, 1);
	setGPIODirection(44, 1);
	setGPIODirection(26, 0);
	setGPIODirection(46, 0);
	setGPIODirection(65, 0);
}

void setGPIODirection(unsigned int channel, unsigned int setAsInput)
{
	unsigned int * gpioOE;

	unsigned int header = channel / 32;
	unsigned int pin = channel % 32;

	gpioOE = (unsigned int *)(gpioAddr[header] + GPIO_OE);

	if(!setAsInput)
	{
		*gpioOE &= ~(1 << pin); 
	}
	else
	{
		*gpioOE |= (1 << pin); 
	}
}

void setGPIOValue(unsigned int channel, unsigned int value)
{
	unsigned int * gpioSetDataOut;
	unsigned int * gpioClearDataOut;

	unsigned int header = channel / 32;
	unsigned int pin = channel % 32;
/*
	gpioSetDataOut = (unsigned int *)(gpioAddr[header] + GPIO_SETDATAOUT);
	gpioClearDataOut = (unsigned int *)(gpioAddr[header] + GPIO_CLRDATAOUT);
	
	if(value)
	{
		*gpioSetDataOut |= (1 << pin); 
	}
	else
	{
		*gpioClearDataOut |= (1 << pin); 
	}
*/
	unsigned int * gpioDataOut = (unsigned int *)(gpioAddr[header] + GPIO_DATAOUT);
	
	if(value)
	{
		*gpioDataOut |= (1 << pin);
	}	
	else
	{
		*gpioDataOut &= ~(1 << pin);
	}
}

unsigned int getGPIOValue(unsigned int channel)
{
	unsigned int * gpioDataIn;

	unsigned int header = channel / 32;
	unsigned int pin = channel % 32;

	gpioDataIn = (unsigned int *)(gpioAddr[header] + GPIO_DATAIN);
	if(*gpioDataIn & (1 << pin))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void enableAIN()
{	
	// adc code
	// configuring adc to run a channel
	unsigned int * adcCtrl = (unsigned int *)(adcAddr + ADC_CTRL);
	//printf("0x%x\n", *adcCtrl);
	*adcCtrl &= (UINT_MAX << 1); // disabling adc to configure steps
	
	//printf("0x%x\n", *adcCtrl);
	unsigned int * adcStepEn = (unsigned int *)(adcAddr + ADC_STEPEN);
	//printf("0x%x\n", *adcStepEn);
	*adcStepEn &= ((UINT_MAX << 17) + 0x1); 
	*adcStepEn += 0x1fffe; // only enables all steps
	//printf("0x%x\n", *adcStepEn);

	// idle config
	unsigned int * adcIdleConfig = (unsigned int *)(adcAddr + ADC_IDLECONF);
	//printf("0x%x\n", *adcIdleConfig);
	*adcIdleConfig &= ((UINT_MAX << 26) + (0x1f)); // set all configurable bits to zero
	*adcIdleConfig += 0x78000; // add general configuration
	*adcIdleConfig |= (1 << 5);
	*adcIdleConfig += (0x000 << 19); // add channel configuration (channel << 19), for AIN0
	//printf("0x%x\n", *adcIdleConfig);
	// step config
	for(int i = 0; i < 16; i++)
	{
		unsigned int * adcStepConfig = (unsigned int *)(adcAddr + ADC_STEPCONF1 + (i * ADC_STEPOFFSET));

		*adcStepConfig &= (UINT_MAX << 28); // set all configurable bits to zero

		switch(i%4)
		{
			case 0: // steps for AIN0
			*adcStepConfig += 0x78011; // add general configuration, continuous sampling with average the last 16 values
			*adcStepConfig |= (1 << 5);
			*adcStepConfig += (0x0 << 19); // add channel configuration (channel << 19), for AIN0
				break;

			case 1: // steps for AIN1
			*adcStepConfig += 0x4078011; // uses fifo1
			*adcStepConfig += (0x1 << 19);
			*adcStepConfig |= (1 << 9);
				break;

			case 2: // steps for AIN2
			*adcStepConfig += 0x4078011; // uses fifo1
			*adcStepConfig += (0x2 << 19);
			*adcStepConfig |= (1 << 7);
				break;

			case 3: // steps for AIN3
			*adcStepConfig += 0x78011;
			*adcStepConfig |= (1 << 8);
			*adcStepConfig += (0x3 << 19);
				break;
		}	


		/*
		if(i%2) // if even, step measures battery level
		{
			printf("0x%x\n", *adcStepConfig);		
			*adcStepConfig &= (UINT_MAX << 28); // set all configurable bits to zero
			*adcStepConfig += 0x78011; // add general configuration, continuous sampling with average the last 16 values
			*adcStepConfig |= (1 << 5);
			*adcStepConfig += (0x0 << 19); // add channel configuration (channel << 19), for AIN0
			printf("0x%x\n", *adcStepConfig);		
		}
		else
		{
			printf("0x%x\n", *adcStepConfig);
			*adcStepConfig &= (UINT_MAX << 28);
			*adcStepConfig += 0x4078011; // uses fifo1
			if((i/2)%2)
			{
				*adcStepConfig += (0x2 << 19); 
				*adcStepConfig |= (1 << 7);
			}
			else
			{
				*adcStepConfig += (0x1 << 19);
				*adcStepConfig |= (1 << 9);
			}
			printf("0x%x\n", *adcStepConfig);		
		}
		*/
	}

	*adcCtrl += 0x1; // enabling the adc module with steps configured
}


/*
// global vars
unsigned int batteryLvl;
unsigned int batteryTemp1;
unsigned int batteryTemp2;
unsigned int ch0M, ch1M, ch2M;
*/
void readAIN()
{	
	static unsigned int prevfifo0Cnt = -1;
	static unsigned int prevfifo1Cnt = -1;
	
	unsigned int * adcFifo0Data = (unsigned int *)(adcAddr + ADC_FIFO0DATA);
	unsigned int * adcFifo0Cnt = (unsigned int *)(adcAddr + ADC_FIFO0COUNT);
	unsigned int * adcFifo1Data = (unsigned int *)(adcAddr + ADC_FIFO1DATA);
	unsigned int * adcFifo1Cnt = (unsigned int *)(adcAddr + ADC_FIFO1COUNT);
	
	unsigned int fifo0Cnt = (*adcFifo0Cnt & 0x3f);
	unsigned int fifo1Cnt = (*adcFifo1Cnt & 0x3f);

	if(prevfifo0Cnt != fifo0Cnt)
	{
		unsigned int data = *adcFifo0Data;
		unsigned int stepID = (~((UINT_MAX << 20) + 0xffff) & data) >> 16;
		unsigned int value = (~(UINT_MAX << 12) & data);

		if(stepID % 4 == 3)
		{
			proximity = value;
			ch0M = 1;
		}
		else
		{
			batteryLvl = value;
			ch3M = 1;
		}
/*
		batteryLvl = value;
		ch0M = 1;
		prevfifo0Cnt = fifo0Cnt;
		*/
	}
	if(prevfifo1Cnt != fifo1Cnt)
	{
		unsigned int data = *adcFifo1Data;
		unsigned int stepID = (~((UINT_MAX << 20) + 0xffff) & data) >> 16;
		unsigned int value = (~(UINT_MAX << 12) & data);

		if(stepID % 4 == 1)
		{
			batteryTemp2 = value;
			ch2M = 1;	
		}
		else
		{
			batteryTemp1 = value;
			ch1M = 1;
		}

		/*
		if((stepID/2)%2)
		{
			batteryTemp2 = value;
			ch2M = 1;
		}
		else
		{
			batteryTemp1 = value;
			ch1M = 1;
		}
*/
		prevfifo1Cnt = fifo1Cnt;
	}

}


void initIO()
{
	// file descriptor for devmem, needed for mmap
	int fildes = open("/dev/mem", O_RDWR | O_SYNC);
	// mmap for base addresses
	ctrlAddr = mmap(NULL, CTRL_MOD_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, CTRL_MOD_ADDR);
	if(MAP_FAILED == ctrlAddr)
	{
		printf("ctrl mmap failed\n");
	}
	gpioAddr[0] = mmap(NULL, GPIO0_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, GPIO0_ADDR);
	if(MAP_FAILED == gpioAddr[0])
	{
		printf("gpio0 mmap failed\n");
	}
	gpioAddr[1] = mmap(NULL, GPIO1_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, GPIO1_ADDR);
	if(MAP_FAILED == gpioAddr[1])
	{
		printf("gpio1 mmap failed\n");
	}
	gpioAddr[2] = mmap(NULL, GPIO2_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, GPIO2_ADDR);
	if(MAP_FAILED == gpioAddr[2])
	{
		printf("gpio2 mmap failed\n");
	}
	gpioAddr[3] = mmap(NULL, GPIO3_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, GPIO3_ADDR);
	if(MAP_FAILED == gpioAddr[3])
	{
		printf("gpio3 mmap failed\n");
	}
	adcAddr = mmap(NULL, ADC_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, ADC_ADDR);
	if(MAP_FAILED == adcAddr)
	{
		printf("adc mmap failed\n");
	}
	pwmAddr[0] = mmap(NULL, PWM_SS0_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, PWM_SS0_ADDR);
	if(MAP_FAILED == ctrlAddr)
	{
		printf("pwm0 mmap failed\n");
	}
	pwmAddr[1] = mmap(NULL, PWM_SS1_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, PWM_SS1_ADDR);
	if(MAP_FAILED == ctrlAddr)
	{
		printf("pwm1 mmap failed\n");
	}
	pwmAddr[2] = mmap(NULL, PWM_SS2_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fildes, PWM_SS2_ADDR);
	if(MAP_FAILED == ctrlAddr)
	{
		printf("pwm2 mmap failed\n");
	}
	close(fildes);

	atexit(releaseIO);

	enableGPIO();
	enableAIN();
	enablePWM();
}

void releaseIO()
{
	munmap((void*)ctrlAddr, CTRL_MOD_SIZE);
	munmap((void*)gpioAddr[0], GPIO0_SIZE);
	munmap((void*)gpioAddr[1], GPIO1_SIZE);
	munmap((void*)gpioAddr[2], GPIO2_SIZE);
	munmap((void*)gpioAddr[3], GPIO3_SIZE);
	munmap((void*)adcAddr, ADC_SIZE);
	munmap((void*)pwmAddr[0], PWM_SS0_SIZE);
	munmap((void*)pwmAddr[1], PWM_SS1_SIZE);
	munmap((void*)pwmAddr[2], PWM_SS2_SIZE);
}
