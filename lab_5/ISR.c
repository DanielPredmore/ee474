#include <stdio.h>
#include "ISR.h"
#include "transportDistance.h"
#include "kernel.h"

extern bool transportSignal;
extern bool fullyDeployed;
extern bool fullyRetracted;
extern bool measureTemp;
extern bool pirateDetect;
extern bool startImageCapture;

extern bool transportSignalAck;
extern bool fullyDeployedAck;
extern bool fullyRetractedAck;
extern bool measureTempAck;
extern bool pirateDetectAck;
extern bool startImageAck;

extern TCB tdTCB;

extern signed int transportDistPid;
extern signed int batteryTempReadPid;
extern signed int solarPanelPid;
extern signed int batteryTempReadPid;
extern signed int imageCaptPid;
extern signed int pirateManPid;

// Interrupt Service Routine
void ISR()
{
	if(transportSignal && !transportSignalAck)
	{
		transportSignalAck = TRUE;
		wakeTask(transportDistPid);
		transportDistance(tdTCB.taskDataPtr);
	}
	if(measureTemp && !measureTempAck)
	{
		measureTempAck = TRUE;
		wakeTask(batteryTempReadPid);
	}
	if(fullyDeployed && !fullyDeployedAck)
	{
		fullyDeployedAck = TRUE;
		wakeTask(solarPanelPid);
	}
	if(fullyRetracted && !fullyRetractedAck)
	{
		fullyRetractedAck = TRUE;
		wakeTask(solarPanelPid);
	}
	if(overTemperature && solarPanelState)
	{
		solarPanelRetract = 1;
	}
	if(pirateDetected && !pirateDetectAck)
	{
		pirateDetectAck = TRUE;
		pirateDetected = FALSE;
		wakeTask(pirateManPid);
	}
	if(startImageCapture && !startImageAck)
	{
		startImageAck = TRUE;
		startImageCapture = FALSE;
		wakeTask(imageCaptPid);
	}
	if(commandRunning && !commandRunningAck)
	{
		commandRunningAck = TRUE;
		commandRunning = FALSE;
		wakeTask(commandPid);
	}
}
