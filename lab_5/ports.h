#ifndef PORTS_H
#define PORTS_H

#define EARTH_PORT "/dev/pts/0"
#define SATELLITE_PORT "/dev/pts/1"
#define V_PORT0 "/dev/pts/0"
#define V_PORT1 "/dev/pts/1"

#endif // PORTS_H
