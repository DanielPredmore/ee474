#ifndef SATELLITE_H
#define SATELLITE_H

/**
 * This header file contains the globally available variables.
 */

// defining default values
#define NO_COMMAND 0x0000
#define FULL 100
#define EMPTY 0
#define BATTERY_LOW 10
#define FUEL_LOW 10
#define MODE_PIN 27
#define TOTAL_INPUT_CHECKS 1
#define DATA_SIZE 8

// defining the TCB struct
struct _satelliteKernelTCB {
		void (*task)(void*);
		void* taskDataPtr;
		char *pName;
};
typedef struct _satelliteKernelTCB TCB;

// Defining a bool
enum boolean {
		FALSE = 0,
		TRUE = 1
};
typedef enum boolean bool;

// defining circular array
struct circularArray
{
	double array[DATA_SIZE]; 
	double * head; 
	double * tail; 
};
typedef struct circularArray circularArray; 

// Thruster Control
unsigned int thrusterCommand;


// Power Management
unsigned int * batteryLevelPtr;
unsigned int * batteryTempPtr;
// unsigned short batteryLevel;
unsigned short fuelLevel;
unsigned short powerConsumption;
unsigned short powerGeneration;

// Solar Panel Control
bool solarPanelState;
bool solarPanelDeploy; 
bool solarPanelRetract;
bool overTemperature;
bool driveMotorSpeedInc; 
bool driveMotorSpeedDec;

// Warning Alarm
bool fuelLow;
bool batteryLow;
bool overTemp;

// Vehicle Communication
char command; 
char response; 

// Display
unsigned int mode; 

// For key input
char currentInput;
unsigned short inputChecks;

// Transport Distance
circularArray distances;
unsigned int transportPeriod;
unsigned int transportDutyCycle;
TCB tdTCB; 

// Image Capture
double imageFrequency;

bool transportSignal;
bool fullyDeployed;
bool fullyRetracted;
bool measureTemp;

bool transportSignalAck;
bool fullyDeployedAck;
bool fullyRetractedAck;
bool measureTempAck;

signed int transportDistPid;
signed int batteryTempReadPid;
signed int driveMotorPid;
signed int solarPanelPid;
signed int keypadPid;

unsigned int driveMotorSpeed;

double freqBuffer[16];

void initializeGlobals();

#endif // SATELLITE_H
