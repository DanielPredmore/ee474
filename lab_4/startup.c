#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "startup.h"
#include "kernel.h"
#include "consoleDisplay.h"
#include "powerSubsystem.h"
#include "satelliteComms.h"
#include "vehicleComms.h"
#include "thrusterSubsystem.h"
#include "warningAlarm.h"
#include "display.h"
#include "timeWindow.h"
#include "solarPanelControl.h"
#include "bbbIO.h"
#include "consoleKeypad.h"
#include "kernelStat.h"
#include "dummy.h"
#include "majorCycle.h"
#include "transportDistance.h"
#include "satellite.h"

#define MAJOR_TIME (5 * CLOCKS_PER_SEC)
#define TIME_UPDATE_TIME (CLOCKS_PER_SEC/10)
#define STAT_TIME (1*CLOCKS_PER_SEC)
#define DISPLAY_TIME (50*CLOCKS_PER_SEC/1000)
#define MINOR_TIME 10000
#define N_RATE (CLOCKS_PER_SEC/7500-14)
#define VCOM_TIME (0.75*CLOCKS_PER_SEC)

extern signed int transportDistPid;
extern signed int batteryTempReadPid;
extern void * vComsWindow2;
extern TCB tdTCB; 

struct startup_struct
{
	unsigned int * channel0;
	unsigned int * channel1; 
	display_t * satelliteDisplay;
	display_t * earthDisplay;
};
typedef struct startup_struct startData;

void startup(void *data);

TCB getStartup(unsigned int * channel0, unsigned int * channel1,  display_t *satelliteDisplay, display_t *earthDisplay)
{
	startData *data = (startData *)malloc(sizeof(startData));
	data->channel0 = channel0;
	data->channel1 = channel1; 
	data->satelliteDisplay = satelliteDisplay;
	data->earthDisplay = earthDisplay;
	TCB tcb;
	tcb.taskDataPtr = data;
	tcb.task = startup;
	tcb.pName = "Startup";
	return tcb;
}

void startup(void *data)
{
	startData * sData = (startData *)data;
	initializeGlobals();

	display_t *satelliteDisplay = sData->satelliteDisplay;
	display_t *earthDisplay = sData->earthDisplay;

	window_t *conWindow = newWindow(satelliteDisplay, TRUE, 3, 1, 15, 128);
	window_t *sComsWindow = newWindow(earthDisplay, TRUE, 3, 1, 15, 128);
	window_t *vComsWindow = newWindow(earthDisplay, TRUE, 19, 1, 7, 128); 
	window_t *temp1 = newWindow(satelliteDisplay, TRUE, 1 ,1, 1, 128);
	windowPrintColor(temp1, "Satellite Terminal", GREEN, 0, 1);
	window_t *temp2 = newWindow(earthDisplay, TRUE, 1, 1, 1, 128);
	windowPrintColor(temp2, "Earth Terminal", CYAN, 0, 1);
    window_t *vComsWindow2 = newWindow(satelliteDisplay, TRUE, 19, 1, 7, 128); 
	window_t *statWindow = newWindow(satelliteDisplay, TRUE, 27, 1, 50, 128);

	addTask(getWarningAlarm(), 3, MINOR_TIME);
	addTask(getVehicleComms(vComsWindow, vComsWindow2, sData->channel0, sData->channel1), 3, VCOM_TIME); 
	addTask(getMajorCycle(sComsWindow, vComsWindow, vComsWindow2, sData->channel0, sData->channel1), 3, MAJOR_TIME);
	addTask(getIOTask(), 3, MINOR_TIME);
	addTask(getTimeWindow(temp1, GREEN, 103), 3, TIME_UPDATE_TIME);
	addTask(getTimeWindow(temp2, CYAN, 103), 3, TIME_UPDATE_TIME);
	addTask(getKernelStat(statWindow), 3, STAT_TIME);
	addTask(getConsoleDisplay(conWindow), 1, DISPLAY_TIME);
	solarPanelPid = addTask(getSolarPanelControl(), 3, MINOR_TIME);
	driveMotorPid = addTask(getDriveMotorTask(), 3, MINOR_TIME);
	sleepOther(solarPanelPid);
	keypadPid = addTask(getConsoleKeypad(), 3, MINOR_TIME);
	sleepOther(keypadPid);
	tdTCB = getTransportDistance(vComsWindow2); 
	transportDistPid = addTask(tdTCB, 3, MINOR_TIME);
	batteryTempReadPid = addTask(getBatteryTempRead(), 3, MINOR_TIME);
	sleepOther(transportDistPid);
	endTask();
	free(data);
}
