#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char ** argv)
{
	if (argc != 1)
			return 1;
	
	char file[50] = "/dev/pts/ptmx";
	strncat(file, argv[0], 49);
	printf(file);

	FILE * port = fopen(file, "r");

	char c;

	do
	{
		c = fgetc(port);
		printf("%c", c);
	}
	while ( c != EOF);

	return 0;
}

