/**
 * This is the main c file for the satellite.
 * It is responsible for initializing the global values 
 * and launching the kernel.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include "kernel.h"
#include "startup.h"
#include "miningVehicle.h"
#include "transportVehicle.h"
#include "keypad.h"
#include "display.h"
#include "bbbIO.h"
#include "block.h"
#include "ISR.h"

#define NUM_THREADS 2

void main(int argc, char **argv) 
{
	signal(SIGUSR1, ISR);
	
	sem_init(&tran, 0, 1);
	sem_init(&mine, 0, 1);

	sem_wait(&tran);
	sem_wait(&mine);
	
	initKernel();
	setNonblockTerminalMode();
	int miningCh[3];
	int transportCh[3];
	
	int errMining;
	int errTransport;
	
	miningCh[0] = open("/dev/pts/ptmx", O_RDWR | O_NONBLOCK);
	miningCh[1] = open("/dev/pts/ptmx", O_RDWR | O_NONBLOCK); 	
	miningCh[2] = 0; // quit boolean for mining vehicle
	
	transportCh[0] = open("/dev/pts/ptmx", O_RDWR | O_NONBLOCK);
	transportCh[1] = open("/dev/pts/ptmx", O_RDWR | O_NONBLOCK); 	
	transportCh[2] = 0; // quit boolean for transport vehicle

	if (pipe(miningCh) == -1) 
	{
		perror("MINING PIPE FAILED"); 
		exit(1); 
	}

	if (pipe(transportCh) == -1) 
	{
		perror("TRANSPORT PIPE FAILED");
		exit(1);
	}


	//FILE *sat = fopen("/dev/pts/1", "w");
	//FILE *earth = stdout;

	FILE * sat = stdout;
	FILE * earth = fopen("/dev/pts/2", "w");

	// create threads 
	pthread_t thread[NUM_THREADS]; 
	errMining = pthread_create(&thread[0], NULL, (void *) miningVehicle, (void *) miningCh);
	errTransport = pthread_create(&thread[1], NULL, (void *) transportVehicle, (void *) transportCh); 

	if (errMining) 
	{
		perror("MINING VEHICLE FAILED"); 
		exit(1);
	}

	if (errTransport) 
	{
		perror("TRANSPORT VEHICLE FAILED"); 
		exit(1);
	}

	display_t *satelliteDisplay = newDisplay(sat, 80, 130);
	display_t *earthDisplay = newDisplay(earth, 80, 130);

	addTask(getStartup(miningCh, transportCh, satelliteDisplay, earthDisplay), 1, 0);
	kernelStart();
	sem_post(&tran);
	sem_post(&mine);
	miningCh[2] = 1;
    transportCh[2] = 1;    

	sem_destroy(&tran);
	sem_destroy(&mine);

	freeDisplay(satelliteDisplay);
	freeDisplay(earthDisplay);

	// terminate threads
	pthread_join(thread[0], NULL);
    pthread_join(thread[1], NULL);	

	pthread_exit(NULL); 
}
