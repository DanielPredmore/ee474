#include <stdio.h>
#include "dummy.h"
#include "kernel.h"

void dummy(void *data);

TCB getDummy()
{
	TCB tcb;
	tcb.taskDataPtr = NULL;
	tcb.task = dummy;
	tcb.pName = "Dummy";
	return tcb;
}

void dummy(void *data)
{
}
