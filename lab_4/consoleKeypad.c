#include <stdio.h>
#include "consoleKeypad.h"
#include "keypad.h"
#include "kernel.h"
#include <stdlib.h>

// global variables defined elsewhere
extern bool solarPanelDeploy;
extern bool solarPanelRetract;
extern bool driveMotorSpeedInc;
extern bool driveMotorSpeedDec;

// data struct for console keypad
struct consoleKeypadData
{
	bool * solarPanelDeploy;
	bool * solarPanelRetract;
	bool * driveMotorSpeedInc;
	bool * driveMotorSpeedDec;
};
typedef struct consoleKeypadData dataStruct;

// private function prototypes
void consoleKeypad(void * data);
/**
 * This function makes a new TCB task for the kernel
 *
 * @return the TCB for the consoleKeypad
 */
TCB getConsoleKeypad()
{
	TCB theTCB;
	dataStruct * data = (dataStruct*)malloc(sizeof(dataStruct));

	// initialization of data struct's datai
	data->solarPanelDeploy = &solarPanelDeploy;
	data->solarPanelRetract = &solarPanelRetract;
	data->driveMotorSpeedInc = &driveMotorSpeedInc;
	data->driveMotorSpeedDec = &driveMotorSpeedDec;

	// setting the values in the TCB
	theTCB.task = &consoleKeypad;
	theTCB.taskDataPtr = data;
	theTCB.pName = "Console Keypad";

	return theTCB;
}
/**
 * This is the task function for the console keypad task of the satellite system
 *
 * @param data: pointer to the consoleKeypad's data struct
 */
void consoleKeypad(void * data)
{
	dataStruct * reData = (dataStruct *)data; 
	if(!(*(reData->solarPanelDeploy) | *(reData->solarPanelRetract)))
	{
		sleepTask();
		return;
	}
	
	if(',' == currentInput)
	{
		*(reData->driveMotorSpeedDec) = TRUE;
		currentInput = '\0';
		//printf("++Speed\r");
	}
	else if('.' == currentInput)
	{
		*(reData->driveMotorSpeedInc) = TRUE;
		currentInput = '\0';
		//printf("--Speed\r");
	}
	inputChecks++;
}
