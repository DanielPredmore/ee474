#ifndef TIME_WINDOW_H
#define TIME_WINDOW_H

#include "satellite.h"
#include "kernel.h"
#include "display.h"

TCB getTimeWindow(window_t *window, color_t color, int offset);

#endif // TIME_WINDOW_H
