/**
 * This is the kernel for the satellite
 */

#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include "kernel.h"
#include "root.h"
#include "satellite.h"
#include "consoleDisplay.h"
#include "powerSubsystem.h"
#include "satelliteComms.h"
#include "thrusterSubsystem.h"
#include "warningAlarm.h"
#include "bbbIO.h"

struct kernelStruct;

/**
 * This is the struct the kernel uses to make the task queue
 * as a linked list
 */
struct kernelStruct {
	TCB tcb; // the task data
	clock_t runlast; // the time it was last run
	clock_t wait; // how long to wait between calls
	clock_t runTotal;
	clock_t runMax;
	clock_t totalWait;
	unsigned long calls;
	unsigned int priority;
	int pid;
	state_t state;
	struct kernelStruct * next; // the next task
	struct kernelStruct * last; // the last task run 
};
typedef struct kernelStruct task_t;

task_t taskBlocks[MAX_TASKS];
state_t taskStates[MAX_TASKS];

// A pointer to the head of the taskQueue
task_t * taskQueue;
task_t * nullQueue;

// function prototypes
void insert(task_t *task);
void run();
void doTask();

void initKernel()
{
	initIO();
	int i;
	for (i = 0; i < MAX_TASKS - 1; i++)
	{
		taskBlocks[i].next = &taskBlocks[i+1];
		taskBlocks[i].pid = i;
		taskBlocks[i].state = FREE;
		taskStates[i] = FREE;
	}
	taskBlocks[MAX_TASKS - 1].next = NULL;
	taskBlocks[MAX_TASKS - 1].pid = MAX_TASKS - 1;
	taskBlocks[MAX_TASKS - 1].state = FREE;
	taskStates[i] = FREE;
	nullQueue = taskBlocks;
}

/**
 * This function adds a task into the task queue.
 * It may be called by any task while the kernel is running.
 * However external functions may only call this function before the
 * kernel runs.
 *
 * @param tcb the tcb data of the task
 * @param wait the amount of time the kernel will wait before rerunning
 */
int addTask(TCB tcb, unsigned int priority, clock_t wait)
{
	if (nullQueue == NULL)
			return -1;
	task_t *task = nullQueue;
	nullQueue = nullQueue->next;
	task->tcb = tcb;
	task->runlast = clock();
	task->runTotal = 0;
	task->calls = 0;
	task->priority = priority;
	task->state = RUNNING;
	task->wait = wait;
	task->runMax = 0;
	task->totalWait = 0;

	if (NULL == taskQueue)
	{
		taskQueue = task;
		task->next = task;
		task->last = task;
	}
	else
	{
		insert(task);
	}
	taskStates[task->pid] = RUNNING;
	return task->pid;
}

/**
 * This function may be called by a task to remove itself form the
 * task queue.
 */
void endTask()
{
	task_t *temp = taskQueue;
	
	if (temp->next == temp)
	{
		taskQueue = NULL;
	}
	else
	{
		temp->last->next = temp->next;
		temp->next->last = temp->last;
		taskQueue = temp->last;
	}

	temp->next = nullQueue;
	nullQueue = temp;
	temp->state = FREE;
	taskStates[temp->pid] = FREE;
}

void sleepTask()
{
	task_t *temp = taskQueue;

	if (temp->next == temp)
	{
		taskQueue = NULL;
	}
	else
	{
		temp->last->next = temp->next;
		temp->next->last = temp->last;
		taskQueue = temp->last;
	}

	temp->state = WAITING;
	taskStates[temp->pid] = WAITING;
}

void wakeTask(int pid)
{
	if (pid >= MAX_TASKS || pid < 0)
			return;

	task_t *task = &taskBlocks[pid];
	
	if (task->state != WAITING)
			return;

	task->runlast = clock();
	insert(task);
}

void insert(task_t *task)
{
	task_t *current;
	for (current = taskQueue->next;
				current != taskQueue && task->priority >= current->priority;
				current = current->next);
	task_t * temp = current->next;
	current->next = task;
	task->next = temp;
	task->last = current;
	temp->last = task;
	task->state = RUNNING;
	taskStates[task->pid] = RUNNING;
}

/**
 * This function is responsible for starting the kernel
 */
void kernelStart() {
	run();
}

/**
 * This the main kernel forever loop
 */
void run() {
	while (taskQueue != NULL)
	{
		taskQueue = taskQueue->next;
		doTask();
		if('q' == currentInput)
		{
			return;	
		}
	}
}

/**
 * This runs a task from the task queue
 */
void doTask()
{
	time_t now = clock();
	time_t wait = now - taskQueue->runlast;
	if (taskQueue->wait <= wait)
	{
		taskQueue->totalWait += wait - taskQueue->wait;
		clock_t stop, runtime;
		TCB *tcb = &taskQueue->tcb;
		task_t *task = taskQueue;
		task->runlast = clock();
		setGPIOValue(67, TRUE);
		tcb->task(tcb->taskDataPtr);
		setGPIOValue(67, FALSE);
		stop = clock();
		runtime = stop - task->runlast;
		task->runTotal += runtime;
		if (runtime > task->runMax)
				task->runMax = runtime;
		task->calls++;
	}
}

state_t * getStateList()
{
	return taskStates;
}

void killTask(int pid)
{
	if (pid >= MAX_TASKS || pid < 0)
			return;
	task_t *task = &taskBlocks[pid];
	if (task->state == FREE)
			return;

	if (task->state == RUNNING)
	{
		if (task->next == task)
			{
				taskQueue = NULL;
			}
		else
			{
				task->next->last = task->last;
				task->last->next = task->next;
			}
	}

	if (task == taskQueue)
			taskQueue = task->last;

	task->state = FREE;
	taskStates[task->pid] = FREE;
	task->next = nullQueue;
	nullQueue = task;
}

void sleepOther(int pid)
{

	if (pid >= MAX_TASKS || pid < 0)
			return;
	task_t *task = &taskBlocks[pid];
	if (task->state != RUNNING)
			return;

	if (task->next == task)
	{
		taskQueue = NULL;
	}
	else
	{
		task->next->last = task->last;
		task->last->next = task->next;
	}

	if (task == taskQueue)
			taskQueue = task->last;

	task->state = WAITING;
	taskStates[task->pid] = WAITING;
}

void getStats(taskStat *stat, int pid)
{
	task_t * task = &taskBlocks[pid];
	stat->pName = task->tcb.pName;
	stat->pid = task->pid;
	stat->runTotal = task->runTotal;
	stat->runMax = task->runMax;
	stat->calls = task->calls;
	stat->state = task->state;
	stat->priority = task->priority;
	stat->totalWait = task->totalWait;
}

void resetStat(int pid)
{
	task_t *task = &taskBlocks[pid];
	task->runTotal = 0;
	task->calls = 0;
	task->runMax = 0;
	task->totalWait = 0;
}

