#ifndef MAJOR_CYCLE_H
#define MAJOR_CYCLE_H

#include "satellite.h"
#include "display.h"

TCB getMajorCycle(window_t *earth, window_t *vehicle, window_t *vehicle2, unsigned int *channel0, unsigned int * channel1);

#endif // MAJOR_CYCLE_H
