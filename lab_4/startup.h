#ifndef STARTUP_H
#define STARTUP_H

#include "satellite.h"
#include "display.h"

TCB getStartup(unsigned int *channel0, unsigned int * channel1, display_t *satelliteDisplay, display_t *earthDisplay);

#endif // STARTUP_H
