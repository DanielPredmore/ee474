#include <stdio.h>
#include <stdlib.h>
#include "kernelStat.h"
#include "kernel.h"
#include "root.h"

#define STAT_ROW 1
#define HEADER_ROW 8
#define PID_START 1
#define NAME_START 7
#define NAME_BUFF_START 33
#define CPU_START 42
#define CALLS_START 34
#define TOTAL_START 51
#define AVG_START 63
#define MAX_START 76
#define STATE_START 87
#define PRIORITY_START 96
#define WAIT_TOTAL_START 102
#define WAIT_AVG_START 114

#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)

struct kernelStat_struct
{
	window_t *window;
};
typedef struct kernelStat_struct data_t;

void kernelStat(void *data);


TCB getKernelStat(window_t * window)
{
	data_t *data = (data_t *)malloc(sizeof(data_t));
	data->window = window;
	TCB tcb;
	tcb.taskDataPtr = data;
	tcb.task = kernelStat;
	tcb.pName = "Kernel Statistics";
	return tcb;
}

void kernelStat(void *data)
{
	data_t *dat = (data_t *)data;
	window_t *window = dat->window;
	windowClear(window);
	
	windowPrintColor(window, "PID", WHITE, HEADER_ROW, PID_START);
	windowPrintColor(window, "NAME", MAGENTA, HEADER_ROW, NAME_START);
	windowPrintColor(window, "CALLS", GREEN, HEADER_ROW, CALLS_START);
	windowPrintColor(window, "CPU %", BLUE, HEADER_ROW, CPU_START);
	windowPrintColor(window, "RUN-TOTAL", RED, HEADER_ROW, TOTAL_START);
	windowPrintColor(window, "RUN-AVG", CYAN, HEADER_ROW, AVG_START);
	windowPrintColor(window, "RUN-MAX", BLUE, HEADER_ROW, MAX_START);
	windowPrintColor(window, "STATE", YELLOW, HEADER_ROW, STATE_START);
	windowPrintColor(window, "PRI", GREEN, HEADER_ROW, PRIORITY_START);
	windowPrintColor(window, "WAIT-TOTAL", RED, HEADER_ROW, WAIT_TOTAL_START);
	windowPrintColor(window, "WAIT-AVG", CYAN, HEADER_ROW, WAIT_AVG_START);

	int i;
	int row = HEADER_ROW + 1;
	state_t *states = getStateList();
	char buff[50];
	clock_t total = 0;
	clock_t totalWait = 0;
	unsigned int calls = 0;

	for (i = 0; i < MAX_TASKS; i++)
	{
		if (states[i] != FREE)
		{
			taskStat stat;
			getStats(&stat, i);
			
			sprintf(buff, "%d", i);
			windowPrintColor(window, buff, WHITE, row, PID_START);

			windowPrintColor(window, stat.pName, MAGENTA, row, NAME_START);
			windowPrint(window, "                    ", row, NAME_BUFF_START);

			sprintf(buff, "%d", stat.calls);
			windowPrintColor(window, buff, GREEN, row, CALLS_START);

			sprintf(buff, "%.2Lf", ((long double) stat.runTotal * 100) / CLOCKS_PER_SEC);
			windowPrintColor(window, buff, BLUE, row, CPU_START);

			sprintf(buff, "%.3Lf", ((long double) stat.runTotal) / CLOCKS_PER_MS);
			windowPrintColor(window, buff, RED, row, TOTAL_START);

			sprintf(buff, "%Lf", ((long double) stat.runTotal) / CLOCKS_PER_MS / (long double) stat.calls);
			windowPrintColor(window, buff, CYAN, row, AVG_START);

			sprintf(buff, "%.3Lf", ((long double) stat.runMax) / CLOCKS_PER_MS);
			windowPrintColor(window, buff, BLUE, row, MAX_START);

			total += stat.runTotal;
			totalWait += stat.totalWait;
			calls += stat.calls;

			if(states[i] == RUNNING)
				windowPrintColor(window, "Running", YELLOW, row, STATE_START);
			else if(states[i] == WAITING)
				windowPrintColor(window, "Waiting", YELLOW, row, STATE_START);

			sprintf(buff, "%d", stat.priority);
			windowPrintColor(window, buff, GREEN, row, PRIORITY_START);
			
			sprintf(buff, "%.3Lf", ((long double) stat.totalWait) / CLOCKS_PER_MS);
			windowPrintColor(window, buff, RED, row, WAIT_TOTAL_START);

			sprintf(buff, "%Lf", ((long double) stat.totalWait) / CLOCKS_PER_MS / (long double) stat.calls);
			windowPrintColor(window, buff, CYAN, row, WAIT_AVG_START);

			resetStat(i);
			row++;
		}
	}
	
	windowPrint(window, "================================================================================================================================",
					HEADER_ROW - 1, 0);

	windowPrintColor(window, "CPU Stats:", MAGENTA, STAT_ROW, 3);
	windowPrintColor(window, "Task Usage:", GREEN, STAT_ROW, 25);
	windowPrintColor(window, "Kernel and System:", RED, STAT_ROW, 50);
	windowPrint(window, "[", STAT_ROW + 1, 2);
	windowPrint(window, "]", STAT_ROW + 1, 103);
	
	int percent = (total * 100) / CLOCKS_PER_SEC;
	long double use = ((long double) total * 100) / CLOCKS_PER_SEC;
	sprintf(buff, "%.2Lf%%", use);
	windowPrintColor(window, buff, GREEN, STAT_ROW,  37);
	sprintf(buff, "%.2lf%%", 100 - use);
	windowPrintColor(window, buff, RED, STAT_ROW, 69);
	
	for (i = 0; i < percent; i++)
	{
		windowPrintColor(window, "|", GREEN, STAT_ROW + 1, i + 3);
	}
	for (i = percent; i < 100; i++)
	{
		windowPrintColor(window, "|", RED, STAT_ROW + 1, i + 3);
	}

	color_t color = GREEN;
	long double wait = ((long double) totalWait) / calls;

	if (wait > 50)
		color = YELLOW;
	if (wait > 75)
		color = RED;

	windowPrintColor(window, "System Response:", MAGENTA, STAT_ROW + 3, 3);
	sprintf(buff, "Avg Wait: %.3Lf uS", wait);
	windowPrintColor(window, buff, color, STAT_ROW + 3, 25);
	sprintf(buff, "Total Calls: %d", calls);
	windowPrintColor(window, buff, CYAN, STAT_ROW + 3, 50);
	sprintf(buff, "Total Wait: %.3f mS", ((long double) totalWait) / CLOCKS_PER_MS);
	windowPrintColor(window, buff, BLUE, STAT_ROW + 3, 75);
	windowPrint(window, "[", STAT_ROW + 4, 2);
	windowPrint(window, "]", STAT_ROW + 4, 103);

	for (i = 0; i < wait && i < 100; i++)
	{
		windowPrintColor(window, "|", color, STAT_ROW + 4, i + 3);
	}
}


