#ifndef FREQUENCY_CAPTURE_H
#define FREQUENCY_CAPTURE_H

#include "bbbIO.h"
#include "satellite.h"

void freqEnable(bool enable);
unsigned short freqRead();
void freqReset();

#endif // FREQUENCY_CAPTURE_H
