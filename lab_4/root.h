#ifndef ROOT_H
#define ROOT_H

#include <time.h>

#define MAX_TASKS 50

enum state_enum
{
	RUNNING,
	WAITING,
	FREE
};
typedef enum state_enum state_t;

struct taskStat_struct
{
	char *pName;
	int pid;
	clock_t runTotal;
	clock_t runMax;
	clock_t totalWait;
	unsigned long calls;
	state_t state;
	unsigned int priority;
};
typedef struct taskStat_struct taskStat;

state_t * getStateList();
void killTask(int pid);
void getStats(taskStat *stat, int pid);
void resetStat(int pid);

#endif // ROOT_H
