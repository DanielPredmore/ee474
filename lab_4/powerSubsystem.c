#include "powerSubsystem.h"
#include "bbbIO.h"
#include "kernel.h"
#include "solarPanelControl.h"
#include <stdlib.h>

#define DEPLOY_PERCENT 10
#define RETRACT_PERCENT 95
#define POWER_CONS_LOW 5
#define POWER_CONS_HI 10
#define POWER_GEN_MAX 50
#define BATTERY_LVL_CHANNEL 0

// global variables 
extern bool solarPanelDeploy;
extern bool solarPanelRetract;
extern bool solarPanelState;
extern bool batteryLow;
extern unsigned int * batteryLevelPtr;
extern unsigned int * batteryTempPtr;
extern unsigned short powerConsumption;
extern unsigned short powerGeneration;

// data struct for the global power system variables
struct powerSubsystemData
{
	bool * solarPanelState;
	bool * solarPanelDeploy;
	bool * solarPanelRetract;
	bool * batteryLow;
	unsigned int * batteryLevelPtr;
	unsigned int * batteryTempPtr;
	unsigned short * powerConsumption;
	unsigned short * powerGeneration;
};
typedef struct powerSubsystemData dataStruct;

// private function prototype
void powerSubsystem(void * data);
void simulatePower(dataStruct * data, short powerConInc, short powerGenInc);

/**
 * This function makes a new TCB task for the kernel 
 *
 * @return the TCB for the powerSubsystem
 */
TCB getPowerSubsystem()
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));

	theTCB.pName = "Power Subsystem";

	// initialization of data struct's data
	data->solarPanelState = &solarPanelState;
	data->solarPanelDeploy = &solarPanelDeploy;
	data->solarPanelRetract = &solarPanelRetract;
	data->batteryLow = &batteryLow;
   	data->batteryLevelPtr = batteryLevelPtr;
	data->batteryTempPtr = batteryLevelPtr;
	data->powerConsumption = &powerConsumption;
	data->powerGeneration = &powerGeneration;
	
	// setting the values in the TCB
	theTCB.task = &powerSubsystem;
	theTCB.taskDataPtr = data;

	return theTCB;
}
/**
 * This function determines when the satellite generates and consumes power.
 *
 * @param data is the data struct for the given power subsystem
 */
void powerSubsystem(void * data)
{			
	// vars keeping track of the simulated power consumption
	static bool odd = FALSE;
	// casting the void * to a dataStruct * to access its contents
	dataStruct * pd = (dataStruct *) data;

	//*(pd->batteryLevelPtr) = readAIN(BATTERY_LVL_CHANNEL)*100/0xfff;

	if(odd) // case when odd is true
	{
		odd = FALSE;
		if(*(pd->batteryLevelPtr) < FULL/2)
			simulatePower(pd, -1, 1);
		else
			simulatePower(pd, 1, 0);
	}
	else // even case
	{
		odd = TRUE;
		simulatePower(pd, 2, 2);
	}

	if(*(pd->batteryLevelPtr) < BATTERY_LOW)
		*(pd->batteryLow) = TRUE;
	else
		*(pd->batteryLow) = FALSE;
	sleepTask();
}

/**
 * This function determine the rate in which the satellite consumes
 * or generates power. It control when the solar panels are deployed
 *
 * @param powerConInc is the value which the consumption rate increments or
 * decrements, powerGenInc is the value which the generation rate increments
 * or decrements. 
 */
void simulatePower(dataStruct * data, short powerConInc, short powerGenInc)
{
	static bool reverse = FALSE;
	static bool taskLaunched = FALSE;
	
	if(reverse)
	{// system decreases power consumption until it's below 5
		*(data->powerConsumption) -= powerConInc;
		if(*(data->powerConsumption) < POWER_CONS_LOW)
			reverse = FALSE;
	}
	else
	{// system increases power consumption until it's above 10
		*(data->powerConsumption) += powerConInc;
		if(*(data->powerConsumption) > POWER_CONS_HI)
			reverse = TRUE;
	}
	if(TRUE == *(data->solarPanelState))
	{
		if(*(data->powerGeneration) + powerGenInc >= POWER_GEN_MAX)
			*(data->powerGeneration) = POWER_GEN_MAX;
		else
			*(data->powerGeneration) += powerGenInc;

		// issue command to retract solar panel
		if(*(data->batteryLevelPtr) > RETRACT_PERCENT)
		{
			*(data->solarPanelRetract) = TRUE;
			if(FALSE == taskLaunched)
			{
				wakeTask(solarPanelPid);
				taskLaunched = TRUE;
			}
		}
	}
	else
	{
		*(data->powerGeneration) = 0;
		// issue command to deploy solar panel
		if(*(data->batteryLevelPtr) < DEPLOY_PERCENT && !overTemperature)
		{
			*(data->solarPanelDeploy) = TRUE;
			if(FALSE == taskLaunched)
			{
				wakeTask(solarPanelPid);
				taskLaunched = TRUE;
			}
		}
	}
	if(!(*(data->solarPanelDeploy) | *(data->solarPanelRetract)))
	{
		taskLaunched = FALSE;
	}
}
