#include <stdio.h>
#include <signal.h>
#include "bbbIO.h"
#include "keypad.h"
#include "kernel.h"

#define BUFFER_SIZE 16
#define AMP_SIGNAL_MAX 325.0
#define AIN_MAX 1800.0

extern bool measureTemp;
extern bool measureTempAck;

void shiftBuffer(unsigned int * buffer, unsigned int nValue);
void shiftBufferD(double * buffer, double nValue);
void ioTask(void * data);
void batteryTempRead(void * data);

TCB getIOTask()
{
  TCB theTCB;
  theTCB.pName = "I/O Task";
  theTCB.task = &ioTask;
  return theTCB;
}

TCB getBatteryTempRead()
{
	TCB theTCB;
	theTCB.pName = "Batt. Temp. Read";
	theTCB.task = &batteryTempRead;
	return theTCB;
}

void ioTask(void * data)
{
	static unsigned int btLBuffer[BUFFER_SIZE];
	
	// named for the pwm channel they're coming from ecap1 is not exposed on the board
	unsigned int period0, duty0, period2, duty2;

	mode = getGPIOValue(27);

	readAIN();
	*(batteryLevelPtr) = 100 * batteryLvl / 0xfff;
	shiftBuffer(btLBuffer, *(batteryLevelPtr));

	measureTemp = getGPIOValue(45);
	
	if(measureTemp && !measureTempAck)
		raise(SIGUSR1);

	// get data from the PWM capture channels
	getPWM(&period0, &duty0, 0);
	getPWM(&period2, &duty2, 2);

	if(period0 == 0)
	{
		imageFrequency = 0;
	}
	else
	{
		imageFrequency = 1000000000.0 / (period0 * 10.0);
	}
	shiftBufferD(freqBuffer, imageFrequency);
	
	if(period2 == 0)
	{
		transportSignal = FALSE;
		transportPeriod = 0;
		transportDutyCycle = 0;
	}
	else
	{	
		transportSignal = TRUE;
		transportPeriod = period2 * 10;
		transportDutyCycle = duty2;
		if(!transportSignalAck)
			raise(SIGUSR1);
	}	
	
	if(TOTAL_INPUT_CHECKS == inputChecks)
	{
		inputChecks = 0;
		currentInput = '\0';
	}
	if(inputPending())
	{
		currentInput = getInput();
	}
}

void batteryTempRead(void * data)
{
	static unsigned int btTBuffer[BUFFER_SIZE] = {33, 33, 33, 33, 33, 33, 33, 33,
			33, 33, 33, 33, 33, 33, 33, 33};
	
	unsigned int nbatteryTemp1 = (unsigned int) (32 * (batteryTemp1 * (AIN_MAX / 0xfff) / AIN_MAX) + 33);
	unsigned int nbatteryTemp2 = (unsigned int) (32 * (batteryTemp2 * (AIN_MAX / 0xfff) / AIN_MAX) + 33);

	if(((btTBuffer[BUFFER_SIZE - 1] * 12.0)/ 10.0 < (double)nbatteryTemp1)||
		((btTBuffer[BUFFER_SIZE - 1] * 12.0)/ 10.0 < (double)nbatteryTemp2))
	{
		overTemperature = TRUE;
		raise(SIGUSR1);
	}
	
	*(batteryTempPtr) = (nbatteryTemp1 + nbatteryTemp2) / 2;
	shiftBuffer(btTBuffer, *(batteryTempPtr));	

	if(!measureTemp)
	{
		measureTempAck = FALSE;
		sleepTask();
	}
}

void shiftBuffer(unsigned int * buffer, unsigned int nValue)
{
	for(int i = 0; i < BUFFER_SIZE - 1; i++)
	{
		buffer[i] = buffer[i + 1];
	}
	buffer[BUFFER_SIZE - 1] = nValue;
}


void shiftBufferD(double * buffer, double nValue)
{
	for(int i = 0; i < BUFFER_SIZE - 1; i++)
	{
		buffer[i] = buffer[i + 1];
	}
	buffer[BUFFER_SIZE - 1] = nValue;
}
