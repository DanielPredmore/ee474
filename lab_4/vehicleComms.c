#include <stdio.h>
#include "satellite.h"
#include <unistd.h>
#include "keypad.h"
#include "display.h"
#include "vehicleComms.h"
#include <stdlib.h>
#include <fcntl.h>
#include <semaphore.h>
#include "ports.h"
#include "kernel.h"
#include "block.h"

#define CHAR sizeof(char)

// global variables
extern char command;
extern char response; 
extern circularArray distances; 

// data struct for vehicle communication
struct vehicleCommsData 
{
	char * command;
	char prevCommand; 
	char * response;
	circularArray * distances;
	unsigned int * miningCh;
	unsigned int * transportCh;
	window_t * satWindow; 
	window_t * window; 
}; 

typedef struct vehicleCommsData dataStruct;

// function prototypes
void vehicleComms(void *data);

/**
 * This function creates a new TCB for the kernal
 *
 * @returns TCB for vehicleComms
 */

TCB getVehicleComms(window_t * window, window_t * window2, unsigned int * miningCh, unsigned int * transportCh) 
{
	TCB theTCB; 
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 

	// initilizaing data struct
	(data->command) = &command;
	(data->prevCommand) = '\0';
	(data->response) = &response;
	(data->miningCh) = miningCh;
	(data->distances) = &distances; 
	(data->transportCh) = transportCh; 	
	(data->window) = window;
	(data->satWindow) = window2; 

	theTCB.task = &vehicleComms;
	theTCB.taskDataPtr = data;
	theTCB.pName = "Vehicle Comms";

	return theTCB;
}

void vehicleComms(void * data) 
{
	char response[2] = {'\0', '\0'};
	char buffer[100];
	color_t color = RESET;
		
	dataStruct * commsData = (dataStruct *) data;
	
	sem_post(&mine);
	write(commsData->miningCh[1], commsData->command, CHAR);	

	// MINING VEHICLE COMMUNICATIONS
	
	// read response from mining vehicle
	if (read(commsData->miningCh[0], response, CHAR) > 0) 
	{
		commsData->response[0] =  response[0];
	} 
	else
	{
		commsData->response[0] = '\0';
	}
	sem_wait(&mine);
	
	// clears window
	windowClear(commsData->window);
	
	// Prints the commands sent
	windowPrintColor(commsData->window, "Mining Vehicle Communication:\0", MAGENTA, 1, 2);
	windowPrintColor(commsData->window, "Earth sent command = \0", BLUE, 3, 4);
	windowCharColor(commsData->window, *commsData->command, BLUE, 3, 26);

	// if receive response from vehicle 
	if (commsData->response[0] != '\0') 
	{
		sprintf(buffer, "Earth received the response: A %c\0", commsData->response[0]);
		windowPrintColor(commsData->window, buffer, GREEN, 5, 4); 
	} 
	else 
	{
		response[0] = '\0';
		windowPrintColor(commsData->window, "No response from mining vehicle", RED, 5, 4); 
	}
	
	// prints last command
	commsData->prevCommand = *commsData->command;		
	
	// TRANSPORT VEHICLE COMMUNICATIONS	
	char request[2] = {'x', '\0'};
	char confirmation[2] = {'\0', '\0'};
	double distance; 


	sem_post(&tran);
	windowPrint(commsData->satWindow, "                                      ", 3, 4); 
	windowPrint(commsData->satWindow, "                                      ", 5, 4); 
	windowPrintColor(commsData->satWindow, "Transport Vehicle Communications: ", MAGENTA, 1, 2); 

	// checks for transport vehicle request 
	if (read(commsData->transportCh[0], request, CHAR) > 0)
	{
		sprintf(buffer, "Received the request: %c\0", request[0]);
		if (request[0] == 'T') // lift off request
		{ 
			windowPrintColor(commsData->satWindow, buffer, GREEN, 3, 4);
			confirmation[0] = 'K'; 
			sprintf(buffer, "Sent confirmation: %c\0", confirmation[0]);
			windowPrintColor(commsData->satWindow, buffer, GREEN, 5, 4);
		}
		else if (request[0] == 'D') // docking request
		{
			windowPrintColor(commsData->satWindow, buffer, GREEN, 3, 4); 
			confirmation[0] = 'C';
			sprintf(buffer, "Sent confirmation: %c\0", confirmation[0]);
			windowPrintColor(commsData->satWindow, buffer, GREEN, 5, 4); 
		}
		else 
		{
			windowPrintColor(commsData->satWindow, "No requests from transport vehicle\0", RED, 3, 4); 
		}	
	}

	// sends confirmation to transport vehicle 
	write(commsData->transportCh[1], confirmation, CHAR);

	sem_wait(&tran);
}
