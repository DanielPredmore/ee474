#include <stdio.h>
#include "ISR.h"
#include "transportDistance.h"
#include "kernel.h"

extern bool transportSignal;
extern bool fullyDeployed;
extern bool fullyRetracted;
extern bool measureTemp;

extern bool transportSignalAck;
extern bool fullyDeployedAck;
extern bool fullyRetractedAck;
extern bool measureTempAck;
extern TCB tdTCB;

extern signed int transportDistPid;
extern signed int batteryTempReadPid;

// Interrupt Service Routine
void ISR()
{
	if(transportSignal && !transportSignalAck)
	{
		transportSignalAck = TRUE;
		wakeTask(transportDistPid);
		transportDistance(tdTCB.taskDataPtr);
	}
	if(measureTemp && !measureTempAck)
	{
		measureTempAck = TRUE;
		wakeTask(batteryTempReadPid);
	}
	if(fullyDeployed || fullyRetracted)
	{
		if(fullyDeployed)
			fullyDeployedAck = TRUE;
		else if (fullyRetracted)
			fullyDeployedAck = TRUE;
		wakeTask(solarPanelPid);
	}
	if(overTemperature && solarPanelState)
	{
		solarPanelRetract = 1;
	}
}
