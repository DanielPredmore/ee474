cmake_minimum_required(VERSION 2.8.9)
project(satellite)

find_package (Threads)
set (CMAKE_C_STANDARD 99)
set(CMAKE_BUILD_TYPE Debug)

set(LIB_FILES
		satellite.c
		satellite.h
		powerSubsystem.c
		powerSubsystem.h
		thrusterSubsystem.c
		thrusterSubsystem.h
		warningAlarm.c
		warningAlarm.h
		consoleDisplay.c
		consoleDisplay.h
		satelliteComms.c
		satelliteComms.h
		vehicleComms.c
		vehicleComms.h
		miningVehicle.c
		miningVehicle.h
		transportVehicle.c
		transportVehicle.h
		transportDistance.c
		transportDistance.h
		solarPanelControl.c
		solarPanelControl.h
		kernel.c
		kernel.h
		startup.c
		startup.h
		display.c
		display.h
		timeWindow.c
		timeWindow.h
		bbbIO.c
		bbbIO.h
		consoleKeypad.c
		consoleKeypad.h
		keypad.c
		keypad.h
		root.h
		kernelStat.c
		kernelStat.h
		optfft.c
		optfft.h
		dummy.c
		dummy.h
		majorCycle.c
		majorCycle.h
		ioTask.c
		frequencyCapture.c
		frequencyCapture.h
		ISR.c
		ISR.h)

add_executable(satellite.out main.c ${LIB_FILES})
target_link_libraries (satellite.out ${CMAKE_THREAD_LIBS_INIT})
