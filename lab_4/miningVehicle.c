#include <unistd.h>
#include <pthread.h>
#include <unistd.h>
#include "miningVehicle.h"
#include <fcntl.h>
#include "ports.h"
#include "block.h"
#include <semaphore.h>

#define TRUE 1
#define CHAR sizeof(char)

void miningVehicle(void * channel) 
{
	unsigned int *channel_ = channel;
	char buffer[2] = {'\0', '\0'}; 
	
	//channel_[0] = open(V_PORT0, O_WRONLY);
   	//channel_[1] = open(V_PORT1, O_RDONLY); 	
		
	while(!channel_[2])
	{
		sem_wait(&mine);
		sem_post(&mine);
		if(read(channel_[1], buffer, CHAR) > 0) 
		{
			write(channel_[0], buffer, CHAR);				
		}
		else
		{
			buffer[0] = '\0';	
		}
		
		//buffer[0] = 'A';
		//write(channel_[0], buffer, CHAR);
	  	//usleep(1000000);	
	}
}
