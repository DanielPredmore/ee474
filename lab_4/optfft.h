#ifndef OPTFFT_H
#define OPTFFT_H

#include "optfft.h"

signed int optfft(signed int x[256], signed int y[256]);

#endif // OPTFFT_H
