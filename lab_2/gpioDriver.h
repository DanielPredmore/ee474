#ifndef GPIO_DRIVER_H
#define GPIO_DRIVER_H

#include "satellite.h"

#define GPIO_PATH "/sys/class/gpio/"
#define GPIO_VALUE "value"
#define GPIO_DIRECTION "direction"
#define GPIO_MAX_PATH_LENGTH 100
#define GPIO char*
#define gpioMode char*
#define GPIO_INPUT "in"
#define GPIO_OUTPUT "out"
#define GPIO_67 "gpio67/"

void setGPIOmode(GPIO gpio, gpioMode mode);
void setGPIO(GPIO gpio, bool value);
bool checkGPIO(GPIO gpio);

#endif // GPIO_DRIVER_H
