#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "warningAlarm.h"
#include "satellite.h"
#include "ledDriver.h"

extern unsigned short batteryLevel;
extern unsigned short fuelLevel;

struct warningAlarmData {
	bool ledStates[3];
};
typedef struct warningAlarmData dataStruct;

// private function prototypes
void warningAlarm(void *data);
void checkSetLed(int led, bool value, dataStruct *data);
void blinkLed(int led, int rate, dataStruct *data);

/**
 * This function makes a new TCB task for the kernel
 *
 * @return the TCB for the warningAlarm
 */
TCB getWarningAlarm() 
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));

	// here is space to initialize the data struct's data
	data->ledStates[0] = checkLed(1);
	data->ledStates[1] = checkLed(2);
	data->ledStates[2] = checkLed(3);


	// setting the values in the TCB
	theTCB.task = &warningAlarm;
	theTCB.taskDataPtr = data;
	return theTCB;
}

/**
 * This is the main TCB task function for the warning alarm system.
 * It is responsible for setting the warning lights
 *
 * @param data this is the data struct form the PCB
 */
void warningAlarm(void *data)
{
	// casting back to dataStruct
	dataStruct *theData = (dataStruct*)data;

	// checking the warning conditions
	if (batteryLevel > 50 && fuelLevel > 50)
		checkSetLed(3, TRUE, theData);
	else
		checkSetLed(3, FALSE, theData);

	if (batteryLevel <= 50 && batteryLevel > BATTERY_LOW)
		blinkLed(2, 2, theData);
	else if (batteryLevel <= BATTERY_LOW) 
		blinkLed(2, 1, theData);
	else
		checkSetLed(2, FALSE, theData);

	if (fuelLevel <= 50 && fuelLevel > FUEL_LOW)
		blinkLed(1, 2, theData);
	else if (fuelLevel <= FUEL_LOW)
		blinkLed(1, 1, theData);
	else
		checkSetLed(1, FALSE, theData);
}

/**
 * checkSetLed compares the current value of an led to the set value
 * if they are different, it updates the led to the new value.
 * Otherwise it keep the led the same and does nothing
 *
 * @param led the led #
 * @param value the value of the led
 * @param data a pointer to the data struct
 */
void checkSetLed(int led, bool value, dataStruct *data)
{
	if (value != data->ledStates[led-2])
	{
		setLed(led, value);
		data->ledStates[led-2] = value;
	}
}

/**
 * This function blinks an led at a set rate
 *
 * @param led the led #
 * @param rate the speed of the blink in seconds
 * @param data a pointer to the data struct
 */
void blinkLed(int led, int rate, dataStruct *data)
{
	time_t rawtime;
	time(&rawtime);
	bool set = (rawtime / rate) % (rate * 2) <= rate/2;
	checkSetLed(led, set, data);
}
