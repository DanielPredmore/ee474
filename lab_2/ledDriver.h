#ifndef LED_DRIVER_H
#define LED_DRIVER_H

#include "satellite.h"

// path is system system specific please change 
// these values to point to the file locations on
// your system
#define LED_0_PATH "/sys/devices/platform/leds/leds/beaglebone:green:usr0/brightness"
#define LED_1_PATH "/sys/devices/platform/leds/leds/beaglebone:green:usr1/brightness"
#define LED_2_PATH "/sys/devices/platform/leds/leds/beaglebone:green:usr2/brightness"
#define LED_3_PATH "/sys/devices/platform/leds/leds/beaglebone:green:usr3/brightness"

void setLed(int led, bool state);
bool checkLed(int led);

#endif // LED_DRIVER_H
