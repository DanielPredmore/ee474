#include "satellite.h"
#include "satelliteComms.h"
#include "consoleDisplay.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// global variables
extern bool fuelLow;
extern bool batteryLow;
extern bool solarPanelDeployed; 
extern unsigned short batteryLevel; 
extern unsigned short fuelLevel; 
extern unsigned short powerConsumption; 
extern unsigned short powerGeneration; 
extern unsigned int thrusterCommand; 

// data struct for the satellite communication
struct satelliteCommsData
{
	bool * fuelLow;
	bool * batteryLow;
	bool * solarPanelDeployed; 
	unsigned short * batteryLevel; 
	unsigned short * fuelLevel; 
	unsigned short * powerConsumption; 
    //unsigned short * powerGeneration; // debug
	unsigned int * thrusterCommand; 
};
typedef struct satelliteCommsData dataStruct; 

// function prototypes
void satelliteComms(void *data);

/**
 * This function makes a new TCB task for the kernel
 * 
 * @returns the TCB for satelliteComms
 **/
TCB getSatelliteComms()
{
	TCB theTCB; 
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 
	
	// initilizing data struct	
	(data->fuelLow) = &fuelLow; 
	(data->batteryLow) = &batteryLow;
	(data->solarPanelDeployed) = &solarPanelDeployed; 
	(data->batteryLevel) = &batteryLevel;
   	(data->fuelLevel) = &fuelLevel; 
	(data->powerConsumption) = &powerConsumption; 
	//(data->powerGeneration) = &powerGeneration; // debug 
	(data->thrusterCommand) = &thrusterCommand; 	
	

	// initialize random seed
	srand (time(NULL));

	// setting the values in the TCB
	theTCB.task = &satelliteComms; 
	theTCB.taskDataPtr = data; 
	
	return theTCB; 
}

/**
 * This function sends random commands to thruster subsystems and send data onto the console
 *
 * @param data is the data struct for satellite comms
 */
void satelliteComms(void *data)
{
	// cast void point
	dataStruct *commsData = (dataStruct*) data; 
	
	// assigns random 16-bit number to thrusterCommand
	*(commsData->thrusterCommand) = (unsigned int) rand() % 65536; 	

	// transfer data to earth
	// prints status mode
	display ("Sending to Earth...", -1, 1); 
	display ("SATELLITE STATUS MODE:", -1, 1); 
	display ("  Solar Panel State = ", (int) solarPanelDeployed, 1); 
	display ("  Battery Level     = ", (int) batteryLevel, 1); 
	display ("  Fuel Level        = ", (int) fuelLevel, 1); 
	display ("  Power Consumption = ", (int) powerConsumption, 1);
	// display ("  Power Generation  = ", (int) powerGeneration, 1); // debug 
	display ("  ", -1, 1);

	// print annunciation mode
	display ("ANNUNCIATION MODE:", -1, 1); 
	display ("  Fuel Low    = ", (int) fuelLow, 1); 
	display ("  Battery Low = ", (int) batteryLow, 1); 
	display ("  ", -1, 1); 
}



