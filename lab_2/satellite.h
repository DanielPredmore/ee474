#ifndef SATELLITE_H
#define SATELLITE_H

/**
 * This header file contains the globally available variables.
 */

// defining default values
#define NO_COMMAND 0x0000
#define FULL 100
#define EMPTY 0
#define BATTERY_LOW 10
#define FUEL_LOW 10


// defining the TCB struct
struct _satelliteKernelTCB {
		void (*task)(void*);
		void* taskDataPtr;
};
typedef struct _satelliteKernelTCB TCB;

// Defining a bool
enum boolean {
		FALSE = 0,
		TRUE = 1
};
typedef enum boolean bool;


// Thruster Control
unsigned int thrusterCommand;

// Power Management
unsigned short batteryLevel;
unsigned short fuelLevel;
unsigned short powerConsumption;
unsigned short powerGeneration;

// Solar Panel Control
bool solarPanelDeployed;

// Warning Alarm
bool fuelLow;
bool batteryLow;

void initializeGlobals();

#endif // SATELLITE_H
