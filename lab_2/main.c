/**
 * This is the main c file for the satellite.
 * It is responsible for initializing the global values 
 * and launching the kernel.
 */

#include <stdio.h>
#include "satellite.h"
#include "ledDriver.h"
#include "warningAlarm.h"
#include "consoleDisplay.h"
#include "powerSubsystem.h"
#include "satelliteComms.h"
#include "thrusterSubsystem.h"
#include "kernel.h"

extern unsigned short batteryLevel;
extern unsigned short fuelLevel;

void main() {
	initializeGlobals();

	// resetting the leds
	setLed(3,0);
	setLed(2,0);
	setLed(1,0);

	addTask(getWarningAlarm(), 0);
	addTask(getConsoleDisplay(), 0);
	addTask(getPowerSubsystem(), 5 * CLOCKS_PER_SEC);
	addTask(getSatelliteComms(), 5 * CLOCKS_PER_SEC);
	addTask(getThrusterSubsystem(), 5 * CLOCKS_PER_SEC);

	kernelStart();
}
