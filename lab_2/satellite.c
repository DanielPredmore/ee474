/**
 * satellite.c initializes all global variables
 */

#include <stdio.h>
#include "satellite.h"

// global variables
extern unsigned int thrusterCommand;
extern unsigned short batteryLevel;
extern unsigned short fuelLevel;
extern unsigned short powerConsumption;
extern unsigned short powerGeneration;
extern bool solarPanelDeployed;
extern bool fuelLow;
extern bool batteryLow;

void initializeGlobals() {
		thrusterCommand = NO_COMMAND;
		batteryLevel = FULL;
		fuelLevel = FULL;
		powerConsumption = 0;
		powerGeneration = 0;
		solarPanelDeployed = FALSE;
		fuelLow = FALSE;
		batteryLow = FALSE;
}
