#include "powerSubsystem.h"
#include <stdlib.h>

#define FULL_ON 0x01
#define FULL_OFF 0x00
#define DRIVE_RATE 0x01
#define DEPLOY_PERCENT 50
#define RETRACT_PERCENT 95
#define POWER_CONS_LOW 5
#define POWER_CONS_HI 10

// global variables 
extern bool solarPanelDeployed;
extern bool batteryLow;
extern unsigned short batteryLevel;
extern unsigned short powerConsumption;
extern unsigned short powerGeneration;

// data struct for the global power system variables
struct powerSubsystemData
{
	bool * solarPanelDeployed;
	bool * batteryLow;
	unsigned short * batteryLevel;
	unsigned short * powerConsumption;
	unsigned short * powerGeneration;
};
typedef struct powerSubsystemData dataStruct;

// private function prototype
void powerSubsystem(void * data);
void powerSubsystem2(void * data);
void simulatePower(
				unsigned short * powerConPtr, 
				unsigned short * powerGenPtr, 
				unsigned short * batteryLevel,
			   	bool * solarPanelDep,	
				short powerConInc, 
				short powerGenInc);

/**
 * This function makes a new TCB task for the kernel 
 *
 * @return the TCB for the powerSubsystem
 */
TCB getPowerSubsystem()
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct));

	// initialization of data struct's data
	data->solarPanelDeployed = &solarPanelDeployed;
	data->batteryLow = &batteryLow;
   	data->batteryLevel = &batteryLevel;
	data->powerConsumption = &powerConsumption;
	data->powerGeneration = &powerGeneration;
	
	// setting the values in the TCB
	theTCB.task = &powerSubsystem;
	theTCB.taskDataPtr = data;

	return theTCB;
}
/**
 * This is the task function for the power subsystem task of the satellite system
 *
 * @param data: pointer to the power subsystem data struct
 */
void powerSubsystem2(void * data)
{
	// vars keeping track of the simulated power consumption
	static bool odd = FALSE;
	static bool reverse = FALSE;
	// vars keeping track of the panel deployment simulation
    static unsigned short motorDrive = FULL_OFF; 
    static bool deploying = FALSE;
    static bool retracting = FALSE;

	// casting the void * to a dataStruct * to access its contents
	dataStruct * pd = (dataStruct *) data;
	
	// calculating motor drive
    if(deploying)
    {
        motorDrive += DRIVE_RATE;
        if(FULL_ON == motorDrive)
        {
            deploying = FALSE;
            *(pd->solarPanelDeployed) = TRUE;
        }
    }
    else if (retracting)
    {
        motorDrive -= DRIVE_RATE;
        if(FULL_OFF == motorDrive)
        {
            retracting = FALSE;
            *(pd->solarPanelDeployed) = FALSE;
			*(pd->powerGeneration) = 0;
        }
    }

	if(odd) // case when odd is true
	{
		odd = FALSE;
		if(reverse)
        { // system decreases power consumption until it's below 5
    		*(pd->powerConsumption) += 1;
            if(*(pd->powerConsumption) < POWER_CONS_LOW)
            {
                reverse = FALSE;
            }
        }
        else
        { // system increases power consumption until it's above 10
            *(pd->powerConsumption) -= 1;
            if(*(pd->powerConsumption) > POWER_CONS_HI)
            {
                reverse = TRUE;
            }
        }
        if(*(pd->solarPanelDeployed))
        { 
            if(*(pd->batteryLevel) < FULL/2)
            	*(pd->powerGeneration) += 1;
            *(pd->batteryLevel) += *(pd->powerGeneration) - *(pd->powerConsumption);
			if(*(pd->batteryLevel) > FULL)
			{
				if(*(pd->powerGeneration) - *(pd->powerConsumption) > 0)
				{
					*(pd->batteryLevel) = FULL;
				}
				else
				{
					*(pd->batteryLevel) = 0;
				}
			}
			if(*(pd->batteryLevel) > RETRACT_PERCENT)
            {
                // *** issue command to retract solar panel ***
                retracting = TRUE;
            }
        }
        else
        { 
            *(pd->batteryLevel) -= 3 * *(pd->powerConsumption);
			if(*(pd->batteryLevel) >= FULL)
			{
				*(pd->batteryLevel) = 0;
			}
			if(*(pd->batteryLevel) < DEPLOY_PERCENT)
            {
                // *** issue command to deploy solar panel ***
                deploying = TRUE;
            }
        }
	}
    else // even case
    {
        odd = TRUE;
		if(reverse)
		{
			*(pd->powerConsumption) -= 2;
			if(*pd->powerConsumption < POWER_CONS_LOW)
			{
				reverse = FALSE;
			}
		}
		else
		{
			*(pd->powerConsumption) += 2;
            if(*(pd->powerConsumption) > POWER_CONS_HI)
            {
                reverse = TRUE;
            }
        }
        if(*(pd->solarPanelDeployed))
        {
            *(pd->powerGeneration) += 2;
            *(pd->batteryLevel) += *(pd->powerGeneration) - *(pd->powerConsumption);
			if(*(pd->batteryLevel) > FULL)
			{
				if(*(pd->powerGeneration) - *(pd->powerConsumption) > 0)
				{
					*(pd->batteryLevel) = FULL;
				}
				else
				{
					*(pd->batteryLevel) = 0;
				}
			}
            if(*(pd->batteryLevel) > RETRACT_PERCENT)
            {
                // *** issue command to retract solar panel ***
                retracting = TRUE;
            }
        }
        else
        {
			*(pd->batteryLevel) -= 3 * *(pd->powerConsumption);
			if(*(pd->batteryLevel) >= FULL)
			{
				*(pd->batteryLevel) = 0;
			}
            if(*(pd->batteryLevel) < DEPLOY_PERCENT)
            {
                // *** issue command to deploy solar panel ***
                deploying = TRUE;
            }
        }
	}

}

/**
 * This function determines when the satellite generates and consumes power.
 *
 * @param data is the data struct for the given power subsystem
 */
void powerSubsystem(void * data)
{
	
	// vars keeping track of the simulated power consumption
	static bool odd = FALSE;

	// casting the void * to a dataStruct * to access its contents
	dataStruct * pd = (dataStruct *) data;

	if(odd) // case when odd is true
	{
		odd = FALSE;
		if(*(pd->batteryLevel) < FULL/2)
			simulatePower(
						pd->powerConsumption,
						pd->powerGeneration,
						pd->batteryLevel,
						pd->solarPanelDeployed,
						-1,
						1);
		else
			simulatePower(
						pd->powerConsumption,
						pd->powerGeneration,
						pd->batteryLevel,
						pd->solarPanelDeployed,
						-1,
						0);
	}
	else // even case
	{
		odd = TRUE;
		simulatePower(
					pd->powerConsumption,
					pd->powerGeneration,
					pd->batteryLevel,
					pd->solarPanelDeployed,
					2,
					2);
	}

	if(*(pd->batteryLevel) < BATTERY_LOW)
		*(pd->batteryLow) = TRUE;
	else
		*(pd->batteryLow) = FALSE;
}

/**
 * This function determine the rate in which the satellite consumes
 * or generates power. It contorl when the solar panels are deployed
 *
 * @param powerConPtr is the rate that power is consumed, powerGenPtr is 
 * the rate that power is generated, batteryLevel is the power level the
 * battery is at, solarPanelDep is if the solar panels are deployed or not, 
 * powerConInc is the value which the consumption rate increments or
 * decrements, powerGenInc is the value which the generation rate increments
 * or decrements. 
 */
void simulatePower(
				unsigned short * powerConPtr, 
				unsigned short * powerGenPtr, 
				unsigned short * batteryLevel, 
			   	bool * solarPanelDep,
				short powerConInc, 
				short powerGenInc)
{
	static bool reverse = FALSE;
	// vars keeping track of the panel deployment simulation
    static unsigned short motorDrive = FULL_OFF; 
    static bool deploying = FALSE;
    static bool retracting = FALSE;
	
	// calculating motor drive
    if(deploying)
    {
        motorDrive += DRIVE_RATE;
        if(FULL_ON == motorDrive)
        {
            deploying = FALSE;
            *solarPanelDep = TRUE;
        }
    }
    else if (retracting)
    {
        motorDrive -= DRIVE_RATE;
        if(FULL_OFF == motorDrive)
        {
            retracting = FALSE;
            *solarPanelDep = FALSE;
			*powerGenPtr = 0;
        }
    }

	if(reverse)
	{// system decreases power consumption until it's below 5
		*powerConPtr -= powerConInc;
		if(*powerConPtr < POWER_CONS_LOW)
			reverse = FALSE;
	}
	else
	{// system increases power consumption until it's above 10
		*powerConPtr += powerConInc;
		if(*powerConPtr > POWER_CONS_HI)
			reverse = TRUE;
	}
	if(*solarPanelDep)
	{
		*powerGenPtr += powerGenInc;
		*batteryLevel += *powerGenPtr - *powerConPtr;
		if(*batteryLevel > FULL)
		{
			if(*powerGenPtr - *powerConPtr > 0)
				*batteryLevel = FULL;
			else
				*batteryLevel = 0;
		}
		// issue command to retract solar panel
		if(*batteryLevel > RETRACT_PERCENT)
			retracting = TRUE;
	}
	else
	{
		*batteryLevel -= 3 * *powerConPtr;
		if(*batteryLevel >= FULL)
			*batteryLevel = 0;
		// issue command to deploy solar panel
		if(*batteryLevel < DEPLOY_PERCENT)
			deploying = TRUE;
	}
}
