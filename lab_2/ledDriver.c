#include <stdio.h>
#include "ledDriver.h"

// private function prototype
FILE *getLed(int led, char * mode);

/**
 * This function sets the state of an Led
 *
 * @param led the number the led to be set
 * @param state on or off
 */
void setLed(int led, bool state)
{
	FILE *fp = getLed(led, "w");

	// bad led number
	if (NULL == fp)
	{
		#ifdef DEBUG
			printf("Error opening led #%d\n", i);
		#endif // DEBUG
		return;
	}
	if (state)
	{
		fprintf(fp, "1");
	}
	else
	{
		fprintf(fp, "0");
	}
	fclose(fp);
}

/**
 * This function checks the state of an LED
 *
 * @param led the led to be checked
 * @return the state of the led
 */
bool checkLed(int led)
{
	FILE *fp = getLed(led, "r");
	if (NULL == fp)
	{
		#ifdef DEBUG
			printf("Error opening led #%d\n", i);
		#endif
		return FALSE;
	}

	char state = fgetc(fp);
	fclose(fp);
	return state != '0';
}

/**
 * This function opens a file pointer to the led
 *
 * @param led which led file to open
 * @param mode the mode to open the file
 * @return a pointer to the file
 */
FILE *getLed(int led, char * mode)
{
	switch (led) 
	{
		case 0 :
			return fopen(LED_0_PATH, mode);
			break;
		case 1 :
			return fopen(LED_1_PATH, mode);
			break;
		case 2 :
			return fopen(LED_2_PATH, mode);
			break;
		case 3 :
			return fopen(LED_3_PATH, mode);
			break;
		default :
			return NULL;
	}
}
