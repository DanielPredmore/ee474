#include <stdio.h>
#include <unistd.h>
#include "gpioDriver.h"

FILE *openGpio(GPIO gpio, char *file, char *mode);

/**
 * This function sets the GPIO mode of a GPIO port
 *
 * @param gpio the pin to be accessed
 * @param mode the mode to set the GPIO either in or out
 */
void setGPIOmode(GPIO gpio, gpioMode mode)
{
	FILE *fp = openGpio(gpio, GPIO_DIRECTION, "w");
	
	if (fp == NULL)
	{
		return;
	}

	fprintf(fp, mode);
	fclose(fp);
}

/**
 * This function sets the value of a GPIO port
 *
 * @param gpio the port to be set
 * @param value the value of the gpio
 */
void setGPIO(GPIO gpio, bool value)
{
	FILE *fp = openGpio(gpio, GPIO_VALUE, "w");

	if (fp == NULL)
	{
		return;
	}

	fprintf(fp, "%d", value);
	fclose(fp);
}

/**
 * This function finds the state of a GPIO pin
 *
 * @param gpio the port to check
 * @return a bool for if the pin was high or low
 */
bool checkGPIO(GPIO gpio)
{
	FILE *fp = openGpio(gpio, GPIO_VALUE, "r");

	if (fp == NULL)
	{
		return FALSE;
	}

	char value = fgetc(fp);
	return value != '0';
}

/**
 * This function opens a file pointer to a GPIO file
 *
 * @param gpio the GPIO port to be opened
 * @param file the name of the file to open
 * @param mode the edit mode of the file
 * @return a pointer to the file
 */
FILE *openGpio(GPIO gpio, char *file, char *mode)
{
	FILE *fp;

	chdir(GPIO_PATH);
	chdir(gpio);
	
	fp = fopen(file, mode);
	return fp;
}
