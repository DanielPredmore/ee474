#include "satellite.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "consoleDisplay.h"

// global variables
extern bool solarPanelDeployed;
extern bool fuelLow; 
extern bool batteryLow; 
extern unsigned short batteryLevel; 
extern unsigned short fuelLevel;
extern unsigned short powerConsumption; 
extern unsigned short powerGeneration; 

// terminal ports 
int	satellite;
int	earth; 

// the corresponding data struct for console display data
struct consoleDisplayData
{
	bool  solarPanelDeployed; 
	bool  fuelLow; 
	bool  batteryLow; 
	unsigned short  batteryLevel;
	unsigned short  fuelLevel;
	unsigned short  powerConsumption; 
	//unsigned short  powerGeneration; 
};
typedef struct consoleDisplayData dataStruct; 

// function prototypes 
void consoleDisplay(void * data); 
void display(char * str, int data, int terminal); 

/**
 * This function makes a new TCB for console display
 *
 * @return the TCB for the consoleDisplay 
 */
TCB getConsoleDisplay()
{
	TCB theTCB;
	dataStruct *data = (dataStruct*)malloc(sizeof(dataStruct)); 
	
	satellite = open ("/dev/pts/0", O_WRONLY);
	earth = open ("/dev/pts/1", O_WRONLY);

	// -1 to each values so the values are initilized to differnt
	data->solarPanelDeployed = solarPanelDeployed - 1;
	data->fuelLow = fuelLow - 1; 
	data->batteryLow = batteryLow - 1; 
	data->batteryLevel = batteryLevel - 1; 
	data->fuelLevel = fuelLevel - 1;
	data->powerConsumption = powerConsumption - 1;
	//data->powerGeneration = powerGeneration - 1; //debugging purposes

	theTCB.task = &consoleDisplay; 
	theTCB.taskDataPtr = data; 

	return theTCB; 
}

/**
 * This function prints the status and annunciation modes
 * for the satellite. 
 *
 * @param the given data struct for console display
 */
void consoleDisplay(void * data) 
{
	dataStruct *prevData = (dataStruct *)data;
		
	// checks if any value has changed
	if (prevData->solarPanelDeployed != solarPanelDeployed ||
			prevData->fuelLow != fuelLow ||
			prevData->batteryLow != batteryLow ||
			prevData->fuelLevel != fuelLevel ||
			prevData->powerConsumption != powerConsumption) 
			//||prevData->powerGeneration != powerGeneration) // debugging
	{
		// prints to satellite console
		// prints status mode
		display ("SATELLITE STATUS MODE:", -1, 0); 
	   	display ("  Solar Panel State = ", (int) solarPanelDeployed, 0); 
		display ("  Battery Level     = ", (int) batteryLevel, 0); 
		display ("  Fuel Level        = ", (int) fuelLevel, 0); 
		display ("  Power Consumption = ", (int) powerConsumption, 0);
		//display ("  Power Generation  = ", (int) powerGeneration, 0); // debug
	    display ("  ", -1, 0);

		// print annunciation mode
		display ("ANNUNCIATION MODE:", -1, 0); 
		display ("  Fuel Low    = ", (int) fuelLow, 0); 
		display ("  Battery Low = ", (int) batteryLow, 0); 
		display ("  ", -1, 0); 
		
		// set prev values to current values
		prevData->solarPanelDeployed = solarPanelDeployed;
		prevData->fuelLow = fuelLow; 
		prevData->batteryLow = batteryLow;
		prevData->fuelLevel = fuelLevel;
	   	prevData->powerConsumption = powerConsumption;
		//prevData->powerGeneration = powerGeneration; // debug
		prevData->batteryLevel = batteryLevel; 	
	}
}

/**
 * This function prints statements to a given terminal 
 *
 * @param str takes the string to be prints, data is corresponding data to the
 * string, and terminal determines which terminal screen to print to.
 */
void display(char * str, int data, int terminal)
{
	// print to satellite terminal 
	if (!terminal) 
	{
		// print only the string 
		if (-1 == data) 
		{
			dprintf(satellite, "%s \n", str); 	
		}
		
		// print string and data
		else
		{
			dprintf(satellite, "%s %d \n", str, data); 
		}
	}

	// print to earth terminal
	else 
	{
		//print only the string
		if (-1 == data) 
		{
			dprintf(earth, "%s \n", str); 
		}
		
		// print string and data
		else 
		{
			dprintf(earth, "%s %d \n", str, data); 
		}
	}	
}
