#include <stdio.h>
#include "delay.h" 

#define TRUE 1
#define FALSE 0

#define DELAY 1000

int main(void)
{
    // define local variables 
	volatile int i = 0; 
	char myData; 

	// using escape sequence to remove cursor
	printf("\e[?25l");

	while (TRUE)
    {
		// prints data 
		for (i = 0; i < 4; i++) 
		{
			myData = (char)(i + 'A'); 

			printf("%c ", myData);
		    fflush(stdout); 
			
			// delay so we can see the display 
			delay(DELAY); 
		}
		
		printf("%c", 0x0d); // print cartridge return
		fflush(stdout); 
		myData = ' ';

		// clear line
		for (i = 0; i < 4; i++)
		{
			printf("%c ", myData); 
			fflush(stdout); 

			// delay
			delay(DELAY); 	
		}

		printf("%c", 0x0d); // print cartridge return
		fflush(stdout);
		myData = ' ';

	}
}
