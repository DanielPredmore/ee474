/*
 * "Hello World" example.
 *
 * This example prints
	'Hello World I’m running...
	This is good exercise –  bye bye. '

   to the STDOUT stream.
 *
 */

#include <stdio.h>

int main()
{
  // Printing stuff
  printf("Hello World I'm running...\n");
  printf("This is good exercise - bye bye. \n");
  // It worked (I hope)
  return 0;
}
