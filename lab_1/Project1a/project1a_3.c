//*****************************************************************************
//
// Built from hello.c - Simple hello world example.
//
// Copyright (c) 2006-2011 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
//
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 7243 of the EK-LM3S8962 Firmware Package.
//
//*****************************************************************************

#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

void delay(unsigned long aValue);
void f1Data(unsigned long delay1);
void f2Clear(unsigned long delay2);

//*****************************************************************************
//
// Print "0123456789" 
//
//*****************************************************************************
int main(int argc, char * argv[])
{
    //  define some local variables
    volatile int i = 0;
    int k = 15;
    long delayTime1 = 0;
    long delayTime2 = 0;

    // if no arguments were passed
    if (argc == 1) 
    {
        delayTime1 = 1000;
        delayTime2 = 1000;
    }
    // if only one argument was passed
    else if (argc == 2) 
    {
        delayTime1 = strtol(argv[1], NULL, 10);
        if (0 == delayTime1)
        {
            printf("Please specify proper delay times.\n");
            return 1;
        }
        delayTime2 = delayTime1;
    }
    // if two arguments were passed
    else if (argc == 3)
    {
        delayTime1 = strtol(argv[1], NULL, 10);
        if (0 == delayTime1)
        {
            printf("Please specify proper delay times.\n");
            return 1;
        }
	delayTime2 = strtol(argv[2], NULL, 10);
        if(0 == delayTime2)
        {
            printf("Please specify proper delay time.\n");
            return 1;
        }
    }
    else 
    {
        printf("Incorrect number of arguments.\n");
        return 1;
    }

    //
    // The value if i is:
    //
    printf("The value of i is: %d\n", i);
	fflush(stdout);

    //
    //  print the digits 9 8 7 6 5 4 3 2 1 0
    while(TRUE)
    {
      //  working C style string
      f1Data(delayTime1);

      //  clear the line
      f2Clear(delayTime2);
    }
}
//  software delay
void delay(unsigned long aValue)
{
    volatile unsigned long i = 0;

    volatile unsigned int j = 0;

    for (i = aValue; i > 0; i--)
    {
        for (j = 0; j < 100000; j++);
    }

    return;
}
// for-loop 1
void f1Data(unsigned long delay1)
{
    char myData[3];    

    volatile int i = 0;

    for (i = 9; i >=0; i--)
    {
        myData[0] = i + '0';        //  convert the int i to ascii
        myData[1] = '\0';           //  terminate the string
        printf("%s ", myData);
            fflush(stdout);

        delay(delay1);                //  delay so we can read the display
    }
   
    printf("%c", 0x0d);           //  print a carridge return
        fflush(stdout);
}
// for-loop 2
void f2Clear(unsigned long delay2)
{
    char myData[3] = " \0";

    volatile int i = 0;    

    for (i = 0; i < 10; i++)
    {
        printf("%s ", myData);
            fflush(stdout);

        delay(delay2);
    }

    printf("%c", 0x0d);           //  print a carridge return
        fflush(stdout);
}
