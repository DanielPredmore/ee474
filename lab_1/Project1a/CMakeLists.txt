cmake_minimum_required(VERSION 2.8.9)
project(project1a)

set(LIB_FILES
        fData.c
        fData.h)

add_executable(project1a_step3.out project1a_step3.c ${LIB_FILES})
add_executable(project1a_step7.out project1a_step7.c ${LIB_FILES})
add_executable(project1a_step8.out project1a_step8.c ${LIB_FILES})
add_executable(project1a_step9.out project1a_step9.c ${LIB_FILES})
add_executable(project1a_step10.out project1a_step10.c ${LIB_FILES})
add_executable(project1a_step11.out project1a_step11.c ${LIB_FILES})
