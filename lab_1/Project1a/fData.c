#include "fData.h"

//  software delay
void delay(unsigned long aValue)
{
    volatile unsigned long i = 0;

    volatile unsigned int j = 0;

    for (i = aValue; i > 0; i--)
    {
        for (j = 0; j < 100000; j++);
    }

    return;
}

void fData(char data, unsigned long * delay1, unsigned long * delay2)
{
    printf("%c ", data);
    fflush(stdout);

    if(data != ' ')
		delay(*delay1);                //  delay so we can read the display
	else
		delay(*delay2);
}
