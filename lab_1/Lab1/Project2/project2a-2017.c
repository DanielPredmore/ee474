#include <stdio.h>
#include "delay.h"

#define TRUE 1
#define FALSE 0

#define DELAY 1000

int main(void) 
{
	// forever loop
    while (TRUE)
    {
        // prints string
		printf("A B C D");
		fflush(stdout); 
		
		// carridge return
		printf("%c", 0x0d);
		fflush(stdout); 
		
		// delay
		delay(DELAY);
		
		printf("       "); 
		fflush(stdout);

		// carridge return
		printf("%c", 0x0d);
		fflush(stdout);
		
		delay(DELAY); 
	} 	
}
