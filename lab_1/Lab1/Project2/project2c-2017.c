#include <stdio.h>
#include "delay.h"

#define TRUE 1
#define FALSE 0

#define DELAY 1000

int main(void)
{
    // define local variables 
	volatile int i = 0; 
	char * myData[] = { 
			"A B C D",
			"  B   D",
			"A   C  ",
			"       "}; 

	// using escape sequence to remove cursor
	printf("\e[?25l");

	while (TRUE)
    {
		for(i = 0; i < 4; i++)
		{
			// print string
			printf("%s", myData[i]);
			fflush(stdout); 
			
			// delay to display the string
			delay(DELAY);
			
			// carridge return
			printf("%c", 0x0d);
			fflush(stdout);	
		}
	}
}
