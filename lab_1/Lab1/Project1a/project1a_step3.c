//*****************************************************************************
//
// Built from hello.c - Simple hello world example.
//
// Copyright (c) 2006-2011 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
//
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 7243 of the EK-LM3S8962 Firmware Package.
//
//*****************************************************************************

// What it does: includes the function prototypes of stdio into the program
// Purpose: allows the program to use the stdio functions
#include <stdio.h> 

// What it does: creates a macro that during compilation is replaced with the int value 1
// Purpose: allows code to be more readable by using macros instead of magic numbers
#define TRUE 1
// What it does: creates a macro that during compilation is replaced with the int value 0
// Purpose: allows code to be more readable by using macros instead of magic numbers
#define FALSE 0

// What it does: declares a function prototype and allows this function to be used by subsequent function definitions in the program
// Purpose: allows this function to be used by main
void delay(unsigned long aValue);

//*****************************************************************************
//
// Print "0123456789" 
//
//*****************************************************************************
// What it does: function header for main starting its function definitions
// Purpose: starts the definition of the main function
int main(void)
{

	//  define some local variables
	// What it does: instantiates the volatile int i, allocating memory for it of size int, and setting it to 0
	// Purpose: creates a variable that will not be optimized away by the compiler 
	volatile int i = 0;
	// What it does: instantiates an int variable, allocating memory for it, and setting it to 15
	// Purpose: nothing, it's not used by the program, it should be removed.
	int k = 15;
	
	//
	// The value if i is:
	//
	// What it does: prints "The value of i is: " followed by the value of i as a decimal and a new line to stdout
	// Purpose: prints the initial value of i to the console
	printf("The value of i is: %d\n", i);
	// What it does: finishes writing data that was sent to but not yet written to stdout 
	// Purpose: allows stdout to catch up to the printing and write all data that was sent to it
	fflush(stdout);
	
	//
	//  print the digits 9 8 7 6 5 4 3 2 1 0
	// What it does: creates an infinite loop
	// Purpose: allows the program to run indefinitely until closed by the user
	while(TRUE)
	{
		//  working C style string
		// 
		// What it does: allocates memory for an array the size of 3 char variables
		// Purpose: stores character data to be printed, but should be reduced to 2 as the third element is never used
		char myData[3];
		
		// What it does: creates a for-loop that last for 10 iterations
		// Purpose: a for-loop to contain the repeating prints of the numbers 9 to 0 of the program
		for (i = 9; i >=0; i--)
		{
			// What it does: sets the value of the first element of the myData array according to i
			// Purpose: converts the int value of i to its ascii character value
			myData[0] = i + '0';        //  convert the int i to ascii
			// What it does: sets the value of the second element of the array to the null terminator
			// Purpose: to terminate the c-string so that a print function finishes with the character before this
			myData[1] = '\0';           //  terminate the string
			// What it does: prints the contents of the array and a space to stdout
			// Purpose: to display the numbers 9 through 0 on the console with spaces in between
			printf("%s ", myData);
			// What it does: finishes writing data that was sent to but not yet written to stdout 
			// Purpose: allows stdout to catch up to the printing and write all data that was sent to it
			fflush(stdout);
			
			// What it does: calls the delay function to delay with a value of 1000(approximately 1 second)
			// Purpose: to create pauses between the prints of each number so that the animation is visible to the user
			delay(1000);                //  delay so we can read the display
		}

		// What it does: prints a carriage return to the console setting the print cursor to the beginning of the current line
		// Purpose: set the cursor to the start of the line to set up the erasure the line one number at a line
		printf("%c", 0x0d);           //  print a carridge return
		// What it does: finishes writing data that was sent to but not yet written to stdout 
		// Purpose: allows stdout to catch up to the printing and write all data that was sent to it
		fflush(stdout);
		// What it does: sets the first element of the array to a space
		// Purpose: the program will erase the numbers on the console by replacing them with spaces
		myData[0] = ' ';              //  print a space
		// What it does: sets the value of the second element of the array to the null terminator
		// Purpose: to terminate the c-string so that a print function finishes with the character before this
		myData[1] = '\0';
		
		//  clear the line
		// What it does: creates a for-loop that last for 10 iterations
		// Purpose: a for-loop to contain the repeating the repeating erases of the program
		for (i = 0; i < 10; i++)
		{
			// What it does: prints the contents of the array and a space to stdout
			// Purpose: to erase the numbers 9 through 0 that were printed on the console
			printf("%s ", myData);
			// What it does: finishes writing data that was sent to but not yet written to stdout 
			// Purpose: allows stdout to catch up to the printing and write all data that was sent to it
			fflush(stdout);
			
			// What it does: calls the delay function to delay with a value of 1000(approximately 1 second)
			// Purpose: to create pauses between the prints of each number so that the animation is visible to the user
			delay(1000);
		}
		
		// What it does: prints a carriage return to the console setting the print cursor to the beginning of the current line
		// Purpose: set the cursor to the start of the line to set up the erasure the line one number at a line
		printf("%c", 0x0d);           //  print a carridge return
		// What it does: finishes writing data that was sent to but not yet written to stdout 
		// Purpose: allows stdout to catch up to the printing and write all data that was sent to it
		fflush(stdout);
		// What it does: sets the first element of the array to a space
		// Purpose: the program will erase the numbers on the console by replacing them with spaces
		myData[0] = ' ';              //  print a space
		// What it does: sets the value of the second element of the array to the null terminator
		// Purpose: to terminate the c-string so that a print function finishes with the character before this
		myData[1] = '\0';
	}
}
//  software delay
// What it does: function header for the delay function starting its function definitions
// Purpose: starts the definition of the delay function
void delay(unsigned long aValue)
{
	// What it does: instantiates the volatile unsigned long i, allocating memory for it of size long, and setting it to 0
	// Purpose: creates a variable that will not be optimized away by the compiler so that the program will properly create delays
	volatile unsigned long i = 0;
	// What it does: instantiates the volatile unsigned long j, allocating memory for it of size long, and setting it to 0
	// Purpose: creates a variable that will not be optimized away by the compiler so that the program will properly create delays
	volatile unsigned int j = 0;
	
	// What it does: creates a for-loop that last for the amount of iterations specified by the aValue parameter
	// Purpose: a for-loop that delays the program that can be controlled by the value passed to this function
	for (i = aValue; i > 0; i--)
	{
		// What it does: creates a for-loop that last for 100000 iterations
		// Purpose: a constant delay that delays the program for approximately 1 millisecond for each iteration of the controlled for-loop
		for (j = 0; j < 100000; j++);
	}
	
	// What it does: returns to the scope that called this function
	// Purpose: exits the function
	return;
}