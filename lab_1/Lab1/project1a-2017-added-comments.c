//*****************************************************************************
//
// Built from hello.c - Simple hello world example.
//
// Copyright (c) 2006-2011 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
//
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 7243 of the EK-LM3S8962 Firmware Package.
//
//*****************************************************************************

// including the standard library for c
#include <stdio.h>

// making a easy to use definition for true and false
#define TRUE 1
#define FALSE 0

// function prototype for the delay function
void delay(unsigned long aValue);

//*****************************************************************************
//
// Print "0123456789" to the OLED on the Stellaris evaluation board.
//
//*****************************************************************************

// main function defintion returns an int exit code
int main(void)
{

    //  define some local variables
    volatile int i = 0; // this will be left alone by the compiler
    int k = 15; // an unused int (should be removed)

    //
    // The value if i is:
    //
    printf("The value of i is: %d\n", i); // print i formatted as a decimal
	fflush(stdout); // letting stdout catch up to the printing

    //
    //  print the digits 9 8 7 6 5 4 3 2 1 0
    while(TRUE) // main forever loop
    {
      //  working C style string
	  //  this value could be defined in the main section of the function
      char myData[3]; // This is too long, the 3rd space in never used

	  // for loop to generate the string of numbers
      for (i = 9; i >=0; i--)
      {
		// Why are we using a string as apposed to a char
		// becsues the string only ever contains one used char
        myData[0] = i + '0';        //  convert the int i to ascii
		// this is unnecessary because myData[1] is never changed
		// it should be set once before the loop begins
        myData[1] = '\0';           //  terminate the string
        printf("%s ", myData);      // printing as a null terminated string
		fflush(stdout); // letting stdout catch up to printing

		// 1000 is a magic number
		// should use defined constant such as #define DELAY 1000
        delay(1000);                //  delay so we can read the display
      }

	  // putting the cursor to the beginning of the line
      printf("%c", 0x0d);           //  print a carriage return
	  fflush(stdout);
	  // once again it would be better to use just one char
      myData[0] = ' ';              //  print a space
	  // this is unneeded because myData[1] is already '\0'
      myData[1] = '\0';

      //  clear the line

	  // printing a space over the value once every delay
      for (i = 0; i < 10; i++)
      {
        printf("%s ", myData);
		fflush(stdout);

		// do you believe in magic?
		// I know I don't
		// get rid of the magic number
        delay(1000);
      }

      printf("%c", 0x0d);           //  print a carriage return
      myData[0] = ' ';              //  print a space
      myData[1] = '\0';
    }

}
//  software delay
//  This is the implementation of the prototyped delay function
//  It works by counting to 100000 an aValue number of times
void delay(unsigned long aValue)
{
	// this value is used to count to 100000 for each delay
    volatile unsigned long i = 0;

	// this value counts to the number of delays
    volatile unsigned int j = 0;

    for (i = aValue; i > 0; i--)
    {
		// There is magic in the air
        for (j = 0; j < 100000; j++); // j must be volatile
    }

	// unneeded but not bad
    return;
}
